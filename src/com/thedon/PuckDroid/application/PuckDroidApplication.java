package com.thedon.PuckDroid.application;

import android.app.Application;
import com.thedon.PuckDroid.data.League;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.Team;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PuckDroidApplication extends Application
{
    private static League sLeague;
    private static LeagueSettings sLeagueSettings;
    private static Team sCurrentUserTeam;

    private static int sRosterPlayersCount;
    private static int sTomorrowsRosterPlayersCount;

    public static void setLeague(League aLeague)
    {
        sLeague = aLeague;
    }

    public static League getLeague()
    {
        return sLeague;
    }

    public static void setLeagueSettings(LeagueSettings aLeagueSettings)
    {
        sLeagueSettings = aLeagueSettings;
    }

    public static LeagueSettings getLeagueSettings()
    {
        return sLeagueSettings;
    }

    public static void setCurrentUserTeamInfo(Team aTeam)
    {
        sCurrentUserTeam = aTeam;
    }

    public static Team getCurrentUserTeamInfo()
    {
        return sCurrentUserTeam;
    }

    public static void setRosterPlayersCount(int aRosterPlayersCount)
    {
        sRosterPlayersCount = aRosterPlayersCount;
    }

    public static int getRosterPlayersCount()
    {
        return sRosterPlayersCount;
    }

    public static void setTomorrowsRosterPlayersCount(int aRosterPlayersCount)
    {
        sTomorrowsRosterPlayersCount = aRosterPlayersCount;
    }

    public static int getTomorrowsRosterPlayersCount()
    {
        return sTomorrowsRosterPlayersCount;
    }
}
