package com.thedon.PuckDroid.application;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Preferences
{
    private static Preferences sInstance;

    private final SharedPreferences mSharedPreferences;
    private final SharedPreferences.Editor mSharedPreferencesEditor;

    private static final String PREFERENCES_FILE = "puckDroidPrefs";

    //League settings
    public static final String KEY_SETTINGS_ROSTER_C_COUNT = "settings_roster_c_count";
    public static final String KEY_SETTINGS_ROSTER_LW_COUNT = "settings_roster_lw_count";
    public static final String KEY_SETTINGS_ROSTER_RW_COUNT = "settings_roster_rw_count";
    public static final String KEY_SETTINGS_ROSTER_W_COUNT = "settings_roster_w_count";
    public static final String KEY_SETTINGS_ROSTER_F_COUNT = "settings_roster_f_count";
    public static final String KEY_SETTINGS_ROSTER_D_COUNT = "settings_roster_d_count";
    public static final String KEY_SETTINGS_ROSTER_G_COUNT = "settings_roster_g_count";
    public static final String KEY_SETTINGS_ROSTER_UTIL_COUNT = "settings_roster_util_count";
    public static final String KEY_SETTINGS_ROSTER_BN_COUNT = "settings_roster_bn_count";
    public static final String KEY_SETTINGS_ROSTER_IR_COUNT = "settings_roster_ir_count";

    //OAuth tokens
    public static final String KEY_REQUEST_TOKEN = "requestToken";
    public static final String KEY_REQUEST_SECRET = "requestSecret";
    public static final String KEY_ACCESS_TOKEN = "accessToken";
    public static final String KEY_ACCESS_SECRET = "accessSecret";
    public static final String KEY_SESSION_HANDLE = "sessionHandle";
    public static final String KEY_LAST_ACCESS_TOKEN_REFRESH = "lastAccessTokenRefresh";

    //User
    public static final String KEY_LEAGUE_KEY = "leagueKey";
    public static final String KEY_CURRENT_USER_TEAM_KEY = "currentUserTeamKey";

    //League
    public static final String KEY_CURRENT_WEEK = "currentWeek";


    private Preferences(Context context)
    {
        mSharedPreferences = context.getSharedPreferences(PREFERENCES_FILE, Context.MODE_PRIVATE);
        mSharedPreferencesEditor = mSharedPreferences.edit();
    }

    /**
     * Returns the instance of this singleton.
     *
     * @param context The application context to use.
     * @return The singleton.
     */
    public static Preferences getInstance(Context context)
    {
        if (sInstance == null)
        {
            sInstance = new Preferences(context);
        }

        return sInstance;
    }

    public void putString(String key, String value)
    {
        mSharedPreferencesEditor.putString(key, value);
        mSharedPreferencesEditor.commit();
    }

    public String getString(String key, String defValue)
    {
        return mSharedPreferences.getString(key, defValue);
    }

    public void putInt(String key, int value)
    {
        mSharedPreferencesEditor.putInt(key, value);
        mSharedPreferencesEditor.commit();
    }

    public int getInt(String key, int defValue)
    {
        return mSharedPreferences.getInt(key, defValue);
    }

    public void putLong(String key, long value)
    {
        mSharedPreferencesEditor.putLong(key, value);
        mSharedPreferencesEditor.commit();
    }

    public long getLong(String key, long defValue)
    {
        return mSharedPreferences.getLong(key, defValue);
    }

    public void remove(String key)
    {
        mSharedPreferencesEditor.remove(key);
        mSharedPreferencesEditor.commit();
    }

    public void clear()
    {
        mSharedPreferencesEditor.clear();
        mSharedPreferencesEditor.commit();
    }

}
