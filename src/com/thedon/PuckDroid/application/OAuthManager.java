package com.thedon.PuckDroid.application;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.thedon.PuckDroid.utils.Utils;
import org.scribe.builder.ServiceBuilder;
import org.scribe.builder.api.YahooApi;
import org.scribe.model.*;
import org.scribe.oauth.OAuthService;
import org.scribe.utils.OAuthEncoder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class OAuthManager
{
    private static final String TAG = OAuthManager.class.getSimpleName();

    private static OAuthManager sInstance;

    private final String CONSUMER_KEY = "dj0yJmk9T3ZDaEhpdjBFdjdiJmQ9WVdrOWJWQk1jR2QzTlRZbWNHbzlNQS0tJnM9Y29uc3VtZXJzZWNyZXQmeD0wYg--";
    private final String CONSUMER_SECRET = "a3583c1194f9142b97e98f2b344fa2d73558b555";

    private static final String REQUEST_TOKEN_ENDPOINT_URL ="https://api.login.yahoo.com/oauth/v2/get_request_token";
    private static final String ACCESS_TOKEN_ENDPOINT_URL ="https://api.login.yahoo.com/oauth/v2/get_token";
    private static final String AUTHORIZE_WEBSITE_URL ="https://api.login.yahoo.com/oauth/v2/request_auth";

    private OAuthService mService;

    private Token mRequestToken;
    private Token mAccessToken;
    private String mRequestTokenString;
    private String mRequestSecretString;

    String CALLBACK_URL = "puckdroid://www.yahoo.com";

    private final Context mContext;

    private Preferences mPreferences;

    private static long sLastAccessTokenPersistTimestamp = -1;

    private static final long ACCESS_TOKEN_REFRESH_THRESHOLD = 2700000; //45 minutes

    private OAuthManager(Context aContext)
    {
        mContext = aContext;
        init();
    }

    public static OAuthManager getInstance(Context aContext)
    {
        if (sInstance == null)
        {
            sInstance = new OAuthManager(aContext);
        }

        return sInstance;
    }

    private void init()
    {
        Log.v(TAG, TAG + ".init()");

        mPreferences = Preferences.getInstance(mContext.getApplicationContext());

        mService = new ServiceBuilder()
                                       .provider(YahooApi.class)
                                       .apiKey(CONSUMER_KEY)
                                       .apiSecret(CONSUMER_SECRET)
                                       .callback(CALLBACK_URL)
                                       .build();
    }

    public String getRequestToken()
    {
        Log.v(TAG, TAG + ".getRequestToken()");

        mRequestToken = mService.getRequestToken();
        mRequestTokenString = mRequestToken.getToken();
        mRequestSecretString = mRequestToken.getSecret();

        String authorizationURL = mService.getAuthorizationUrl(mRequestToken);

        if (mPreferences != null)
        {
            mPreferences.putString(Preferences.KEY_REQUEST_TOKEN, mRequestTokenString);
            mPreferences.putString(Preferences.KEY_REQUEST_SECRET, mRequestSecretString);
        }

        return authorizationURL;
    }

    public void getAccessToken(Uri aUri)
    {
        Log.v(TAG, TAG + ".getAccessToken(): " + aUri);

        if (aUri != null)
        {
            final String oauth_verifier = aUri.getQueryParameter("oauth_verifier");
            Log.v(TAG, "oauth_verifier: " + oauth_verifier);

            Verifier verifier = new Verifier(oauth_verifier);
            if (mPreferences != null)
            {
                mRequestTokenString = mPreferences.getString(Preferences.KEY_REQUEST_TOKEN, "");
                mRequestSecretString = mPreferences.getString(Preferences.KEY_REQUEST_SECRET, "");
            }

            if (mService != null)
            {
                mRequestToken = new Token(mRequestTokenString, mRequestSecretString);
                mAccessToken = mService.getAccessToken(mRequestToken, verifier);

                persistAccessToken();
            }
        }
    }

    private String getSessionHandle(String aResponseString)
    {
        Log.v(TAG, TAG + ".getSessionHandle(): " + aResponseString);
        int startIndex = aResponseString.indexOf("&oauth_session_handle=");
        int endIndex = aResponseString.indexOf("&oauth_authorization_expires_in", startIndex);
        String oauthSessionHandle = aResponseString.substring(startIndex + 22, endIndex);

        return oauthSessionHandle;
    }

    public String sendRequest(String aUrl, Verb aVerb, String aData)
    {
        Log.v(TAG, TAG + ".sendRequest(): " + aUrl);

        String responseBody = "";

        try
        {
            if (mPreferences != null)
            {
                mAccessToken = new Token(mPreferences.getString(Preferences.KEY_ACCESS_TOKEN, ""), mPreferences.getString(Preferences.KEY_ACCESS_SECRET, ""));
            }

            if (accessTokenRefreshNeeded())
            {
                refreshAccessToken();
            }

            OAuthRequest request = new OAuthRequest(aVerb, aUrl);
            mService.signRequest(mAccessToken, request);

            if (!Utils.isNullOrEmptyString(aData))
            {
                request.addHeader("Content-Type", "application/xml");
                request.addPayload(aData.getBytes());
            }

            Response response = request.send();
            responseBody = response.getBody();
        }
        catch (Exception e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return responseBody;
    }

    private boolean accessTokenRefreshNeeded()
    {
        if (sLastAccessTokenPersistTimestamp == -1)
        {
            sLastAccessTokenPersistTimestamp = mPreferences.getLong(Preferences.KEY_LAST_ACCESS_TOKEN_REFRESH, -1);
        }
        return (sLastAccessTokenPersistTimestamp == -1) || (System.currentTimeMillis() - sLastAccessTokenPersistTimestamp > ACCESS_TOKEN_REFRESH_THRESHOLD);
    }

    private void refreshAccessToken()
    {
        Log.v(TAG, TAG + ".refreshAccessToken()");

        try
        {
            if(mPreferences != null)
            {
                OAuthRequest request = new OAuthRequest(Verb.GET, ACCESS_TOKEN_ENDPOINT_URL);
                request.addOAuthParameter("oauth_session_handle", mPreferences.getString(Preferences.KEY_SESSION_HANDLE, ""));
                //request.addOAuthParameter(OAuthConstants.TOKEN, mAccessToken.getToken());
                mService.signRequest(mAccessToken, request);
                Response response = request.send();

                mAccessToken = YahooApi.class.newInstance().getAccessTokenExtractor().extract(response.getBody());

                persistAccessToken();

            }
        }
        catch (InstantiationException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IllegalAccessException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void persistAccessToken()
    {
        Log.v(TAG, TAG + ".persistAccessToken()");

        if (mPreferences != null)
        {
            sLastAccessTokenPersistTimestamp = System.currentTimeMillis();

            mPreferences.putString(Preferences.KEY_ACCESS_TOKEN, mAccessToken.getToken());
            mPreferences.putString(Preferences.KEY_ACCESS_SECRET, mAccessToken.getSecret());
            mPreferences.putString(Preferences.KEY_SESSION_HANDLE, getSessionHandle(mAccessToken.getRawResponse()));
            mPreferences.putLong(Preferences.KEY_LAST_ACCESS_TOKEN_REFRESH, sLastAccessTokenPersistTimestamp);
        }
    }
}
