package com.thedon.PuckDroid.application;

import android.util.Log;
import android.util.Xml;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.data.WaiverTransaction;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.adapters.RosterPlayersSelectionListAdapter;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class YahooSportsApiXmlWriter
{
    public static String saveRoster(ArrayList<Player> aRoster, String aDate)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            // start DOCUMENT
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "roster");
            xmlSerializer.startTag("", "coverage_type");
            xmlSerializer.text("date");
            xmlSerializer.endTag("", "coverage_type");
            xmlSerializer.startTag("", "date");
            xmlSerializer.text(aDate);
            xmlSerializer.endTag("", "date");

            xmlSerializer.startTag("", "players");

            for (Player player : aRoster)
            {
                if (!player.isEmptyPlayer())
                {
                    xmlSerializer.startTag("", "player");
                    xmlSerializer.startTag("", "player_key");
                    xmlSerializer.text(player.getPlayerKey());
                    xmlSerializer.endTag("", "player_key");
                    xmlSerializer.startTag("", "position");
                    xmlSerializer.text(player.getSelectedPosition().getPosition());
                    xmlSerializer.endTag("", "position");
                    xmlSerializer.endTag("", "player");
                }
            }
            xmlSerializer.endTag("", "players");
            xmlSerializer.endTag("", "roster");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String addDropTransaction(String aPlayerToDrop, String aPlayerToAddKey, String aTeamKey, String aFaabBidValue)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "transaction");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("add/drop");
            xmlSerializer.endTag("", "type");

            if (!Utils.isNullOrEmptyString(aFaabBidValue))
            {
                xmlSerializer.startTag("", "faab_bid");
                xmlSerializer.text(aFaabBidValue);
                xmlSerializer.endTag("", "faab_bid");
            }

            xmlSerializer.startTag("", "players");

            //Player to add
            xmlSerializer.startTag("", "player");
            xmlSerializer.startTag("", "player_key");
            xmlSerializer.text(aPlayerToAddKey);
            xmlSerializer.endTag("", "player_key");
            xmlSerializer.startTag("", "transaction_data");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("add");
            xmlSerializer.endTag("", "type");
            xmlSerializer.startTag("", "destination_team_key");
            xmlSerializer.text(aTeamKey);
            xmlSerializer.endTag("", "destination_team_key");
            xmlSerializer.endTag("", "transaction_data");
            xmlSerializer.endTag("", "player");

            //Player to drop
            xmlSerializer.startTag("", "player");
            xmlSerializer.startTag("", "player_key");
            xmlSerializer.text(aPlayerToDrop);
            xmlSerializer.endTag("", "player_key");
            xmlSerializer.startTag("", "transaction_data");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("drop");
            xmlSerializer.endTag("", "type");
            xmlSerializer.startTag("", "source_team_key");
            xmlSerializer.text(aTeamKey);
            xmlSerializer.endTag("", "source_team_key");
            xmlSerializer.endTag("", "transaction_data");
            xmlSerializer.endTag("", "player");


            xmlSerializer.endTag("", "players");
            xmlSerializer.endTag("", "transaction");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String addTransaction(String aPlayerToAddKey, String aTeamKey, String aFaabBidValue)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "transaction");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("add");
            xmlSerializer.endTag("", "type");

            if (!Utils.isNullOrEmptyString(aFaabBidValue))
            {
                xmlSerializer.startTag("", "faab_bid");
                xmlSerializer.text(aFaabBidValue);
                xmlSerializer.endTag("", "faab_bid");
            }

            //Player to add
            xmlSerializer.startTag("", "player");
            xmlSerializer.startTag("", "player_key");
            xmlSerializer.text(aPlayerToAddKey);
            xmlSerializer.endTag("", "player_key");
            xmlSerializer.startTag("", "transaction_data");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("add");
            xmlSerializer.endTag("", "type");
            xmlSerializer.startTag("", "destination_team_key");
            xmlSerializer.text(aTeamKey);
            xmlSerializer.endTag("", "destination_team_key");
            xmlSerializer.endTag("", "transaction_data");
            xmlSerializer.endTag("", "player");

            xmlSerializer.endTag("", "transaction");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String dropTransaction(String aPlayerToDrop, String aTeamKey)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "transaction");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("drop");
            xmlSerializer.endTag("", "type");

            //Player to drop
            xmlSerializer.startTag("", "player");
            xmlSerializer.startTag("", "player_key");
            xmlSerializer.text(aPlayerToDrop);
            xmlSerializer.endTag("", "player_key");
            xmlSerializer.startTag("", "transaction_data");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("drop");
            xmlSerializer.endTag("", "type");
            xmlSerializer.startTag("", "source_team_key");
            xmlSerializer.text(aTeamKey);
            xmlSerializer.endTag("", "source_team_key");
            xmlSerializer.endTag("", "transaction_data");
            xmlSerializer.endTag("", "player");

            xmlSerializer.endTag("", "transaction");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String acceptOrRejectTradeOffer(boolean aReject, String aTransactionKey, String aTradeNote)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "transaction");
            xmlSerializer.startTag("", "transaction_key");
            xmlSerializer.text(aTransactionKey);
            xmlSerializer.endTag("", "transaction_key");

            xmlSerializer.startTag("", "type");
            xmlSerializer.text("pending_trade");
            xmlSerializer.endTag("", "type");

            xmlSerializer.startTag("", "action");
            xmlSerializer.text(aReject ? "reject" : "accept");
            xmlSerializer.endTag("", "action");

            if (!Utils.isNullOrEmptyString(aTradeNote))
            {
                xmlSerializer.startTag("", "trade_note");
                xmlSerializer.text(aTradeNote);
                xmlSerializer.endTag("", "trade_note");
            }

            xmlSerializer.endTag("", "transaction");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String voteAgainstTrade(String aTransactionKey, String aTeamKey)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "transaction");
            xmlSerializer.startTag("", "transaction_key");
            xmlSerializer.text(aTransactionKey);
            xmlSerializer.endTag("", "transaction_key");

            xmlSerializer.startTag("", "type");
            xmlSerializer.text("pending_trade");
            xmlSerializer.endTag("", "type");

            xmlSerializer.startTag("", "action");
            xmlSerializer.text("vote_against");
            xmlSerializer.endTag("", "action");

            xmlSerializer.startTag("", "voter_team_key");
            xmlSerializer.text(aTeamKey);
            xmlSerializer.endTag("", "voter_team_key");


            xmlSerializer.endTag("", "transaction");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String proposeTrade(String aTraderTeamKey, String aTradeeTeamKey, String aTradeNote, ArrayList<RosterPlayersSelectionListAdapter.PlayerItem> aTraderPlayers, ArrayList<RosterPlayersSelectionListAdapter.PlayerItem> aTradeePlayers)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "transaction");
            xmlSerializer.startTag("", "type");
            xmlSerializer.text("pending_trade");
            xmlSerializer.endTag("", "type");

            xmlSerializer.startTag("", "trader_team_key");
            xmlSerializer.text(aTraderTeamKey);
            xmlSerializer.endTag("", "trader_team_key");

            xmlSerializer.startTag("", "tradee_team_key");
            xmlSerializer.text(aTradeeTeamKey);
            xmlSerializer.endTag("", "tradee_team_key");

            if (!Utils.isNullOrEmptyString(aTradeNote))
            {
                xmlSerializer.startTag("", "trade_note");
                xmlSerializer.text(aTradeNote);
                xmlSerializer.endTag("", "trade_note");
            }

            xmlSerializer.startTag("", "players");

            for (RosterPlayersSelectionListAdapter.PlayerItem playerItem : aTraderPlayers)
            {
                xmlSerializer.startTag("", "player");
                xmlSerializer.startTag("", "player_key");
                xmlSerializer.text(playerItem.playerKey);
                xmlSerializer.endTag("", "player_key");
                xmlSerializer.startTag("", "transaction_data");
                xmlSerializer.startTag("", "type");
                xmlSerializer.text("pending_trade");
                xmlSerializer.endTag("", "type");

                xmlSerializer.startTag("", "source_team_key");
                xmlSerializer.text(aTraderTeamKey);
                xmlSerializer.endTag("", "source_team_key");

                xmlSerializer.startTag("", "destination_team_key");
                xmlSerializer.text(aTradeeTeamKey);
                xmlSerializer.endTag("", "destination_team_key");
                xmlSerializer.endTag("", "transaction_data");
                xmlSerializer.endTag("", "player");
            }

            for (RosterPlayersSelectionListAdapter.PlayerItem playerItem : aTradeePlayers)
            {
                xmlSerializer.startTag("", "player");
                xmlSerializer.startTag("", "player_key");
                xmlSerializer.text(playerItem.playerKey);
                xmlSerializer.endTag("", "player_key");
                xmlSerializer.startTag("", "transaction_data");
                xmlSerializer.startTag("", "type");
                xmlSerializer.text("pending_trade");
                xmlSerializer.endTag("", "type");

                xmlSerializer.startTag("", "source_team_key");
                xmlSerializer.text(aTradeeTeamKey);
                xmlSerializer.endTag("", "source_team_key");

                xmlSerializer.startTag("", "destination_team_key");
                xmlSerializer.text(aTraderTeamKey);
                xmlSerializer.endTag("", "destination_team_key");
                xmlSerializer.endTag("", "transaction_data");
                xmlSerializer.endTag("", "player");
            }

            xmlSerializer.endTag("", "players");
            xmlSerializer.endTag("", "transaction");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String editWaiversPriority(WaiverTransaction aWaiverTransaction)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            xmlSerializer.setOutput(writer);
            xmlSerializer.startDocument("UTF-8", true);

            xmlSerializer.startTag("", "fantasy_content");
            xmlSerializer.startTag("", "transaction");
            xmlSerializer.startTag("", "transaction_key");
            xmlSerializer.text(aWaiverTransaction.getTransactionKey());
            xmlSerializer.endTag("", "transaction_key");

            xmlSerializer.startTag("", "type");
            xmlSerializer.text("waiver");
            xmlSerializer.endTag("", "type");

            xmlSerializer.startTag("", "waiver_priority");
            xmlSerializer.text(aWaiverTransaction.getWaiverPriority());
            xmlSerializer.endTag("", "waiver_priority");

//            if (!Utils.isNullOrEmptyString(aWaiverTransaction.getFaabBid()))
//            {
//                xmlSerializer.startTag("", "faab_bid");
//                xmlSerializer.text(aWaiverTransaction.getFaabBid());
//                xmlSerializer.endTag("", "faab_bid");
//            }

            xmlSerializer.endTag("", "transaction");
            xmlSerializer.endTag("", "fantasy_content");
            xmlSerializer.endDocument();
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }

    public static String editWaiverClaimFaabValue(String aTransactionKey, String aFaabBid)
    {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try
        {
            if (!Utils.isNullOrEmptyString(aFaabBid))
            {
                xmlSerializer.setOutput(writer);
                xmlSerializer.startDocument("UTF-8", true);

                xmlSerializer.startTag("", "fantasy_content");
                xmlSerializer.startTag("", "transaction");
                xmlSerializer.startTag("", "transaction_key");
                xmlSerializer.text(aTransactionKey);
                xmlSerializer.endTag("", "transaction_key");

                xmlSerializer.startTag("", "type");
                xmlSerializer.text("waiver");
                xmlSerializer.endTag("", "type");

                xmlSerializer.startTag("", "faab_bid");
                xmlSerializer.text(aFaabBid);
                xmlSerializer.endTag("", "faab_bid");

                xmlSerializer.endTag("", "transaction");
                xmlSerializer.endTag("", "fantasy_content");
                xmlSerializer.endDocument();
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return writer.toString();
    }
}
