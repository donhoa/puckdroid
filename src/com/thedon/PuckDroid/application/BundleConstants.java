package com.thedon.PuckDroid.application;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class BundleConstants
{
    public static final String YAHOO_AUTHORIZATION_URL = "yahoo_authorization_url";
    public static final String YAHOO_OAUTH_VERIFIER_URL = "yahoo_oauth_verifier_url";

    public static final String EXTRA_RESULT_RECEIVER = "extra_result_receiver";
    public static final String YAHOO_SPORTS_API_REQUEST = "yahoo_sports_api_request";
    public static final String YAHOO_SPORTS_API_REQUEST_TYPE = "yahoo_sports_api_request_type";
    public static final String YAHOO_SPORTS_API_RESULT = "yahoo_sports_api_result";

    //Yahoo API Calls
    public static final String GET_LEAGUES_LIST = "get_leagues_list";
    public static final String GET_GAME_WEEKS = "get_game_weeks";
    public static final String GET_LEAGUE_STANDINGS = "get_league_standings";
    public static final String GET_LEAGUE_TEAMS = "get_league_teams";
    public static final String GET_LEAGUE_PLAYERS = "get_league_eligible_players";
    public static final String GET_LEAGUE_PLAYERS_FILTER = "get_league_players_filter";
    public static final String GET_LEAGUE_PLAYERS_PAGE_COUNT = "get_league_players_page_count";
    public static final String GET_LEAGUE_TRANSACTIONS = "get_league_transactions";
    public static final String GET_LEAGUE_TRANSACTIONS_PAGE_COUNT = "get_league_transactions_page_count";
    public static final String GET_LEAGUE_SETTINGS = "get_league_settings";
    public static final String GET_LEAGUE_SCOREBOARD = "get_league_scoreboard";
    public static final String GET_ROSTER = "get_roster";
    public static final String GET_TOMORROWS_ROSTER = "get_tomorrows_roster";
    public static final String GET_ROSTER_SEASON_STATS = "get_roster_season_stats";
    public static final String GET_ROSTER_DATE = "get_roster_date";
    public static final String GET_ROSTER_TEAM_KEY = "get_roster_team_key";
    public static final String PUT_ROSTER = "put_roster";
    public static final String GET_PLAYER_INFO = "get_player_info";
    public static final String GET_PLAYER_OWNERSHIP = "get_player_ownership";
    public static final String GET_PLAYER_INFO_KEY = "get_player_info_key";
    public static final String POST_WIRE_TRANSACTION = "post_wire_transaction";
    public static final String USER_MOVE_DATA = "user_move_data";
    public static final String POST_TRADE_PROPOSAL = "post_trade_proposal";
    public static final String GET_TEAM_MATCHUP = "get_team_matchup";
    public static final String SCOREBOARD_WEEK = "scoreboard_week";
    public static final String MATCHUP_WEEK = "matchup_week";
    public static final String GET_TEAM_PENDING_TRADES = "get_team_pending_trades";
    public static final String GET_TEAM_WAIVER_CLAIMS = "get_team_waiver_claims";
    public static final String GET_LEAGUE_PENDING_TRADES = "get_league_pending_trades";
    public static final String DELETE_TRANSACTION = "delete_transaction";
    public static final String PUT_TRANSACTION = "put_transaction";
    public static final String TRANSACTION_KEY = "transaction_key";
    public static final String GET_TEAM_INFO = "get_team_info";
    public static final String GET_TEAM_STATS_BY_DATE = "get_team_stats_by_date";
    public static final String GET_TEAM_STATS_DATE_VALUE = "get_team_stats_date_value";
    public static final String IS_CURRENT_USER = "is_current_user";
    public static final String GET_TEAM_INFO_KEY = "get_team_info_key";
    public static final String GET_TEAM_INFO_NAME = "get_team_info_name";
    public static final String PROPOSE_TRADE_TRADEE_TEAM_KEY = "propose_trade_tradee_team_key";
    public static final String PROPOSE_TRADE_TRADEE_TEAM_NAME = "propose_trade_tradee_team_name";

    public static final String PLAYER_KEY = "player_key";
    public static final String PLAYER_NAME = "player_name";
    public static final String PLAYER_ON_WAIVERS = "player_on_waivers";

    public static final String PENDING_TRADES_FRAGMENT_DATA = "pending_trades_fragment_data";
    public static final String PENDING_TRADES_FRAGMENT_TYPE = "pending_trades_fragment_type";

    public static final String PLAYER_SELECTION_FRAGMENT_TYPE = "player_selection_fragment_type";
}
