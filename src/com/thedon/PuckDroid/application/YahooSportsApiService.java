package com.thedon.PuckDroid.application;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import com.thedon.PuckDroid.utils.Utils;
import org.scribe.model.Verb;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class YahooSportsApiService extends IntentService
{
    private static final String TAG = YahooSportsApiService.class.getSimpleName();

    private static final String EXTRA_REQUEST_PARAM_1 = "extra_request_param_1";
    private static final String EXTRA_REQUEST_PARAM_2 = "extra_request_param_2";

    private static String LEAGUE_KEY;
    private static String CURRENT_USER_TEAM_KEY;
    //Result codes
    public static final int API_RESULT_OK = 0;
    public static final int API_RESULT_ERROR = 1;

    private static final String YAHOO_API_URI_PREFIX = "http://fantasysports.yahooapis.com/fantasy/v2/";
    //Game resource
    private static final String GET_GAME_WEEKS_URI = YAHOO_API_URI_PREFIX + "game/nhl/game_weeks";

    //League resource
    private static final String GET_LEAGUES_LIST_URI = YAHOO_API_URI_PREFIX + "users;use_login=1/games;game_keys=nhl/leagues/teams";
    private static final String GET_LEAGUE_STANDINGS_URI = YAHOO_API_URI_PREFIX + "league/%1$s/standings";
    private static final String GET_TEAM_MATCHUP_URI = YAHOO_API_URI_PREFIX + "team/%1$s/matchups%2$s";
    private static final String GET_LEAGUE_TEAMS_URI = YAHOO_API_URI_PREFIX + "league/%1$s/teams";
    private static final String GET_LEAGUE_SCOREBOARD_URI = YAHOO_API_URI_PREFIX + "league/%1$s/scoreboard%2$s";
    private static final String GET_LEAGUE_PLAYERS_URI = YAHOO_API_URI_PREFIX + "league/%1$s/players;count=20%2$s;out=ownership,stats";
    private static final String GET_LEAGUE_TRANSACTIONS_URI = YAHOO_API_URI_PREFIX + "league/%1$s/transactions;count=10%2$s";
    private static final String GET_LEAGUE_SETTINGS_URI = YAHOO_API_URI_PREFIX + "league/%1$s/settings";
    private static final String GET_TEAM_PENDING_TRADES_URI = YAHOO_API_URI_PREFIX + "league/%1$s/transactions;types=pending_trade;team_key=%2$s";
    private static final String GET_TEAM_WAIVER_CLAIMS_URI = YAHOO_API_URI_PREFIX + "league/%1$s/transactions;team_key=%2$s;type=waiver";
    private static final String GET_LEAGUE_PENDING_TRADES_URI = YAHOO_API_URI_PREFIX + "league/%1$s/transactions;type=trade;";
    private static final String TRANSACTION_URI = YAHOO_API_URI_PREFIX + "transaction/%1$s";

    private static final String GET_TEAM_INFO_URI = YAHOO_API_URI_PREFIX + "team/%1$s;out=stats,standings";
    private static final String GET_TEAM_STATS_BY_DATE_URI = YAHOO_API_URI_PREFIX + "team/%1$s/stats;type=date;date=%2$s";

    private static final String GET_ROSTER_URI = YAHOO_API_URI_PREFIX + "team/%1$s/roster%2$s/players/stats";

    private static final String GET_ROSTER_SEASON_STATS_URI = YAHOO_API_URI_PREFIX + "team/%1$s/roster/players/stats;type=season";

    private static final String PUT_ROSTER_URI = YAHOO_API_URI_PREFIX + "team/%1$s/roster";

    private static final String POST_TRANSACTION_URI = YAHOO_API_URI_PREFIX + "league/%1$s/transactions";

    private static final String GET_PLAYER_INFO_URI = YAHOO_API_URI_PREFIX + "league/%1$s/players;player_keys=%2$s;out=stats,ownership";
    private static final String GET_PLAYER_OWNERSHIP_URI = YAHOO_API_URI_PREFIX + "league/%1$s/players;player_keys=%2$s/ownership";

    public YahooSportsApiService()
    {
        super(TAG);
        Preferences preferences = Preferences.getInstance(this);
        LEAGUE_KEY = preferences.getString(Preferences.KEY_LEAGUE_KEY, "");
        CURRENT_USER_TEAM_KEY = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

        Log.v(TAG, "YahooSportsApiService LEAGUE_KEY: " + LEAGUE_KEY + " - CURRENT_USER_TEAM_KEY: " + CURRENT_USER_TEAM_KEY);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Bundle extras = intent.getExtras();

        Log.v(TAG, "onHandleIntent()");

        ResultReceiver receiver = extras.getParcelable(BundleConstants.EXTRA_RESULT_RECEIVER);

        String request_type = (String)extras.get(BundleConstants.YAHOO_SPORTS_API_REQUEST);

        String extraRequestParam = "";
        String extraRequestParam2 = "";

        if (extras.containsKey(BundleConstants.GET_LEAGUE_PLAYERS_PAGE_COUNT))
        {
            int pageCount = extras.getInt(BundleConstants.GET_LEAGUE_PLAYERS_PAGE_COUNT, 0);
            int start = pageCount * 20;
            Log.v(TAG, "GET LEAGUE PLAYERS START: " + start);
            extraRequestParam = extraRequestParam + ";start=" + String.valueOf(start);
        }

        if (extras.containsKey(BundleConstants.MATCHUP_WEEK))
        {
            String matchupWeek = extras.getString(BundleConstants.MATCHUP_WEEK, "");
            Log.v(TAG, "matchupWeek: " + matchupWeek);

            if (!Utils.isNullOrEmptyString(matchupWeek))
            {
                extraRequestParam = ";weeks=" + matchupWeek;
            }
        }

        if (extras.containsKey(BundleConstants.SCOREBOARD_WEEK))
        {
            String scoreboardWeek = extras.getString(BundleConstants.SCOREBOARD_WEEK, "");
            Log.v(TAG, "scoreboardWeek: " + scoreboardWeek);

            if (!Utils.isNullOrEmptyString(scoreboardWeek))
            {
                extraRequestParam = ";week=" + scoreboardWeek;
            }
        }

        if (extras.containsKey(BundleConstants.GET_LEAGUE_TRANSACTIONS_PAGE_COUNT))
        {
            int pageCount = extras.getInt(BundleConstants.GET_LEAGUE_TRANSACTIONS_PAGE_COUNT, 0);
            int start = pageCount * 10;
            Log.v(TAG, "GET LEAGUE TRANSACTIONS START: " + start);
            extraRequestParam = extraRequestParam + ";start=" + String.valueOf(start);
        }

        if (extras.containsKey(BundleConstants.GET_LEAGUE_PLAYERS_FILTER))
        {
            String filter = extras.getString(BundleConstants.GET_LEAGUE_PLAYERS_FILTER, "");
            Log.v(TAG, "GET LEAGUE PLAYERS filter....: " + filter);

            if (!Utils.isNullOrEmptyString(filter))
            {
                extraRequestParam = extraRequestParam + ";" + filter;
            }
        }

        if (extras.containsKey(BundleConstants.GET_PLAYER_INFO_KEY))
        {
            String playerKey = extras.getString(BundleConstants.GET_PLAYER_INFO_KEY, "");
            Log.v(TAG, "playerKey....: " + playerKey);

            if (!Utils.isNullOrEmptyString(playerKey))
            {
                extraRequestParam = playerKey;
            }
        }

        if (extras.containsKey(BundleConstants.GET_ROSTER_TEAM_KEY))
        {
            String rosterTeamKey = extras.getString(BundleConstants.GET_ROSTER_TEAM_KEY, "");
            Log.v(TAG, "rosterTeamKey: " + rosterTeamKey);

            if (!Utils.isNullOrEmptyString(rosterTeamKey))
            {
                extraRequestParam = rosterTeamKey;
            }

            if (extras.containsKey(BundleConstants.GET_ROSTER_DATE))
            {
                String rosterDate = extras.getString(BundleConstants.GET_ROSTER_DATE, "");
                Log.v(TAG, "rosterDate: " + rosterDate);

                if (!Utils.isNullOrEmptyString(rosterDate))
                {
                    extraRequestParam2 = ";date=" + rosterDate;
                }
            }
        }

        if (extras.containsKey(BundleConstants.TRANSACTION_KEY))
        {
            String pendingTransactionKey = extras.getString(BundleConstants.TRANSACTION_KEY, "");
            Log.v(TAG, "pendingTransactionKey: " + pendingTransactionKey);

            if (!Utils.isNullOrEmptyString(pendingTransactionKey))
            {
                extraRequestParam = pendingTransactionKey;
            }
        }

        if (extras.containsKey(BundleConstants.GET_TEAM_INFO_KEY))
        {
            String teamKey = extras.getString(BundleConstants.GET_TEAM_INFO_KEY, "");
            Log.v(TAG, "teamKey: " + teamKey);

            if (!Utils.isNullOrEmptyString(teamKey))
            {
                extraRequestParam = teamKey;
            }
        }

        if (extras.containsKey(BundleConstants.GET_TEAM_STATS_DATE_VALUE))
        {
            String dateValue = extras.getString(BundleConstants.GET_TEAM_STATS_DATE_VALUE, "");
            Log.v(TAG, "dateValue: " + dateValue);

            if (!Utils.isNullOrEmptyString(dateValue))
            {
                extraRequestParam = dateValue;
            }
        }

        Verb verb = Verb.valueOf((String) extras.get(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE));
        String data = (String)extras.get(BundleConstants.USER_MOVE_DATA);

        final OAuthManager manager = OAuthManager.getInstance(this);

        if (manager != null)
        {
            Bundle requestParams = new Bundle();
            requestParams.putString(EXTRA_REQUEST_PARAM_1, extraRequestParam);
            requestParams.putString(EXTRA_REQUEST_PARAM_2, extraRequestParam2);

            String responseString = manager.sendRequest(getRequestUri(request_type, requestParams), verb, data);

            Bundle resultData = new Bundle();
            resultData.putString(BundleConstants.YAHOO_SPORTS_API_RESULT, responseString);
            resultData.putString(BundleConstants.YAHOO_SPORTS_API_REQUEST, request_type); //Send back the originating request type

            if (receiver != null)
            {
                //TODO: Better way to handle error tag?
                if (responseString.contains("</error>"))
                {
                    receiver.send(API_RESULT_ERROR, resultData);
                }
                else
                {
                    receiver.send(API_RESULT_OK, resultData);
                }
            }
        }
    }

    private String getRequestUri(String aRequest, Bundle aExtraRequestParams)
    {
        String uri = "";
        if (aRequest.equals(BundleConstants.GET_LEAGUES_LIST))
        {
            uri = GET_LEAGUES_LIST_URI;
        }
        else if (aRequest.equals(BundleConstants.GET_LEAGUE_STANDINGS))
        {
            uri = String.format(GET_LEAGUE_STANDINGS_URI, LEAGUE_KEY);
        }
        else if (aRequest.equals(BundleConstants.GET_LEAGUE_PLAYERS))
        {
            uri = String.format(GET_LEAGUE_PLAYERS_URI, LEAGUE_KEY, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_LEAGUE_TRANSACTIONS))
        {
            uri = String.format(GET_LEAGUE_TRANSACTIONS_URI, LEAGUE_KEY, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_LEAGUE_SETTINGS))
        {
            uri = String.format(GET_LEAGUE_SETTINGS_URI, LEAGUE_KEY);
        }
        else if (aRequest.equals(BundleConstants.GET_LEAGUE_SCOREBOARD))
        {
            uri = String.format(GET_LEAGUE_SCOREBOARD_URI, LEAGUE_KEY, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_ROSTER) || aRequest.equals(BundleConstants.GET_TOMORROWS_ROSTER))
        {
            uri = String.format(GET_ROSTER_URI, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1), aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_2));
        }
        else if (aRequest.equals(BundleConstants.GET_ROSTER_SEASON_STATS))
        {
            uri = String.format(GET_ROSTER_SEASON_STATS_URI, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.PUT_ROSTER))
        {
            uri = String.format(PUT_ROSTER_URI, CURRENT_USER_TEAM_KEY);
        }
        else if (aRequest.equals(BundleConstants.POST_WIRE_TRANSACTION) || aRequest.equals(BundleConstants.POST_TRADE_PROPOSAL))
        {
            uri = String.format(POST_TRANSACTION_URI, LEAGUE_KEY);
        }
        else if (aRequest.equals(BundleConstants.GET_PLAYER_INFO))
        {
            uri = String.format(GET_PLAYER_INFO_URI, LEAGUE_KEY, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_PLAYER_OWNERSHIP))
        {
            uri = String.format(GET_PLAYER_OWNERSHIP_URI, LEAGUE_KEY, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_TEAM_MATCHUP))
        {
            uri = String.format(GET_TEAM_MATCHUP_URI, CURRENT_USER_TEAM_KEY, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_TEAM_PENDING_TRADES))
        {
            uri = String.format(GET_TEAM_PENDING_TRADES_URI, LEAGUE_KEY, CURRENT_USER_TEAM_KEY);
        }
        else if (aRequest.equals(BundleConstants.GET_TEAM_WAIVER_CLAIMS))
        {
            uri = String.format(GET_TEAM_WAIVER_CLAIMS_URI, LEAGUE_KEY, CURRENT_USER_TEAM_KEY);
        }
        else if (aRequest.equals(BundleConstants.GET_LEAGUE_PENDING_TRADES))
        {
            uri = String.format(GET_LEAGUE_PENDING_TRADES_URI, LEAGUE_KEY);
        }
        else if (aRequest.equals(BundleConstants.PUT_TRANSACTION) || aRequest.equals(BundleConstants.DELETE_TRANSACTION))
        {
            uri = String.format(TRANSACTION_URI, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_TEAM_INFO))
        {
            uri = String.format(GET_TEAM_INFO_URI, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_TEAM_STATS_BY_DATE))
        {
            uri = String.format(GET_TEAM_STATS_BY_DATE_URI, CURRENT_USER_TEAM_KEY, aExtraRequestParams.getString(EXTRA_REQUEST_PARAM_1));
        }
        else if (aRequest.equals(BundleConstants.GET_GAME_WEEKS))
        {
            uri = GET_GAME_WEEKS_URI;
        }

        Log.v(TAG, "getRequestUri: " + uri);
        return uri;
    }
}
