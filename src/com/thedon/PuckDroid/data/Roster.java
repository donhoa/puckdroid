package com.thedon.PuckDroid.data;

import android.content.Context;
import android.util.Log;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Roster
{
    private ArrayList<Player> mLineup = new ArrayList<Player>();
    private ArrayList<Player> mPlayers = new ArrayList<Player>();

    private ArrayList<Player> mCentres = new ArrayList<Player>();
    private ArrayList<Player> mLeftWings = new ArrayList<Player>();
    private ArrayList<Player> mRightWings = new ArrayList<Player>();
    private ArrayList<Player> mWings = new ArrayList<Player>();
    private ArrayList<Player> mForwards = new ArrayList<Player>();
    private ArrayList<Player> mDefencemen = new ArrayList<Player>();
    private ArrayList<Player> mGoalies = new ArrayList<Player>();
    private ArrayList<Player> mUtility = new ArrayList<Player>();
    private ArrayList<Player> mBN = new ArrayList<Player>();
    private ArrayList<Player> mIR = new ArrayList<Player>();

    private Preferences mPreferences;

    private int mRosterPlayersCount;

    public Roster(ArrayList<Player> aPlayersList, Preferences aPreferences)
    {
        mRosterPlayersCount = 0;
        mPreferences = aPreferences;

        for (Player player : aPlayersList)
        {
            if (player.getSelectedPosition().getPosition().equals("C"))
            {
                mCentres.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("LW"))
            {
                mLeftWings.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("RW"))
            {
                mRightWings.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("D"))
            {
                mDefencemen.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("F"))
            {
                mForwards.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("W"))
            {
                mWings.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("G"))
            {
                mGoalies.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("Util"))
            {
                mUtility.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("BN"))
            {
                mBN.add(player);
                mRosterPlayersCount++;
            }
            else if (player.getSelectedPosition().getPosition().equals("IR"))
            {
                mIR.add(player);
            }

            mPlayers.add(player);
        }

        buildRoster();
    }

    public ArrayList<Player> getLineup()
    {
        return mLineup;
    }

    public ArrayList<Player> getPlayers()
    {
        return mPlayers;
    }

    public int getRosterPlayersCount()
    {
        return mRosterPlayersCount;
    }

    private void buildRoster()
    {
        int centresCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("C"), 0);
        int leftWingsCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("LW"), 0);
        int rightWingsCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("RW"), 0);
        int wingsCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("W"), 0);
        int defencemenCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("D"), 0);
        int goaliesCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("G"), 0);
        int utilitiesCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("Util"), 0);
        int forwardsCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("F"), 0);
        int BNCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("BN"), 0);
        int IRCount = mPreferences.getInt(Utils.getPositionsPreferencesKey("IR"), 0);

        mCentres = completePositionsList(mCentres, centresCount - mCentres.size(), "C");
        mLeftWings = completePositionsList(mLeftWings, leftWingsCount - mLeftWings.size(), "LW");
        mRightWings = completePositionsList(mRightWings, rightWingsCount - mRightWings.size(), "RW");
        mWings = completePositionsList(mWings, wingsCount - mWings.size(), "RW");
        mDefencemen = completePositionsList(mDefencemen, defencemenCount - mDefencemen.size(), "D");
        mGoalies = completePositionsList(mGoalies, goaliesCount - mGoalies.size(), "G");
        mUtility = completePositionsList(mUtility, utilitiesCount - mUtility.size(), "Util");
        mForwards = completePositionsList(mForwards, forwardsCount - mForwards.size(), "F");
        mBN = completePositionsList(mBN, BNCount - mBN.size(), "BN");
        if (mIR.size() > 0 )
        {
            mIR = completePositionsList(mIR, IRCount - mIR.size(), "IR");
        }
    }

    private ArrayList<Player> completePositionsList(ArrayList<Player> aPositionsList, int aEmptySlotsCount, String aPosition)
    {
        Player emptyPlayer = new Player();
        Player.SelectedPosition selectedPosition;

        Calendar calendar = Calendar.getInstance();
        String date = Utils.getFormattedDate(calendar.getTime());

        for (int i = 0; i < aEmptySlotsCount; i++)
        {
            selectedPosition = new Player.SelectedPosition("date", date, aPosition);

            emptyPlayer.setSelectedPosition(selectedPosition);
            aPositionsList.add(emptyPlayer);
        }

        for (Player player : aPositionsList)
        {
            mLineup.add(player);
        }

        return aPositionsList;
    }
}
