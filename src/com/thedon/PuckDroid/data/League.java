package com.thedon.PuckDroid.data;

import android.util.Log;
import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class League
{
    public static final int WEEKLY_DEADLINE_TYPE_TOMORROW = 0;
    public static final int WEEKLY_DEADLINE_TYPE_TODAY = 1;

    private String mLeagueKey;
    private String mLeagueId;
    private String mName;
    private String mUrl;
    private String mDraftStatus;
    private String mNumTeams;
    private String mEditKey;
    private int mWeeklyDeadline;
    private String mLeagueUpdateTimestamp;
    private String mScoringType;
    private String mLeagueType;
    private String mIsProLeague;
    private int mCurrentWeek;
    private int mStartWeek;
    private String mStartDate;
    private int mEndWeek;
    private String mEndDate;
    private String mIsFinished;
    ArrayList<Team> mTeams;
    private LeagueSettings mLeagueSettings;

    private boolean mIsPlayoffWeek;

    public League(String aLeagueKey, String aLeagueId, String aName, String aUrl, String aDraftStatus, String aNumTeams, String aEditKey, String aWeeklyDeadlineString, String aLeagueUpdateTimestamp, String aScoringType, String aLeagueType, String aIsProLeague, String aCurrentWeekString, String aStartWeekString, String aStartDate, String aEndWeekString, String aEndDate, String aIsFinished, ArrayList<Team> aTeams, LeagueSettings aLeagueSettings)
    {
        mLeagueKey = aLeagueKey;
        mLeagueId = aLeagueId;
        mName = aName;
        mUrl = aUrl;
        mDraftStatus = aDraftStatus;
        mNumTeams = aNumTeams;
        mEditKey = aEditKey;
        mLeagueUpdateTimestamp = aLeagueUpdateTimestamp;
        mScoringType = aScoringType;
        mLeagueType = aLeagueType;
        mIsProLeague = aIsProLeague;
        mStartDate = aStartDate;
        mEndDate = aEndDate;
        mIsFinished = aIsFinished;
        mTeams = aTeams;
        mLeagueSettings = aLeagueSettings;

        try
        {
            if (!Utils.isNullOrEmptyString(aCurrentWeekString))
            {
                mCurrentWeek = Integer.parseInt(aCurrentWeekString);
            }

            if (!Utils.isNullOrEmptyString(aStartWeekString))
            {
                mStartWeek = Integer.parseInt(aStartWeekString);
            }

            if (!Utils.isNullOrEmptyString(aEndWeekString))
            {
                mEndWeek = Integer.parseInt(aEndWeekString);
            }

            if (mLeagueSettings != null && mLeagueSettings.usesPlayoff())
            {
                mIsPlayoffWeek = (mCurrentWeek >= mLeagueSettings.getPlayoffStartWeek());
                mLeagueSettings.setIsPlayoffWeek(mIsPlayoffWeek);
            }

            if (aWeeklyDeadlineString.equals("intraday"))
            {
                mWeeklyDeadline = WEEKLY_DEADLINE_TYPE_TODAY;
            }
            else
            {
                mWeeklyDeadline = WEEKLY_DEADLINE_TYPE_TOMORROW;
            }
        }
        catch (NumberFormatException e)
        {
            mIsPlayoffWeek = false;
        }
    }

    public String getLeagueKey()
    {
        return mLeagueKey;
    }

    public String getName()
    {
        return mName;
    }

    public int getCurrentWeek()
    {
        return mCurrentWeek;
    }

    public int getStartWeek()
    {
        return mStartWeek;
    }

    public int getEndWeek()
    {
        return mEndWeek;
    }

    public String getNumberOfTeams()
    {
        return mNumTeams;
    }

    public ArrayList<Team> getTeams()
    {
        return mTeams;
    }

    public int getWeeklyDeadlineType()
    {
        return mWeeklyDeadline;
    }

    public LeagueSettings getLeagueSettings()
    {
        return mLeagueSettings;
    }

    public String toString()
    {
        return "mLeagueKey: " + mLeagueKey
        + " - mLeagueId: " + mLeagueId
        + " - mName: " + mName
        + " - mUrl: " + mUrl
        + " - mDraftStatus: " + mDraftStatus
        + " - mNumTeams: " + mNumTeams
        + " - mEditKey: " + mEditKey
        + " - mWeeklyDeadline: " + mWeeklyDeadline
        + " - mLeagueUpdateTimestamp: " + mLeagueUpdateTimestamp
        + " - mScoringType: " + mScoringType
        + " - mLeagueType: " + mLeagueType
        + " - mIsProLeague: " + mIsProLeague
        + " - mStartDate: " + mStartDate
        + " - mIsFinished: " + mIsFinished;
    }
}
