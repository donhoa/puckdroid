package com.thedon.PuckDroid.data;

import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PlayerStatistics
{
    private String mCoverageType = "";
    private String mSeason = "";
    private String mDate = "";
    private Map<Integer, String> mStatsTable = new TreeMap<Integer, String>();
    private PlayerPoints mPlayerPoints;

    public PlayerStatistics(String aCoverageType, String aSeason, String aDate, Hashtable<Integer,String> aStatsTable)
    {
        mCoverageType = aCoverageType;
        mSeason = aSeason;
        mDate = aDate;

        mStatsTable = new TreeMap<Integer, String>(aStatsTable); //Convert to TreeMap so the stat categories are sorted
    }

    public void setPlayerPoints(PlayerPoints aPlayerPoints)
    {
        mPlayerPoints = aPlayerPoints;
    }

    public PlayerPoints getPlayerPoints()
    {
        return mPlayerPoints;
    }

    public Map<Integer, String> getStatsTable()
    {
        return mStatsTable;
    }

    public static class PlayerPoints
    {
        private String mPlayerPointsCoverageType = "";
        private String mPlayerPointsSeason = "";
        private String mTotal = "";

        public PlayerPoints(String aPlayerPointsCoverageType, String aPlayerPointsSeason, String aTotal)
        {
            mPlayerPointsCoverageType = aPlayerPointsCoverageType;
            mPlayerPointsSeason = aPlayerPointsSeason;
            mTotal = aTotal;
        }

        public String getTotal()
        {
            return mTotal;
        }
    }
}
