package com.thedon.PuckDroid.data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class StatisticsCategory
{
    //Skaters
    public static final int STAT_ID_GOALS = 1;
    public static final int STAT_ID_ASSISTS = 2;
    public static final int STAT_ID_POINTS = 3;
    public static final int STAT_ID_PLUS_MINUS = 4;
    public static final int STAT_ID_PIM = 5;
    public static final int STAT_ID_PPG = 6;
    public static final int STAT_ID_PPA = 7;
    public static final int STAT_ID_PPP = 8;
    public static final int STAT_ID_SHG = 9;
    public static final int STAT_ID_SHA = 10;
    public static final int STAT_ID_SHP = 11;
    public static final int STAT_ID_GWG = 12;
    public static final int STAT_ID_GTG = 13; //Game-Tying Goals
    public static final int STAT_ID_SOG = 14;
    public static final int STAT_ID_SHOOTING_PERCENTAGE = 15;
    public static final int STAT_ID_FACEOFFS_WON = 16;
    public static final int STAT_ID_FACEOFFS_LOST = 17;
    public static final int STAT_ID_F_D_GAMES = 29;
    public static final int STAT_ID_HITS = 31;
    public static final int STAT_ID_BLOCKS = 32;
    public static final int STAT_ID_SKATER_TOI = 33;
    //Goalies
    public static final int STAT_ID_GAMES_STARTED = 18;
    public static final int STAT_ID_WINS = 19;
    public static final int STAT_ID_LOSSES = 20;
    public static final int STAT_ID_TIES = 21;
    public static final int STAT_ID_GOALS_AGAINST = 22;
    public static final int STAT_ID_GAA = 23;
    public static final int STAT_ID_SHOTS_AGAINST = 24;
    public static final int STAT_ID_SAVES = 25;
    public static final int STAT_ID_SAVE_PERCENTAGE = 26;
    public static final int STAT_ID_SHUTOUTS = 27;
    public static final int STAT_ID_GOALIE_TOI = 28;
    public static final int STAT_ID_GOALIE_GAMES = 30;

    private String mStatId;
    private String mEnabled;
    private String mName;
    private String mDisplayName;
    private String mSortOrder;
    private String mPositionType;
    private String mValue;

    public StatisticsCategory(String aStatId, String aEnabled, String aName, String aDisplayName, String aSortOrder, String aPositionType)
    {
        mStatId = aStatId;
        mEnabled = aEnabled;
        mName = aName;
        mDisplayName = aDisplayName;
        mSortOrder = aSortOrder;
        mPositionType = aPositionType;
    }

    public String getStatId()
    {
        return mStatId;
    }

    public String getName()
    {
        return mName;
    }

    public String getDisplayName()
    {
        return mDisplayName;
    }

    public boolean isSkaterStat()
    {
        return mPositionType.equals("P");
    }
}
