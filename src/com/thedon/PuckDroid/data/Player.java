package com.thedon.PuckDroid.data;

import android.util.Log;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.CustomListItem;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Player implements CustomListItem
{
    private String mPlayerKey;
    private String mPlayerId;
    private String mFullName;
    private String mStatus;
    private String mEditorialPlayerKey;
    private String mEditorialTeamKey;
    private String mEditorialTeamFullName;
    private String mEditorialTeamAbbr;
    private String mUniformNumber;
    private String mDisplayPosition;
    private String mImageUrl;
    private String mHighResImageUrl;
    private boolean mIsUndroppable;
    private String mPositionType;
    private List<String> mEligiblePositions;
    private String mHasPlayerNotes;
    private String mHasRecentPlayerNotes;
    private SelectedPosition mSelectedPosition;
    private PlayerStatistics mPlayerStatistics;
    private Ownership mOwnership;

    private boolean mIsEmptyPlayer;

    public Player()
    {
        mIsEmptyPlayer = true;
    }

    public Player(String aPlayerKey, String aPlayerId, String aFullName, String aStatus, String aEditorialPlayerKey, String aEditorialTeamKey, String aEditorialTeamFullName, String aEditorialTeamAbbr, String aUniformNumber, String aDisplayPosition, String aImageUrl, String aIsUndroppableString, String aPositionType, List<String> aEligiblePositions, String aHasPlayerNotes, String aHasRecentPlayerNotes, SelectedPosition aSelectedPosition, PlayerStatistics aPlayerStatistics)
    {
        mPlayerKey = aPlayerKey;
        mPlayerId = aPlayerId;
        mFullName = aFullName;
        mStatus = aStatus;
        mEditorialPlayerKey = aEditorialPlayerKey;
        mEditorialTeamKey = aEditorialTeamKey;
        mEditorialTeamFullName = aEditorialTeamFullName;
        mEditorialTeamAbbr = aEditorialTeamAbbr;
        mUniformNumber = aUniformNumber;
        mDisplayPosition = aDisplayPosition;
        mImageUrl = aImageUrl;
        mPositionType = aPositionType;
        mEligiblePositions = aEligiblePositions;
        mHasPlayerNotes = aHasPlayerNotes;
        mHasRecentPlayerNotes = aHasRecentPlayerNotes;
        mSelectedPosition = aSelectedPosition;
        mPlayerStatistics = aPlayerStatistics;

        mIsUndroppable = aIsUndroppableString.equals("1");

        mIsEmptyPlayer = false;

        retrieveHighResImageUrl();
    }

    public String getPlayerKey()
    {
        return mPlayerKey;
    }

    public String getFullName()
    {
        return mFullName;
    }

    public String getTeamFullName()
    {
        return mEditorialTeamFullName;
    }

    public String getUniformNumber()
    {
        return "#" + mUniformNumber;
    }

    public List<String> getEligiblePositions()
    {
        return mEligiblePositions;
    }

    public String getEligiblePositionsString()
    {
        String positions = "";

        for (int i = 0; i < mEligiblePositions.size(); i++)
        {
            positions = positions + mEligiblePositions.get(i);

            if (i < mEligiblePositions.size() - 1)
            {
                positions = positions + ", ";

            }
        }
        return positions;
    }

    public String getEditorialTeamAbbr()
    {
        return mEditorialTeamAbbr;
    }

    public String getImageUrl()
    {
        return mImageUrl;
    }

    public String getHighResImageUrl()
    {
        return mHighResImageUrl;
    }

    public boolean isUndroppable()
    {
        return mIsUndroppable;
    }


    public boolean isEmptyPlayer()
    {
        return mIsEmptyPlayer;
    }

    @Override
    public boolean isHeader()
    {
        return false;
    }

    public static class SelectedPosition
    {
        String mCoverageType = "";
        String mDate = "";
        String mPosition = "";

        public SelectedPosition(String aCoverageType, String aDate, String aPosition)
        {
            mCoverageType = aCoverageType;
            mDate = aDate;
            mPosition = aPosition;
        }

        public String getPosition()
        {
            return mPosition;
        }

        public void setPosition(String aPosition)
        {
            mPosition = aPosition;
        }
    }

    public SelectedPosition getSelectedPosition()
    {
        return mSelectedPosition;
    }

    public void setSelectedPosition(SelectedPosition aSelectedPosition)
    {
        mSelectedPosition = aSelectedPosition;
    }

    public static class Ownership
    {
        String mOwnershipType = "";
        String mOwnerTeamKey = "";
        String mOwnerTeamName = "";
        String mWaiverDate = "";
        String mSimpleWaiverDate = "";
        boolean mIsFreeAgent = false;
        boolean mIsOnWaivers = false;


        public Ownership(String aOwnershipType, String aOwnerTeamKey, String aOwnerTeamName, String aWaiverDate)
        {
            mOwnershipType = aOwnershipType;
            mOwnerTeamKey = aOwnerTeamKey;
            mOwnerTeamName = aOwnerTeamName;
            mWaiverDate = aWaiverDate;

            mIsFreeAgent = mOwnershipType.equals("freeagents");
            mIsOnWaivers = mOwnershipType.equals("waivers");

            if (mIsOnWaivers)
            {
                mSimpleWaiverDate = Utils.convertToSimpleDate(mWaiverDate);
            }
        }

        public String getOwnerTeamKey()
        {
            return mOwnerTeamKey;
        }

        public String getOwnerTeamName()
        {
            return mOwnerTeamName;
        }

        public boolean isFreeAgent()
        {
            return mIsFreeAgent;
        }

        public boolean isOnWaivers()
        {
            return mIsOnWaivers;
        }

        public String getWaiverDate()
        {
            return mWaiverDate;
        }

        public String getSimpleWaiverDate()
        {
            return mSimpleWaiverDate;
        }
    }

    public Ownership getPlayerOwnership()
    {
        return mOwnership;
    }

    public void setPlayerOwnership(Ownership aOwnership)
    {
        mOwnership = aOwnership;
    }

    public PlayerStatistics getPlayerStatistics()
    {
        return mPlayerStatistics;
    }

    public String toString()
    {
        String playerString =  "mPlayerKey: "  + mPlayerKey
                + " mPlayerId: " + mPlayerId
                + " mFullName: " + mFullName
                + "mStatus: " + mStatus
                + " mEditorialPlayerKey: " + mEditorialPlayerKey
                + " mEditorialTeamKey: " + mEditorialTeamKey
                + " mEditorialTeamFullName: " + mEditorialTeamFullName
                + " mEditorialTeamAbbr: " + mEditorialTeamAbbr
                + " mUniformNumber: " + mUniformNumber
                + " mDisplayPosition: " + mDisplayPosition
                + " mImageUrl: " + mImageUrl
                + " mIsUndroppable: " + mIsUndroppable
                + " mPositionType: " + mPositionType
                + " mHasPlayerNotes: " + mHasPlayerNotes
                + " mHasRecentPlayerNotes: " + mHasRecentPlayerNotes
                + " mEligiblePositions: " + getEligiblePositionsString();

        return playerString;
    }

    private void retrieveHighResImageUrl()
    {
        if (!Utils.isNullOrEmptyString(mImageUrl))
        {
            int secondHttpIndex = mImageUrl.lastIndexOf("http://");
            mHighResImageUrl = mImageUrl.substring(secondHttpIndex);
        }
    }
}
