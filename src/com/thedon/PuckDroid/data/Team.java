package com.thedon.PuckDroid.data;

import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Team
{
    private String mTeamKey;
    private String mTeamId;
    private String mName;
    private String mIsOwnedByCurrentLogin;
    private boolean mIsCurrentUser;
    private String mUrl;
    private String mWaiverPriority;
    private String mFaabBalance;
    private int mNumberOfMoves;
    private String mNumberOfTrades;
    private RosterAdds mRosterAdds;
    private boolean mClinchedPlayoffs;
    private Logo mTeamLogo;
    private ArrayList<Manager> mManagers;
    private Points mPoints;
    private Standings mStandings;

    private ArrayList<Statistic> mTeamStats;

    public Team(String aTeamKey, String aTeamId, String aName, String aIsOwnedByCurrentLogin, String aUrl, String aWaiverPriority, String aFaabBalance, String aNumberOfMovesString, String aNumberOfTrades, RosterAdds aRosterAdds, String aClinchedPlayoffsString, Logo aTeamLogo, Points aPoints, ArrayList<Manager> aManagers, Standings aStandings, ArrayList<Statistic> aTeamStats)
    {
        mTeamKey = aTeamKey;
        mTeamId =  aTeamId;
        mName = aName;
        mIsOwnedByCurrentLogin = aIsOwnedByCurrentLogin;
        mIsCurrentUser = false;
        mUrl = aUrl;
        mWaiverPriority = aWaiverPriority;
        mFaabBalance = aFaabBalance;
        mNumberOfTrades =  aNumberOfTrades;
        mRosterAdds =  aRosterAdds;
        mTeamLogo = aTeamLogo;
        mPoints = aPoints;
        mManagers = aManagers;
        mStandings = aStandings;

        mTeamStats = aTeamStats;

        if (!Utils.isNullOrEmptyString(aNumberOfMovesString))
        {
            mNumberOfMoves = Integer.parseInt(aNumberOfMovesString);
        }
        else
        {
            mNumberOfMoves = 0;
        }

        mClinchedPlayoffs = aClinchedPlayoffsString.equals("1");

        mIsCurrentUser = mIsOwnedByCurrentLogin.equals("1");
    }

    public static class Logo
    {
        private String mSize;
        private String mUrl;

        public Logo(String aSize, String aUrl)
        {
            mSize = aSize;
            mUrl = aUrl;
        }
        public String getSize()
        {
            return mSize;
        }

        public String getUrl()
        {
            return mUrl;
        }
    }

    public static class RosterAdds
    {
        private String mCoverageType;
        private String mCoverageValue;
        private String mValue;

        public RosterAdds(String aCoverageType, String aCoverageValue, String aValue)
        {
            mCoverageType = aCoverageType;
            mCoverageValue = aCoverageValue;
            mValue = aValue;
        }
    }

    public static class Manager
    {
        private String mManagerId;
        private String mNickname;
        private String mGuid;
        private String mIsCurrentLoginString;
        private boolean mIsCurrentLogin;

        public Manager(String aManagerId, String aNickname, String aGuid, String aIsCurrentLoginString)
        {
            mManagerId = aManagerId;
            mNickname = aNickname;
            mGuid = aGuid;
            mIsCurrentLoginString = aIsCurrentLoginString;
            mIsCurrentLogin = mIsCurrentLoginString.equals("1");
        }

        public String getManagerId()
        {
            return mManagerId;
        }

        public String getNickName()
        {
            return mNickname;
        }

        public boolean isCurrentLogin()
        {
            return mIsCurrentLogin;
        }
    }

    public static class Statistic
    {
        private String mStatId;
        private String mValue;

        public Statistic(String aStatId, String aValue)
        {
            mStatId = aStatId;
            mValue = aValue;
        }

        public String getStatId()
        {
            return mStatId;
        }

        public String getValue()
        {
            return mValue;
        }
    }

    public static class Points
    {
        private String mCoverageType = "";
        private String mSeason = "";
        private String mDate = "";
        private String mWeek = "";
        public String mTotal = "";

        private ArrayList<Statistic> mTeamPointsStats;

        public Points(String aCoverageType, String aSeason, String aDate, String aWeek, String aTotal, ArrayList<Statistic> aPointsStats)
        {
            mCoverageType = aCoverageType;
            mSeason = aSeason;
            mDate = aDate;
            mWeek = aWeek;
            mTotal = aTotal;
            mTeamPointsStats = aPointsStats;
        }

        public String getTotal()
        {
            return mTotal;
        }

        public ArrayList<Statistic> getStats()
        {
            return mTeamPointsStats;
        }
    }

    public static class Standings
    {
        private String mRank;
        private String mPointsChange;
        private String mPointsBack;
        private Record mRecord;

        public Standings(String aRank, String aPointsChange, String aPointsBack, Record aRecord)
        {
            mRank = aRank;
            mPointsChange = Utils.getFormattedPoints(aPointsChange);
            mPointsBack = aPointsBack;
            mRecord = aRecord;
        }

        public String getRank()
        {
            return mRank;
        }

        public String getPointsChange()
        {
            return mPointsChange;
        }

        public String getPointsBack()
        {
            return mPointsBack;
        }

        public Record getRecord()
        {
            return mRecord;
        }

        public static class Record
        {
            private String mWins;
            private String mLosses;
            private String mTies;
            private String mPercentage;
            private String mRecord;

            public Record(String aWins, String aLosses, String aTies, String aPercentage)
            {
                mWins = aWins;
                mLosses = aLosses;
                mTies = aTies;
                mPercentage = aPercentage;

                if (!Utils.isNullOrEmptyString(mWins))
                {
                    mRecord = mWins + "-" + mLosses + "-" + mTies;
                }
            }

            public String getRecord()
            {
                return mRecord;
            }
        }
    }

    public String getTeamKey()
    {
        return mTeamKey;
    }

    public String getTeamId()
    {
        return mTeamId;
    }

    public String getName()
    {
        return mName;
    }

    public boolean isCurrentUser()
    {
        return mIsCurrentUser;
    }

    public Logo getTeamLogo()
    {
        return mTeamLogo;
    }

    public String getFaabBalance()
    {
        return mFaabBalance;
    }

    public ArrayList<Manager> getManagers()
    {
        return mManagers;
    }

    public ArrayList<Statistic> getTeamStats()
    {
        return mTeamStats;
    }

    public Points getPoints()
    {
        return mPoints;
    }

    public Standings getStandings()
    {
        return mStandings;
    }

    public String getWaiverPriority()
    {
        return mWaiverPriority;
    }

    public int getNumberOfMoves()
    {
        return mNumberOfMoves;
    }

    public String getNumberOfTrades()
    {
        return mNumberOfTrades;
    }

    public boolean clinchedPlayoffs()
    {
        return mClinchedPlayoffs;
    }
}
