package com.thedon.PuckDroid.data.parsers;

import android.util.Log;
import com.thedon.PuckDroid.data.League;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.Team;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class LeagueXmlParser extends BaseXmlParser
{
    private ArrayList<League> mLeagues = new ArrayList<League>();

    public ArrayList<League> readGames(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String code = "";
        boolean isNHL = false;

        parser.require(XmlPullParser.START_TAG, null, "game");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("code"))
            {
                code = readTag(parser, parserName);
                isNHL = code.equals("nhl");
            }
            else if (parserName.equals("leagues") && isNHL)
            {
                readLeagues(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return mLeagues;
    }

    public void readLeagues(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "leagues");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("league"))
            {
                League league = readLeague(parser);
                mLeagues.add(league);
            }
            else
            {
                skip(parser);
            }
        }
    }

    public League readLeague(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "league");
        String league_key = "";
        String league_id = "";
        String name = "";
        String url = "";
        String draft_status = "";
        String num_teams = "";
        String edit_key = "";
        String weekly_deadline = "";
        String league_update_timestamp = "";
        String scoring_type = "";
        String league_type = "";
        String is_pro_league = "";
        String current_week = "";
        String start_week = "";
        String start_date = "";
        String end_week = "";
        String end_date = "";
        String is_finished = "";
        ArrayList<Team> teams = new ArrayList<Team>();
        LeagueSettings leagueSettings = null;

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("league_key"))
            {
                league_key = readTag(parser, parserName);
            }
            else if (parserName.equals("league_id"))
            {
                league_id = readTag(parser, parserName);
            }
            else if (parserName.equals("name"))
            {
                name = readTag(parser, parserName);
            }
            else if (parserName.equals("url"))
            {
                url = readTag(parser, parserName);
            }
            else if (parserName.equals("draft_status"))
            {
                draft_status = readTag(parser, parserName);
            }
            else if (parserName.equals("num_teams"))
            {
                num_teams =readTag(parser, parserName);
            }
            else if (parserName.equals("edit_key"))
            {
                edit_key = readTag(parser, parserName);
            }
            else if (parserName.equals("weekly_deadline"))
            {
                weekly_deadline = readTag(parser, parserName);
            }
            else if (parserName.equals("league_update_timestamp"))
            {
                league_update_timestamp = readTag(parser, parserName);
            }
            else if (parserName.equals("scoring_type"))
            {
                scoring_type = readTag(parser, parserName);
            }
            else if (parserName.equals("league_type"))
            {
                league_type = readTag(parser, parserName);
            }
            else if (parserName.equals("is_pro_league"))
            {
                is_pro_league = readTag(parser, parserName);
            }
            else if (parserName.equals("current_week"))
            {
                current_week = readTag(parser, parserName);
            }
            else if (parserName.equals("start_week"))
            {
                start_week = readTag(parser, parserName);
            }
            else if (parserName.equals("start_date"))
            {
                start_date = readTag(parser, parserName);
            }
            else if (parserName.equals("end_week"))
            {
                end_week = readTag(parser, parserName);
            }
            else if (parserName.equals("end_date"))
            {
                end_date = readTag(parser, parserName);
            }
            else if (parserName.equals("is_finished"))
            {
                is_finished = readTag(parser, parserName);
            }
            else if (parserName.equals("teams"))
            {
                teams = readTeams(parser);
            }
            else if (parserName.equals("settings"))
            {
                LeagueSettingsXmlParser settingsXmlParser = new LeagueSettingsXmlParser();
                leagueSettings = settingsXmlParser.readLeagueSettings(parser);
            }
            else
            {
                skip(parser);
            }
        }
        return new League(league_key, league_id, name, url, draft_status, num_teams, edit_key, weekly_deadline, league_update_timestamp, scoring_type, league_type, is_pro_league, current_week, start_week, start_date, end_week, end_date, is_finished, teams, leagueSettings);
    }

    private ArrayList<Team> readTeams(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Team> teams = new ArrayList<Team>();

        parser.require(XmlPullParser.START_TAG, null, "teams");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("team"))
            {
                PoolTeamXmlParser teamXmlParser = new PoolTeamXmlParser();

                Team team = teamXmlParser.readTeam(parser);
                teams.add(team);
            }
        }

        return teams;
    }
}
