package com.thedon.PuckDroid.data.parsers;

import android.util.Log;
import com.thedon.PuckDroid.data.Team;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PoolTeamXmlParser extends BaseXmlParser
{
    public Team readTeam(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "team");

        String team_key = "";
        String team_id = "";
        String name = "";
        String is_owned_by_current_login = "0";
        String url = "";
        String waiver_priority = "";
        String faab_balance = "";
        String number_of_moves = "";
        String number_of_trades = "";
        Team.RosterAdds rosterAdds = null;
        String clinched_playoffs = "";
        Team.Logo teamLogo = null;
        Team.Points teamPoints = null;
        ArrayList<Team.Manager> managers = null;
        Team.Standings teamStandings = null;
        ArrayList<Team.Statistic> stats = new ArrayList<Team.Statistic>();

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("team_key"))
            {
                team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("team_id"))
            {
                team_id = readTag(parser, parserName);
            }
            else if (parserName.equals("name"))
            {
                name = readTag(parser, parserName);
            }
            else if (parserName.equals("is_owned_by_current_login"))
            {
                is_owned_by_current_login = readTag(parser, parserName);
            }
            else if (parserName.equals("url"))
            {
                url = readTag(parser, parserName);
            }
            else if (parserName.equals("team_logos"))
            {
                teamLogo = readTeamLogo(parser);
            }
            else if (parserName.equals("waiver_priority"))
            {
                waiver_priority = readTag(parser, parserName);
            }
            else if (parserName.equals("faab_balance"))
            {
                faab_balance = readTag(parser, parserName);
            }
            else if (parserName.equals("number_of_moves"))
            {
                number_of_moves = readTag(parser, parserName);
            }
            else if (parserName.equals("number_of_trades"))
            {
                number_of_trades = readTag(parser, parserName);
            }
            else if (parserName.equals("roster_adds"))
            {
                rosterAdds = readRosterAdds(parser);
            }
            else if (parserName.equals("clinched_playoffs"))
            {
                clinched_playoffs = readTag(parser, parserName);
            }
            else if (parserName.equals("team_stats"))
            {
                stats = readTeamStats(parser);
            }
            else if (parserName.equals("team_points"))
            {
                teamPoints = readTeamPoints(parser) ;
            }
            else if (parserName.equals("managers"))
            {
                managers = readManagers(parser);
            }
            else if (parserName.equals("team_standings"))
            {
                teamStandings = readTeamStandings(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return new Team(team_key, team_id, name, is_owned_by_current_login, url, waiver_priority, faab_balance, number_of_moves, number_of_trades, rosterAdds, clinched_playoffs, teamLogo, teamPoints, managers, teamStandings, stats);
    }

    protected Team.Logo readTeamLogo(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String size = "";
        String url = "";

        parser.require(XmlPullParser.START_TAG, null, "team_logos");

        parser.nextTag(); //Ignore <team_logos>

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("size"))
            {
                size = readTag(parser, parserName);
            }
            else if (parserName.equals("url"))
            {
                url = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }
        Team.Logo teamLogo = new Team.Logo(size, url);

        parser.nextTag(); //Ignore </team_logos>

        return teamLogo;
    }

    protected Team.Points readTeamPoints(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String coverage_type = "";
        String season = "";
        String date = "";
        String week = "";
        String total = "";

        ArrayList<Team.Statistic> pointsStats = new ArrayList<Team.Statistic>();

        parser.require(XmlPullParser.START_TAG, null, "team_points");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("coverage_type"))
            {
                coverage_type = readTag(parser, parserName);
            }
            else if (parserName.equals("season"))
            {
                season = readTag(parser, parserName);
            }
            else if (parserName.equals("week"))
            {
                week = readTag(parser, parserName);
            }
            else if (parserName.equals("date"))
            {
                date = readTag(parser, parserName);
            }
            else if (parserName.equals("total"))
            {
                total = readTag(parser, parserName);
            }
            else if (parserName.equals("stats"))
            {
                pointsStats = readStats(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return new Team.Points(coverage_type, season, date, week, total, pointsStats);
    }

    protected Team.RosterAdds readRosterAdds(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String coverage_type = "";
        String coverage_value = "";
        String value = "";

        parser.require(XmlPullParser.START_TAG, null, "roster_adds");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("coverage_type"))
            {
                coverage_type = readTag(parser, parserName);
            }
            else if (parserName.equals("coverage_value"))
            {
                coverage_value = readTag(parser, parserName);
            }
            else if (parserName.equals("value"))
            {
                value = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new Team.RosterAdds(coverage_type, coverage_value, value);
    }

    protected ArrayList<Team.Manager> readManagers(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Team.Manager> managers = new ArrayList<Team.Manager>();

        parser.require(XmlPullParser.START_TAG, null, "managers");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("manager"))
            {
                managers.add(readManager(parser));
            }
            else
            {
                skip(parser);
            }
        }

        return managers;
    }

    protected Team.Manager readManager(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Team.Statistic> stats = new ArrayList<Team.Statistic>();

        String manager_id = "";
        String nickname = "";
        String guid = "";
        String is_current_login = "";

        parser.require(XmlPullParser.START_TAG, null, "manager");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("manager_id"))
            {
                manager_id = readTag(parser, parserName);
            }
            else if (parserName.equals("nickname"))
            {
                nickname = readTag(parser, parserName);
            }
            else if (parserName.equals("guid"))
            {
                guid = readTag(parser, parserName);
            }
            else if (parserName.equals("is_current_login"))
            {
                is_current_login = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new Team.Manager(manager_id, nickname, guid, is_current_login);
    }

    protected ArrayList<Team.Statistic> readTeamStats(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Team.Statistic> stats = new ArrayList<Team.Statistic>();

        String coverage_type = "";
        String season = "";
        String week = "";

        parser.require(XmlPullParser.START_TAG, null, "team_stats");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("coverage_type"))
            {
                coverage_type = readTag(parser, parserName);
            }
            else if (parserName.equals("season"))
            {
                season = readTag(parser, parserName);
            }
            else if (parserName.equals("week"))
            {
                week = readTag(parser, parserName);
            }
            else if (parserName.equals("stats"))
            {
                stats = readStats(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return stats;
    }

    protected ArrayList<Team.Statistic> readStats(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Team.Statistic> stats = new ArrayList<Team.Statistic>();

        parser.require(XmlPullParser.START_TAG, null, "stats");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("stat"))
            {
                stats.add(readStat(parser));
            }
            else
            {
                skip(parser);
            }
        }

        return stats;
    }

    protected Team.Statistic readStat(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String stat_id = "";
        String value = "";

        parser.require(XmlPullParser.START_TAG, null, "stat");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("stat_id"))
            {
                stat_id = readTag(parser, parserName);
            }
            else if (parserName.equals("value"))
            {
                value = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new Team.Statistic(stat_id, value);
    }

    protected Team.Standings readTeamStandings(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String rank = "";
        String points_change = "";
        String points_back = "";
        Team.Standings.Record record = null;

        parser.require(XmlPullParser.START_TAG, null, "team_standings");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("rank"))
            {
                rank = readTag(parser, parserName);
            }
            else if (parserName.equals("points_change"))
            {
                points_change = readTag(parser, parserName);
            }
            else if (parserName.equals("points_back"))
            {
                points_back = readTag(parser, parserName);
            }
            else if (parserName.equals("outcome_totals"))
            {
                record = readOutcomeTotals(parser);
            }
            else
            {
                skip(parser);
            }
        }

        Team.Standings standings = new Team.Standings(rank, points_change, points_back, record);

        return standings;
    }

    protected Team.Standings.Record readOutcomeTotals(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String wins = "";
        String losses = "";
        String ties = "";
        String percentage = "";

        parser.require(XmlPullParser.START_TAG, null, "outcome_totals");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("wins"))
            {
                wins = readTag(parser, parserName);
            }
            else if (parserName.equals("losses"))
            {
                losses = readTag(parser, parserName);
            }
            else if (parserName.equals("ties"))
            {
                ties = readTag(parser, parserName);
            }
            else if (parserName.equals("percentage"))
            {
                percentage = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new Team.Standings.Record(wins, losses, ties, percentage);
    }
}
