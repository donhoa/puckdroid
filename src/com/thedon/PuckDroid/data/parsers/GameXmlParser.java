package com.thedon.PuckDroid.data.parsers;

import android.util.Log;
import com.thedon.PuckDroid.data.Game;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class GameXmlParser extends BaseXmlParser
{
    public Game readGame(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String game_key = "";
        String game_id = "";
        String name = "";
        String code = "";
        String type = "";
        String url = "";
        String season = "";
        Hashtable<String, Game.GameWeek> gameWeeks = null;

        parser.require(XmlPullParser.START_TAG, null, "game");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("game_key"))
            {
                game_key = readTag(parser, parserName);
            }
            else if (parserName.equals("game_id"))
            {
                game_id = readTag(parser, parserName);
            }
            else if (parserName.equals("name"))
            {
                name = readTag(parser, parserName);
            }
            else if (parserName.equals("code"))
            {
                code = readTag(parser, parserName);
            }
            else if (parserName.equals("type"))
            {
                type = readTag(parser, parserName);
            }
            else if (parserName.equals("url"))
            {
                url = readTag(parser, parserName);
            }
            else if (parserName.equals("season"))
            {
                season = readTag(parser, parserName);
            }
            else if (parserName.equals("game_weeks"))
            {
                gameWeeks = readGameWeeks(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return new Game(game_key, game_id, name, code, type, url, season, gameWeeks);
    }

    private Hashtable<String, Game.GameWeek> readGameWeeks(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        Hashtable<String, Game.GameWeek> gameWeeks = new Hashtable<String, Game.GameWeek>();

        parser.require(XmlPullParser.START_TAG, null, "game_weeks");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("game_week"))
            {
                Game.GameWeek gameWeek = readGameWeek(parser);
                gameWeeks.put(gameWeek.getWeek(), gameWeek);
            }
            else
            {
                skip(parser);
            }
        }

        return gameWeeks;
    }

    private Game.GameWeek readGameWeek(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String week = "";
        String start = "";
        String end = "";

        parser.require(XmlPullParser.START_TAG, null, "game_week");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("week"))
            {
                week = readTag(parser, parserName);
            }
            else if (parserName.equals("start"))
            {
                start = readTag(parser, parserName);
            }
            else if (parserName.equals("end"))
            {
                end = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new Game.GameWeek(week, start, end);
    }
}
