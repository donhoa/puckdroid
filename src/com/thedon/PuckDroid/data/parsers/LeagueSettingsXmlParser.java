package com.thedon.PuckDroid.data.parsers;

import android.util.Log;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.StatisticsCategory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class LeagueSettingsXmlParser extends BaseXmlParser
{
    public LeagueSettings readLeagueSettings(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String draft_type = "";
        String is_auction_draft = "";
        String scoring_type = "";
        String uses_playoff = "";
        String has_playoff_consolation_games = "";
        String playoff_start_week = "";
        String uses_playoff_reseeding = "";
        String uses_lock_eliminated_teams = "";
        String num_playoff_teams = "";
        String num_playoff_consolation_teams = "";
        String waiver_rule = "";
        String uses_faab = "";
        String waiver_time = "";
        String trade_end_date = "";
        String trade_ratify_type = "";
        String trade_reject_time = "";
        String player_pool = "";
        String can_trade_draft_picks = "";
        Hashtable<String,Integer> roster_positions = new Hashtable<String,Integer>();
        Hashtable<String, StatisticsCategory> stats = new Hashtable<String,StatisticsCategory>();
        String max_adds = "";

        parser.require(XmlPullParser.START_TAG, null, "settings");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("draft_type"))
            {
                draft_type = readTag(parser, parserName);
            }
            else if (parserName.equals("is_auction_draft"))
            {
                is_auction_draft = readTag(parser, parserName);
            }
            else if (parserName.equals("scoring_type"))
            {
                scoring_type = readTag(parser, parserName);
            }
            else if (parserName.equals("uses_playoff"))
            {
                uses_playoff = readTag(parser, parserName);
            }
            else if (parserName.equals("has_playoff_consolation_games"))
            {
                has_playoff_consolation_games = readTag(parser, parserName);
            }
            else if (parserName.equals("playoff_start_week"))
            {
                playoff_start_week = readTag(parser, parserName);
            }
            else if (parserName.equals("uses_playoff_reseeding"))
            {
                uses_playoff_reseeding = readTag(parser, parserName);
            }
            else if (parserName.equals("uses_lock_eliminated_teams"))
            {
                uses_lock_eliminated_teams = readTag(parser, parserName);
            }
            else if (parserName.equals("num_playoff_teams"))
            {
                num_playoff_teams = readTag(parser, parserName);
            }
            else if (parserName.equals("num_playoff_consolation_teams"))
            {
                num_playoff_consolation_teams = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_rule"))
            {
                waiver_rule = readTag(parser, parserName);
            }
            else if (parserName.equals("uses_faab"))
            {
                uses_faab = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_time"))
            {
                waiver_time = readTag(parser, parserName);
            }
            else if (parserName.equals("trade_end_date"))
            {
                trade_end_date = readTag(parser, parserName);
            }
            else if (parserName.equals("trade_ratify_type"))
            {
                trade_ratify_type = readTag(parser, parserName);
            }
            else if (parserName.equals("trade_reject_time"))
            {
                trade_reject_time = readTag(parser, parserName);
            }
            else if (parserName.equals("player_pool"))
            {
                player_pool = readTag(parser, parserName);
            }
            else if (parserName.equals("can_trade_draft_picks"))
            {
                can_trade_draft_picks = readTag(parser, parserName);
            }
            else if (parserName.equals("roster_positions"))
            {
                roster_positions = readRosterPositions(parser);
            }
            else if (parserName.equals("stat_categories"))
            {
                stats = readStats(parser);
            }
            else if (parserName.equals("max_adds"))
            {
                max_adds = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new LeagueSettings(draft_type, is_auction_draft, scoring_type, uses_playoff, has_playoff_consolation_games, playoff_start_week, uses_playoff_reseeding, uses_lock_eliminated_teams, num_playoff_teams, num_playoff_consolation_teams, waiver_rule, uses_faab, waiver_time, trade_end_date, trade_ratify_type, trade_reject_time, player_pool, can_trade_draft_picks, roster_positions, stats, max_adds);
    }

    public Hashtable<String,Integer> readRosterPositions(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        Hashtable<String,Integer> positions = new Hashtable<String,Integer>();

        parser.require(XmlPullParser.START_TAG, null, "roster_positions");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("roster_position"))
            {
                String[] position = readRosterPosition(parser);
                positions.put(position[0], Integer.parseInt(position[1]));
            }
            else
            {
                skip(parser);
            }
        }

        return positions;
    }

    public String[] readRosterPosition(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String position = "";
        String count = "";

        parser.require(XmlPullParser.START_TAG, null, "roster_position");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("position"))
            {
                position = readTag(parser, parserName);
            }
            else if (parserName.equals("count"))
            {
                count = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new String[] {position, count};
    }

    public Hashtable<String,StatisticsCategory> readStats(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        Hashtable<String,StatisticsCategory> stats = new Hashtable<String,StatisticsCategory>();

        parser.require(XmlPullParser.START_TAG, null, "stat_categories");

        parser.nextTag(); //ignore <stat_categories>

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("stat"))
            {
                StatisticsCategory stat = readStat(parser);
                stats.put(stat.getStatId(), stat);
            }
            else
            {
                skip(parser);
            }
        }

        parser.nextTag();  //ignore </stat_categories>

        return stats;
    }

    public StatisticsCategory readStat(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String stat_id = "";
        String enabled = "";
        String name = "";
        String display_name = "";
        String sort_order = "";
        String position_type = "";

        parser.require(XmlPullParser.START_TAG, null, "stat");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("stat_id"))
            {
                stat_id = readTag(parser, parserName);
            }
            else if (parserName.equals("enabled"))
            {
                enabled = readTag(parser, parserName);
            }
            else if (parserName.equals("name"))
            {
                name = readTag(parser, parserName);
            }
            else if (parserName.equals("display_name"))
            {
                display_name = readTag(parser, parserName);
            }
            else if (parserName.equals("sort_order"))
            {
                sort_order = readTag(parser, parserName);
            }
            else if (parserName.equals("position_type"))
            {
                position_type = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new StatisticsCategory(stat_id, enabled, name, display_name, sort_order, position_type);
    }
}
