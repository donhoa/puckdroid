package com.thedon.PuckDroid.data.parsers;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public abstract class BaseXmlParser
{
    private static final String ns = null;

    protected String readTag(XmlPullParser parser, String aTag) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, ns, aTag);
        String tag = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, aTag);
        return tag;
    }

    protected String readText(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String result = "";
        if(parser.next() == XmlPullParser.TEXT)
        {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }

    protected void skip(XmlPullParser parser) throws XmlPullParserException, IOException
    {
        if (parser.getEventType() != XmlPullParser.START_TAG)
        {
            throw new IllegalStateException();
        }
        int depth = 1;

        while (depth != 0)
        {
            switch (parser.next())
            {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }
    }
}
