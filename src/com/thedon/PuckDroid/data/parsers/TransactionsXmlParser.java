package com.thedon.PuckDroid.data.parsers;

import android.util.Log;
import com.thedon.PuckDroid.data.Transaction;
import com.thedon.PuckDroid.data.WaiverTransaction;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class TransactionsXmlParser extends BaseXmlParser
{
    private static final String TAG = TransactionsXmlParser.class.getSimpleName();

    public Transaction readTransaction(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Transaction.Player> players = new ArrayList<Transaction.Player>();

        parser.require(XmlPullParser.START_TAG, null, "transaction");

        String transaction_key = "";
        String transaction_id = "";
        String type = "";
        String status = "";
        String timestamp = "";
        String faab_bid = "";

        //Trades
        String trader_team_key = "";
        String trader_team_name = "";
        String tradee_team_key = "";
        String tradee_team_name = "";
        String trade_proposed_time = "";
        String tradee_unused_roster_spots = "";
        String current_login_can_respond = "";
        String trade_roster_reflect_key = "";
        String trade_note = "";

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("transaction_key"))
            {
                transaction_key = readTag(parser, parserName);
            }
            else if (parserName.equals("transaction_id"))
            {
                transaction_id = readTag(parser, parserName);
            }
            else if (parserName.equals("type"))
            {
                type = readTag(parser, parserName);
            }
            else if (parserName.equals("status"))
            {
                status = readTag(parser, parserName);
            }
            else if (parserName.equals("timestamp"))
            {
                timestamp = readTag(parser, parserName);
            }
            else if (parserName.equals("faab_bid"))
            {
                faab_bid = readTag(parser, parserName);
            }
            else if (parserName.equals("trader_team_key"))
            {
                trader_team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("trader_team_name"))
            {
                trader_team_name = readTag(parser, parserName);
            }
            else if (parserName.equals("tradee_team_key"))
            {
                tradee_team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("tradee_team_name"))
            {
                tradee_team_name = readTag(parser, parserName);
            }
            else if (parserName.equals("trade_proposed_time"))
            {
                trade_proposed_time = readTag(parser, parserName);
            }
            else if (parserName.equals("tradee_unused_roster_spots"))
            {
                tradee_unused_roster_spots = readTag(parser, parserName);
            }
            else if (parserName.equals("current_login_can_respond"))
            {
                current_login_can_respond = readTag(parser, parserName);
            }
            else if (parserName.equals("trade_roster_reflect_key"))
            {
                trade_roster_reflect_key = readTag(parser, parserName);
            }
            else if (parserName.equals("trade_note"))
            {
                trade_note = readTag(parser, parserName);
            }
            else if (parserName.equals("players"))
            {
                players = readPlayers(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return new Transaction(transaction_key, transaction_id, type, status, timestamp, faab_bid, trader_team_key, trader_team_name, tradee_team_key, tradee_team_name, trade_proposed_time, tradee_unused_roster_spots, current_login_can_respond, trade_roster_reflect_key, trade_note, players);
    }

    public WaiverTransaction readWaiverTransaction(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Transaction.Player> players = new ArrayList<Transaction.Player>();

        parser.require(XmlPullParser.START_TAG, null, "transaction");

        String transaction_key = "";
        String type = "";
        String status = "";
        String waiver_player_key = "";
        String waiver_team_key = "";
        String waiver_team_name = "";
        String waiver_date = "";
        String waiver_priority = "";
        ArrayList<String> waiver_priority_options = new ArrayList<String>();
        String faab_bid = "";

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("transaction_key"))
            {
                transaction_key = readTag(parser, parserName);
            }
            else if (parserName.equals("type"))
            {
                type = readTag(parser, parserName);
            }
            else if (parserName.equals("status"))
            {
                status = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_player_key"))
            {
                waiver_player_key = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_team_key"))
            {
                waiver_team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_team_name"))
            {
                waiver_team_name = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_date"))
            {
                waiver_date = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_priority"))
            {
                waiver_priority = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_priority_options"))
            {
                waiver_priority_options = readWaiverPriorityOptions(parser);
            }
            else if (parserName.equals("faab_bid"))
            {
                faab_bid = readTag(parser, parserName);
            }
            else if (parserName.equals("players"))
            {
                players = readPlayers(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return new WaiverTransaction(transaction_key, type, status, waiver_player_key, waiver_team_key, waiver_team_name, waiver_date, waiver_priority, waiver_priority_options, faab_bid, players);
    }

    private ArrayList<Transaction.Player> readPlayers(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Transaction.Player> players = new ArrayList<Transaction.Player>();

        parser.require(XmlPullParser.START_TAG, null, "players");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("player"))
            {
                players.add(readPlayer(parser));
            }
            else
            {
                skip(parser);
            }
        }

        Log.v(TAG, "readPlayers() SIZE: " + players.size());

        return players;
    }

    private Transaction.Player readPlayer(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        Log.v(TAG, "readPlayer()");

        parser.require(XmlPullParser.START_TAG, null, "player");

        String player_key = "";
        String player_id = "";
        Transaction.Player.Name name = null;
        String editorial_team_abbr = "";
        String display_position = "";
        String position_type = "";
        Transaction.Player.TransactionData data = null;

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("player_key"))
            {
                player_key = readTag(parser, parserName);
            }
            else if (parserName.equals("player_id"))
            {
                player_id = readTag(parser, parserName);
            }
            else if (parserName.equals("name"))
            {
                name = readName(parser);
            }
            else if (parserName.equals("editorial_team_abbr"))
            {
                editorial_team_abbr = readTag(parser, parserName);
            }
            else if (parserName.equals("display_position"))
            {
                display_position = readTag(parser, parserName);
            }
            else if (parserName.equals("position_type"))
            {
                position_type = readTag(parser, parserName);
            }
            else if (parserName.equals("transaction_data"))
            {
                data = readTransactionData(parser);
            }
        }

        return new Transaction.Player(player_key, player_id, name, editorial_team_abbr, display_position, position_type, data);
    }

    private Transaction.Player.Name readName(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "name");

        String fullName = "";
        String firstName = "";
        String lastName = "";

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("full"))
            {
                fullName = readTag(parser, parserName);
            }
            else if (parserName.equals("first"))
            {
                firstName = readTag(parser, parserName);
            }
            else if (parserName.equals("last"))
            {
                lastName = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new Transaction.Player.Name(fullName, firstName, lastName);
    }

    private Transaction.Player.TransactionData readTransactionData(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "transaction_data");

        String type = "";
        String source_type = "";
        String destination_type = "";
        String destination_team_key = "";
        String destination_team_name = "";
        String source_team_key = "";
        String source_team_name = "";

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("type"))
            {
                type = readTag(parser, parserName);
            }
            else if (parserName.equals("source_type"))
            {
                source_type = readTag(parser, parserName);
            }
            else if (parserName.equals("destination_type"))
            {
                destination_type = readTag(parser, parserName);
            }
            else if (parserName.equals("destination_team_key"))
            {
                destination_team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("destination_team_name"))
            {
                destination_team_name = readTag(parser, parserName);
            }
            else if (parserName.equals("source_team_key"))
            {
                source_team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("source_team_name"))
            {
                source_team_name = readTag(parser, parserName);
            }
        }

        Transaction.Player.TransactionData data = new Transaction.Player.TransactionData(type, source_type, destination_type, destination_team_key, destination_team_name, source_team_key, source_team_name);

        return data;
    }

    private ArrayList<String> readWaiverPriorityOptions(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<String> waiverPriorityOptions = new ArrayList<String>();

        parser.require(XmlPullParser.START_TAG, null, "waiver_priority_options");

        String option = "";

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("option"))
            {
                option = readTag(parser, parserName);
                waiverPriorityOptions.add(option);
            }
            else
            {
                skip(parser);
            }
        }

        return waiverPriorityOptions;
    }
}
