package com.thedon.PuckDroid.data.parsers;

import android.util.Log;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.data.PlayerStatistics;
import com.thedon.PuckDroid.data.StatisticsCategory;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PlayersXmlParser extends BaseXmlParser
{
    public Player readPlayer(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "player");
        String player_key = "";
        String player_id = "";
        String fullName = "";
        String status = "";
        String editorial_player_key = "";
        String editorial_team_key = "";
        String editorial_team_full_name = "";
        String editorial_team_abbr = "";
        String uniform_number = "";
        String display_position = "";
        String image_url = "";
        String is_undroppable = "";
        String position_type = "";
        String has_player_notes = "0";
        String has_recent_player_notes = "0";
        List<String> eligible_positions = null;
        Player.SelectedPosition selectedPosition = null;
        PlayerStatistics playerStatistics = null;
        PlayerStatistics.PlayerPoints playerPoints = null;
        Player.Ownership playerOwnership = null;

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("player_key"))
            {
                player_key = readTag(parser, parserName);
            }
            else if (parserName.equals("player_id"))
            {
                player_id = readTag(parser, parserName);
            }
            else if (parserName.equals("name"))
            {
                fullName = readName(parser);
            }
            else if (parserName.equals("status"))
            {
                status = readTag(parser, parserName);
            }
            else if (parserName.equals("editorial_player_key"))
            {
                editorial_player_key = readTag(parser, parserName);
            }
            else if (parserName.equals("editorial_team_key"))
            {
                editorial_team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("editorial_team_full_name"))
            {
                editorial_team_full_name = readTag(parser, parserName);
            }
            else if (parserName.equals("editorial_team_abbr"))
            {
                editorial_team_abbr = readTag(parser, parserName);
            }
            else if (parserName.equals("uniform_number"))
            {
                uniform_number = readTag(parser, parserName);
            }
            else if (parserName.equals("display_position"))
            {
                display_position = readTag(parser, parserName);
            }
            else if (parserName.equals("image_url"))
            {
                image_url = readTag(parser, parserName);
            }
            else if (parserName.equals("is_undroppable"))
            {
                is_undroppable = readTag(parser, parserName);
            }
            else if (parserName.equals("position_type"))
            {
                position_type = readTag(parser, parserName);
            }
            else if (parserName.equals("eligible_positions"))
            {
                eligible_positions = readEligiblePositions(parser);
            }
            else if (parserName.equals("has_player_notes"))
            {
                has_player_notes = readTag(parser, parserName);
            }
            else if (parserName.equals("has_recent_player_notes"))
            {
                has_recent_player_notes = readTag(parser, parserName);
            }
            else if (parserName.equals("selected_position"))
            {
                selectedPosition = readPlayerSelectedPosition(parser);
            }
            else if (parserName.equals("player_stats"))
            {
                playerStatistics = readPlayerStats(parser);
            }
            else if (parserName.equals("player_points"))
            {
                playerPoints = readPlayerPoints(parser);
            }
            else if (parserName.equals("ownership"))
            {
                playerOwnership = readOwnership(parser);
            }
            else
            {
                skip(parser);
            }
        }

        if (playerStatistics != null)
        {
            playerStatistics.setPlayerPoints(playerPoints);
        }

        Player player = new Player(player_key, player_id, fullName, status, editorial_player_key, editorial_team_key, editorial_team_full_name, editorial_team_abbr, uniform_number, display_position, image_url, is_undroppable, position_type, eligible_positions, has_player_notes, has_recent_player_notes, selectedPosition, playerStatistics);
        player.setPlayerOwnership(playerOwnership);

        return player;
    }

    public String readName(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String fullName = "";
        parser.require(XmlPullParser.START_TAG, null, "name");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();
            if (parserName.equals("full"))
            {
                fullName = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return fullName;
    }

    public List<String> readEligiblePositions(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        List<String> positions = new ArrayList<String>();

        parser.require(XmlPullParser.START_TAG, null, "eligible_positions");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("position"))
            {
                positions.add(readTag(parser, parserName));
            }
            else
            {
                skip(parser);
            }
        }

        positions.add("BN");

        return positions;
    }

    public Player.SelectedPosition readPlayerSelectedPosition(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String coverage_type = "";
        String date = "";
        String position = "";

        parser.require(XmlPullParser.START_TAG, null, "selected_position");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("coverage_type"))
            {
                coverage_type = readTag(parser, parserName);
            }
            else if (parserName.equals("date"))
            {
                date = readTag(parser, parserName);
            }
            else if (parserName.equals("position"))
            {
                position = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        Player.SelectedPosition selectedPosition = new Player.SelectedPosition(coverage_type, date, position);

        return selectedPosition;
    }

    public PlayerStatistics readPlayerStats(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String coverage_type = "";
        String season = "";
        String date = "";
        Hashtable<Integer,String> stats = new Hashtable<Integer,String>();

        parser.require(XmlPullParser.START_TAG, null, "player_stats");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();
            if (parserName.equals("coverage_type"))
            {
                coverage_type = readTag(parser, parserName);
            }
            else if (parserName.equals("season"))
            {
                season = readTag(parser, parserName);
            }
            else if (parserName.equals("date"))
            {
                date = readTag(parser, parserName);
            }
            else if (parserName.equals("stats"))
            {
                stats = readStats(parser);
            }
            else
            {
                skip(parser);
            }
        }

        PlayerStatistics statistics = new PlayerStatistics(coverage_type, season, date, stats);

        return statistics;
    }

    public Hashtable<Integer,String> readStats(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        Hashtable<Integer,String> statsTable = new Hashtable<Integer,String>();
        String[] stat;

        parser.require(XmlPullParser.START_TAG, null, "stats");

        int statId;

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();
            if (parserName.equals("stat"))
            {
                stat = readSingleStat(parser);

                if (stat != null)
                {
                    statId = Integer.parseInt(stat[0]);
                    statsTable.put(statId, stat[1]);
                }
            }
            else
            {
                skip(parser);
            }
        }

        return statsTable;
    }

    public String[] readSingleStat(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String stat_id = "";
        String value = "";

        parser.require(XmlPullParser.START_TAG, null, "stat");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();
            if (parserName.equals("stat_id"))
            {
                stat_id = readTag(parser, parserName);
            }
            else if (parserName.equals("value"))
            {
                value = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new String[]{stat_id, value};
    }

    public PlayerStatistics.PlayerPoints readPlayerPoints(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String coverage_type = "";
        String season = "";
        String total = "";

        parser.require(XmlPullParser.START_TAG, null, "player_points");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();
            if (parserName.equals("coverage_type"))
            {
                coverage_type = readTag(parser, parserName);
            }
            else if (parserName.equals("season"))
            {
                season = readTag(parser, parserName);
            }
            else if (parserName.equals("total"))
            {
                total = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        PlayerStatistics.PlayerPoints playerPoints = new PlayerStatistics.PlayerPoints(coverage_type, season, total);
        return playerPoints;
    }

    public Player.Ownership readOwnership(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        String ownership_type = "";
        String owner_team_key = "";
        String owner_team_name = "";
        String waiver_date = "";

        parser.require(XmlPullParser.START_TAG, null, "ownership");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();
            if (parserName.equals("ownership_type"))
            {
                ownership_type = readTag(parser, parserName);
            }
            else if (parserName.equals("owner_team_key"))
            {
                owner_team_key = readTag(parser, parserName);
            }
            else if (parserName.equals("owner_team_name"))
            {
                owner_team_name = readTag(parser, parserName);
            }
            else if (parserName.equals("waiver_date"))
            {
                waiver_date = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        Player.Ownership ownership = new Player.Ownership(ownership_type, owner_team_key, owner_team_name, waiver_date);

        return ownership;
    }
}
