package com.thedon.PuckDroid.data.parsers;

import android.util.Log;
import com.thedon.PuckDroid.data.Matchup;
import com.thedon.PuckDroid.data.Team;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class MatchupXmlParser extends PoolTeamXmlParser
{
    private static final String TAG = MatchupXmlParser.class.getSimpleName();

    public Matchup readMatchup(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "matchup");

        String week = "";
        String week_start = "";
        String week_end = "";
        String status = "";
        String is_playoffs = "";
        String is_consolation = "";
        ArrayList<Team> teams = null;

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("week"))
            {
                week = readTag(parser, parserName);
            }
            else if (parserName.equals("week_start"))
            {
                week_start = readTag(parser, parserName);
            }
            else if (parserName.equals("week_end"))
            {
                week_end = readTag(parser, parserName);
            }
            else if (parserName.equals("status"))
            {
                status = readTag(parser, parserName);
            }
            else if (parserName.equals("is_playoffs"))
            {
                is_playoffs = readTag(parser, parserName);
            }
            else if (parserName.equals("is_consolation"))
            {
                is_consolation = readTag(parser, parserName);
            }
            else if (parserName.equals("teams"))
            {
                teams = readTeams(parser);
            }
            else
            {
                skip(parser);
            }
        }

        return new Matchup(week, week_start, week_end, status, is_playoffs, is_consolation, teams);
    }

    private ArrayList<Team> readTeams(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        ArrayList<Team> teams = new ArrayList<Team>();

        parser.require(XmlPullParser.START_TAG, null, "teams");

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();
            if (parserName.equals("team"))
            {
                Team team = readTeam(parser);

                teams.add(team);
            }
        }

        return teams;
    }
}
