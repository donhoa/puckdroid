package com.thedon.PuckDroid.data.parsers;

import com.thedon.PuckDroid.data.YahooApiError;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class YahooApiErrorXmlParser extends BaseXmlParser
{
    public YahooApiError readError(XmlPullParser parser) throws IOException, XmlPullParserException
    {
        parser.require(XmlPullParser.START_TAG, null, "error");

        String description = "";

        while (parser.next() != XmlPullParser.END_TAG)
        {
            if (parser.getEventType() != XmlPullParser.START_TAG)
            {
                continue;
            }

            String parserName = parser.getName();

            if (parserName.equals("description"))
            {
                description = readTag(parser, parserName);
            }
            else
            {
                skip(parser);
            }
        }

        return new YahooApiError(description);
    }
}
