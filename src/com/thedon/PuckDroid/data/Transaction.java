package com.thedon.PuckDroid.data;

import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Transaction
{
    public static final int TRANSACTION_PROPOSED_STATE = 0;
    public static final int TRANSACTION_ACCEPTED_STATE = 1;
    public static final int TRANSACTION_VETOED_STATE = 2;

    protected String mTransactionKey;
    protected String mTransactionId;
    protected String mType;
    protected String mStatus;
    protected String mTimestamp;
    protected String mFaabBid;
    protected boolean mIsFaabBid;
    protected boolean mIsTrade;
    protected boolean mIsPendingTrade;
    protected int mState;

    protected String mTraderTeamKey;
    protected String mTraderTeamName;
    protected String mTradeeTeamKey;
    protected String mTradeeTeamName;
    protected String mTradeProposedTime;
    protected String mTradeeUnusedRosterSpots;
    protected String mCurrentLoginCanRespond;
    protected String mTradeRosterReflectKey;
    protected String mTradeNote;

    protected ArrayList<Player> mPlayers = new ArrayList<Player>();

    protected ArrayList<Player> mTraderPlayers = new ArrayList<Player>();
    protected ArrayList<Player> mTradeePlayers = new ArrayList<Player>();

    //Formatted strings to display in views
    protected String mTraderPlayersString;
    protected String mTradeePlayersString;

    public Transaction()
    {
    }

    public Transaction(String aTransactionKey, String aTransactionId, String aType, String aStatus, String aTimestamp, String aFaabBid, String aTraderTeamKey, String aTraderTeamName, String aTradeeTeamKey, String aTradeeTeamName, String aTradePoposedTime, String aTradeeUnusedRosterSpots, String aCurrentLoginCanRespond, String aTradeRosterReflectKey, String aTradeNote, ArrayList<Player> aPlayers)
    {
        mTransactionKey = aTransactionKey;
        mTransactionId = aTransactionId;
        mType = aType;
        mStatus = aStatus;
        mTimestamp = aTimestamp;
        mFaabBid = aFaabBid;

        mTraderTeamKey = aTraderTeamKey;
        mTraderTeamName = aTraderTeamName;
        mTradeeTeamKey = aTradeeTeamKey;
        mTradeeTeamName = aTradeeTeamName;

        mTradeProposedTime = aTradePoposedTime;
        mTradeeUnusedRosterSpots = aTradeeUnusedRosterSpots;
        mCurrentLoginCanRespond = aCurrentLoginCanRespond;
        mTradeRosterReflectKey = aTradeRosterReflectKey;
        mTradeNote = aTradeNote;

        mPlayers = aPlayers;

        mIsTrade = mType.equals("trade");
        mIsPendingTrade = mType.equals("pending_trade");

        if (mStatus.equals("proposed"))
        {
            mState = TRANSACTION_PROPOSED_STATE;

        }
        else if (mStatus.equals("accepted"))
        {
            mState = TRANSACTION_ACCEPTED_STATE;
        }
        else if (mStatus.equals("vetoed"))
        {
            mState = TRANSACTION_VETOED_STATE;
        }

        mIsFaabBid = !Utils.isNullOrEmptyString(mFaabBid);

        if (mIsTrade || mIsPendingTrade)
        {
            setTradedPlayersLists();
        }
    }

    public String getTransactionKey()
    {
        return mTransactionKey;
    }

    public String getType()
    {
        return mType;
    }

    public boolean isTrade()
    {
        return mIsTrade;
    }

    public boolean isFaabBid()
    {
        return mIsFaabBid;
    }

    public String getFaabBid()
    {
        return mFaabBid;
    }

    public String getTraderTeamKey()
    {
        return mTraderTeamKey;
    }

    public String getTraderTeamName()
    {
        return mTraderTeamName;
    }

    public String getTradeeTeamKey()
    {
        return mTradeeTeamKey;
    }

    public int getState()
    {
        return mState;
    }

    private void setTradedPlayersLists()
    {
        mTraderPlayersString = "";
        mTradeePlayersString = "";

        for (Player player : mPlayers)
        {
            if (player.getTransactionData().getSourceTeamKey().equals(mTraderTeamKey))
            {
                mTraderPlayers.add(player);
                mTraderPlayersString = mTraderPlayersString + player.getName().getFullName() + "\n";
            }
            else
            {
                mTradeePlayers.add(player);
                mTradeePlayersString = mTradeePlayersString + player.getName().getFullName() + "\n";
            }
        }
    }

    public long getTimeStamp()
    {
        return Long.parseLong(mTimestamp);
    }

    public ArrayList<Player> getTraderPlayers()
    {
        return mTraderPlayers;
    }

    public ArrayList<Player> getTradeePlayers()
    {
        return mTradeePlayers;
    }

    public String getTraderPlayersString()
    {
        return mTraderPlayersString;
    }

    public String getTradeePlayersString()
    {
        return mTradeePlayersString;
    }

    public String getTradeeTeamName()
    {
        return mTradeeTeamName;
    }

    public String getTradeNote()
    {
        return mTradeNote;
    }

    public ArrayList<Player> getPlayers()
    {
        return mPlayers;
    }

    public static class Player
    {
        String mPlayerKey;
        String mPlayerId;
        Name mName;
        String mEditorialTeamAbbr;
        String mDisplayPosition;
        String mPositionType;
        TransactionData mTransactionData;

        public Player(String aPlayerKey, String aPlayerId, Name aName, String aEditorialTeamAbbr, String aDisplayPosition, String aPositionType, TransactionData aTransactionData)
        {
            mPlayerKey = aPlayerKey;
            mPlayerId = aPlayerId;
            mName = aName;
            mEditorialTeamAbbr = aEditorialTeamAbbr;
            mDisplayPosition = aDisplayPosition;
            mPositionType = aPositionType;
            mTransactionData = aTransactionData;
        }

        public TransactionData getTransactionData()
        {
            return mTransactionData;
        }

        public Name getName()
        {
            return mName;
        }

        public static class Name
        {
            String mFullName;
            String mFirstName;
            String mLastName;

            public Name(String aFullName, String aFirstName, String aLastName)
            {
                mFullName = aFullName;
                mFirstName = aFirstName;
                mLastName = aLastName;
            }

            public String getFullName()
            {
                return mFullName;
            }

            public String getLastName()
            {
                return mLastName;
            }
        }

        public static class TransactionData
        {
            String mType;
            String mSourceType;
            String mDestinationType;
            String mDestinationTeamKey;
            String mDestinationTeamName;
            String mSourceTeamKey;
            String mSourceTeamName;
            boolean mIsPlayerAdded;

            public TransactionData(String aType, String aSourceType, String aDestinationType, String aDestinationTeamKey, String aDestinationTeamName, String aSourceTeamKey, String aSourceTeamName)
            {
                mType = aType;
                mSourceType = aSourceType;
                mDestinationType = aDestinationType;
                mDestinationTeamKey = aDestinationTeamKey;
                mDestinationTeamName = aDestinationTeamName;
                mSourceTeamKey = aSourceTeamKey;
                mSourceTeamName = aSourceTeamName;

                mIsPlayerAdded = mType.equals("add");
            }

            public String getSourceTeamKey()
            {
                return mSourceTeamKey;
            }

            public String getSourceTeamName()
            {
                return mSourceTeamName;
            }

            public String getDestinationTeamKey()
            {
                return mDestinationTeamKey;
            }

            public String getDestinationTeamName()
            {
                return mDestinationTeamName;
            }

            public boolean isPlayerAdded()
            {
                return mIsPlayerAdded;
            }
        }
    }
}
