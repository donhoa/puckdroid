package com.thedon.PuckDroid.data;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class WaiverTransaction extends Transaction
{
    protected String mWaiverPlayerKey;
    protected String mWaiverTeamKey;
    protected String mWaiverTeamName;
    protected String mWaiverDate;
    protected String mWaiverPriority;
    protected ArrayList<String> mWaiverPriorityOptions = new ArrayList<String>();

    public WaiverTransaction(String aTransactionKey, String aType, String aStatus, String aWaiverPlayerKey, String aWaiverTeamKey, String aWaiverTeamName, String aWaiverDate, String aWaiverPriority,  ArrayList<String> aWaiverPriorityOptions, String aFaabBid, ArrayList<Player> aPlayers)
    {
        mTransactionKey = aTransactionKey;
        mType = aType;
        mStatus = aStatus;
        mWaiverPlayerKey = aWaiverPlayerKey;
        mWaiverTeamKey = aWaiverTeamKey;
        mWaiverTeamName = aWaiverTeamName;
        mWaiverDate = aWaiverDate;
        mWaiverPriority = aWaiverPriority;
        mWaiverPriorityOptions = aWaiverPriorityOptions;
        mFaabBid = aFaabBid;
        mPlayers = aPlayers;
    }

    public String getWaiverPriority()
    {
        return mWaiverPriority;
    }

    public void setWaiverPriority(String aWaiverPriority)
    {
        mWaiverPriority = aWaiverPriority;
    }

    public String getWaiverDate()
    {
        return mWaiverDate;
    }

    public ArrayList<String> getWaiverPriorityOptions()
    {
        return mWaiverPriorityOptions;
    }
}
