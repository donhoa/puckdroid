package com.thedon.PuckDroid.data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class YahooApiError
{
    private String mDescription;

    public YahooApiError(String aDescription)
    {
        mDescription = aDescription;
    }

    public String getDescription()
    {
        return mDescription;
    }
}
