package com.thedon.PuckDroid.data;

import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Matchup
{

    private int mWeek;
    private String mWeekStart;
    private String mWeekStartSimple;
    private String mWeekEnd;
    private String mWeekEndSimple;
    private String mStatus;
    private boolean mIsPlayoffs;
    private boolean mIsConsolation;
    ArrayList<Team> mTeams;

    public Matchup(String aWeekString, String aWeekStart, String aWeekEnd, String aStatus, String aIsPlayoffsString, String aIsConsolationString, ArrayList<Team> aTeams)
    {
        mWeek = Integer.parseInt(aWeekString);
        mWeekStart = aWeekStart;
        mWeekEnd = aWeekEnd;
        mStatus = aStatus;
        mIsPlayoffs = aIsPlayoffsString.equals("1");
        mIsConsolation = aIsConsolationString.equals("1");
        mTeams = aTeams;

        mWeekStartSimple = Utils.convertToSimpleDate(mWeekStart);
        mWeekEndSimple = Utils.convertToSimpleDate(mWeekEnd);
    }

    public boolean isPlayoffs()
    {
        return mIsPlayoffs;
    }

    public boolean isConsolation()
    {
        return mIsConsolation;
    }

    public int getWeek()
    {
        return mWeek;
    }

    public String getWeekStartSimple()
    {
        return mWeekStartSimple;
    }

    public String getWeekEndSimple()
    {
        return mWeekEndSimple;
    }

    public ArrayList<Team> getTeams()
    {
        return mTeams;
    }
}
