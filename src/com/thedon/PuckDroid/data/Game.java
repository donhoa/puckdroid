package com.thedon.PuckDroid.data;

import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Game
{
    private String mGameKey;
    private String mGameId;
    private String mName;
    private String mCode;
    private String mType;
    private String mUrl;
    private String mSeason;
    private Hashtable<String, Game.GameWeek> mGameWeeks = new Hashtable<String, Game.GameWeek>();

    public Game(String aGameKey, String aGameId, String aName, String aCode, String aType, String aUrl, String aSeason, Hashtable<String, GameWeek> aGameWeeks)
    {
        mGameKey = aGameKey;
        mGameId = aGameId;
        mName = aName;
        mCode = aCode;
        mType = aType;
        mUrl = aUrl;
        mSeason = aSeason;
        mGameWeeks = aGameWeeks;
    }

    public Hashtable<String, Game.GameWeek> getGameWeeks()
    {
        return mGameWeeks;
    }

    public static class GameWeek
    {
        private String mWeek;
        private String mStart;
        private String mStartSimple;
        private String mEnd;
        private String mEndSimple;

        public GameWeek(String aWeek, String aStart, String aEnd)
        {
            mWeek = aWeek;
            mStart = aStart;
            mEnd = aEnd;

            mStartSimple = Utils.convertToSimpleDate(mStart);
            mEndSimple = Utils.convertToSimpleDate(mEnd);
        }

        public String getWeek()
        {
           return mWeek;
        }

        public String getStart()
        {
            return mStart;
        }

        public String getStartSimple()
        {
            return mStartSimple;
        }

        public String getEnd()
        {
            return mEnd;
        }

        public String getEndSimple()
        {
            return mEndSimple;
        }
    }
}
