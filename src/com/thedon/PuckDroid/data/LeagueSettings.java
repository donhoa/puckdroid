package com.thedon.PuckDroid.data;

import com.thedon.PuckDroid.utils.Utils;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class LeagueSettings
{

    public static final int LEAGUE_TYPE_POINTS = 0;
    public static final int LEAGUE_TYPE_ROTISSERIE = 1;
    public static final int LEAGUE_TYPE_HEAD_TO_HEAD = 2;


    private String mDraftStyle;
    private String mIsAuctionDraft;
    private String mScoringType;
    private boolean mUsesPlayoff;
    private String mHasPlayoffConsolationGames;
    private int mPlayoffStartWeek;
    private String mUsesPlayoffReseeding;
    private String mUsesLockEliminatedTeams;
    private String mNumPlayoffTeams;
    private String mPlayoffConsolationTeams;
    private String mWaiverRule;
    private boolean mUsesFaab;
    private String mWaiverTime;
    private String mTradeEndDate;
    private String mTradeRatifyType;
    private String mTradeRejectTime;
    private String mPlayerPool;
    private String mCanTradeDraftPicks;
    private Hashtable<String, Integer> mRosterPositions;
    private Hashtable<String, StatisticsCategory> mStats;
    private int mMaxAdds;

    private int mRosterTotalPlayersCount;

    private int mLeagueType;
    private boolean mIsPlayoffWeek;
    private boolean mPastTradeDeadline;

    public LeagueSettings(String aDraftStyle, String aIsAuctionDraft, String aScoringType, String aUsesPlayoffString, String aHasPlayoffConsolationGames, String aPlayoffStartWeek, String aUsesPlayoffReseeding, String aUsesLockEliminatedTeams, String aNumPlayoffTeams, String aPlayoffConsolationTeams, String aWaiverRule, String aUsesFaabString, String aWaiverTime, String aTradeEndDate, String aTradeRatifyType, String aTradeRejectTime, String aPlayerPool, String aCanTradeDraftPicks, Hashtable<String, Integer> aRosterPositions, Hashtable<String, StatisticsCategory> aStats, String aMaxAddsString)
    {
        mDraftStyle = aDraftStyle;
        mIsAuctionDraft = aIsAuctionDraft;
        mScoringType = aScoringType;
        mUsesPlayoff = aUsesPlayoffString.equals("1");

        //Playoffs
        mHasPlayoffConsolationGames = aHasPlayoffConsolationGames;
        mUsesPlayoffReseeding = aUsesPlayoffReseeding;
        mUsesLockEliminatedTeams = aUsesLockEliminatedTeams;
        mNumPlayoffTeams = aNumPlayoffTeams;
        mPlayoffConsolationTeams = aPlayoffConsolationTeams;

        mWaiverRule = aWaiverRule;
        mWaiverTime = aWaiverTime;
        mTradeEndDate = aTradeEndDate;
        mTradeRatifyType = aTradeRatifyType;
        mTradeRejectTime = aTradeRejectTime;
        mPlayerPool = aPlayerPool;
        mCanTradeDraftPicks = aCanTradeDraftPicks;
        mRosterPositions = aRosterPositions;
        mStats = aStats;

        if (!Utils.isNullOrEmptyString(aMaxAddsString))
        {
            mMaxAdds = Integer.parseInt(aMaxAddsString);
        }
        else
        {
            mMaxAdds = -1; //No value means "unlimited"
        }

        if (mScoringType.equals("points"))
        {
            mLeagueType = LEAGUE_TYPE_POINTS;
        }
        else if (mScoringType.equals("roto"))
        {
            mLeagueType = LEAGUE_TYPE_ROTISSERIE;

        }
        else if (mScoringType.equals("head"))
        {
            mLeagueType = LEAGUE_TYPE_HEAD_TO_HEAD;
        }

        mUsesFaab = aUsesFaabString.equals("1");

        if (mUsesPlayoff)
        {
            try
            {
                mPlayoffStartWeek = Integer.parseInt(aPlayoffStartWeek);
            }
            catch (NumberFormatException e)
            {
                mPlayoffStartWeek = -1;
            }
        }
        else
        {
            mPlayoffStartWeek = -1;
        }

        mIsPlayoffWeek = false;

        mPastTradeDeadline = Utils.isPastTradeDeadline(mTradeEndDate);

        setRosterTotalPlayersCount();
    }

    public Hashtable<String, Integer> getRosterPositions()
    {
        return mRosterPositions;
    }

    public Hashtable<String, StatisticsCategory> getStats()
    {
        return mStats;
    }

    public int getLeagueType()
    {
        return mLeagueType;
    }

    public boolean usesFaab()
    {
        return mUsesFaab;
    }

    public int getRosterTotalPlayersCount()
    {
        return mRosterTotalPlayersCount;
    }

    public boolean usesPlayoff()
    {
        return mUsesPlayoff;
    }

    public int getPlayoffStartWeek()
    {
        return mPlayoffStartWeek;
    }

    public void setIsPlayoffWeek(boolean aIsPlayoffWeek)
    {
        mIsPlayoffWeek = aIsPlayoffWeek;
    }

    public boolean isPlayoffWeek()
    {
        return mIsPlayoffWeek;
    }

    public boolean isPastTradeDeadline()
    {
        return mPastTradeDeadline;
    }

    public int getMaxAdds()
    {
        return mMaxAdds;
    }

    private void setRosterTotalPlayersCount()
    {
        Enumeration positionsKeys = mRosterPositions.keys();

        mRosterTotalPlayersCount = 0;

        Object key;
        int value;

        while(positionsKeys.hasMoreElements())
        {
            key = positionsKeys.nextElement();

            if (!((String)key).equals("IR"))
            {
                value = mRosterPositions.get(key);
                mRosterTotalPlayersCount += value;
            }
        }
    }
}
