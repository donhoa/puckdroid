package com.thedon.PuckDroid.data;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class NavigationDrawerItem
{
    private String mTitle;

    public NavigationDrawerItem(String aTitle)
    {
        mTitle = aTitle;
    }

    public String getTitle()
    {
        return mTitle;
    }
}
