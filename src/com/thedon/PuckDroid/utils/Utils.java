package com.thedon.PuckDroid.utils;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.StatisticsCategory;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class Utils
{
    private static final String TAG = Utils.class.getSimpleName();

    public static boolean isNullOrEmptyString(final CharSequence a)
    {
        return ((a == null) || (a.length() == 0));
    }

    public static boolean isStringNumeric(String aString)
    {
        for (char c : aString.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

    public static void resetPositionsPreferences(Preferences aPreferences)
    {
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_C_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_LW_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_RW_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_W_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_F_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_D_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_G_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_UTIL_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_BN_COUNT, 0);
        aPreferences.putInt(Preferences.KEY_SETTINGS_ROSTER_IR_COUNT, 0);
    }

    public static String getPositionsPreferencesKey(String aPosition)
    {
        String key = "";

        if (aPosition.equals("C"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_C_COUNT;
        }
        else if (aPosition.equals("LW"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_LW_COUNT;
        }
        else if (aPosition.equals("RW"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_RW_COUNT;
        }
        else if (aPosition.equals("W"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_W_COUNT;
        }
        else if (aPosition.equals("F"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_F_COUNT;
        }
        else if (aPosition.equals("D"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_D_COUNT;
        }
        else if (aPosition.equals("G"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_G_COUNT;
        }
        else if (aPosition.equals("Util"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_UTIL_COUNT;
        }
        else if (aPosition.equals("BN"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_BN_COUNT;
        }
        else if (aPosition.equals("IR"))
        {
            key = Preferences.KEY_SETTINGS_ROSTER_IR_COUNT;
        }

        return key;
    }

    public static ArrayList<String> getPlayersSortSpinnerValues(Context aContext, LeagueSettings aLeagueSettings, boolean aSkaterStats, boolean aGetId)
    {
        Hashtable<String, StatisticsCategory> playerStatsTable = aLeagueSettings.getStats();

        ArrayList<String> values = new ArrayList<String>();

        if (aGetId)
        {
            values.add("OR");
        }
        else
        {
            values.add(aContext.getString(R.string.overall_rank));
        }

        if (aLeagueSettings.getLeagueType() == LeagueSettings.LEAGUE_TYPE_POINTS)
        {
            if (aGetId)
            {
                values.add("PTS");
            }
            else
            {
                values.add(aContext.getString(R.string.fantasy_points));
            }
        }

        for (Hashtable.Entry<String, StatisticsCategory> entry : playerStatsTable.entrySet())
        {
            if (aSkaterStats)
            {
                if (entry.getValue().isSkaterStat())
                {
                    if (aGetId)
                    {
                        values.add(entry.getValue().getStatId());
                    }
                    else
                    {
                        values.add(entry.getValue().getName());
                    }
                }
            }
            else
            {
                if (!entry.getValue().isSkaterStat())
                {
                    if (aGetId)
                    {
                        values.add(entry.getValue().getStatId());
                    }
                    else
                    {
                        values.add(entry.getValue().getName());
                    }
                }
            }
        }

        return values;
    }

    public static String getTodayFormattedDate()
    {
        Calendar calendar = Calendar.getInstance();
        return getFormattedDate(calendar.getTime());
    }

    public static String getTomorrowFormattedDate()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        Date tomorrow = calendar.getTime();

        return getFormattedDate(tomorrow);
    }

    public static String getFormattedDate(Date aDate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        return dateFormat.format(aDate);
    }

    public static boolean isDateBeforeToday(String aDate)
    {
        Calendar today = Calendar.getInstance();
        int result = 0;
        try
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse(aDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            DateComparator comparator = new DateComparator();
            result = comparator.compare(today, calendar);
        }
        catch (ParseException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return result > 0;
    }

    public static boolean isPastTradeDeadline(String aDeadlineDate)
    {
        Calendar today = Calendar.getInstance();
        int result = 0;
        try
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date date = df.parse(aDeadlineDate);
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);

            DateComparator comparator = new DateComparator();
            result = comparator.compare(today, calendar);
        }
        catch (ParseException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return result >= 0;
    }

    /*
    Converts YYYY-MM-DD to simple date
     */
    public static String convertToSimpleDate(String aDate)
    {
        Date date = null;

        try
        {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            date = df.parse(aDate);
        }
        catch (ParseException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return getSimpleDate(date);
    }

    public static String getSimpleDate(Date aDate)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM d");

        return dateFormat.format(aDate);
    }

    public static String getDateDescription(Context aContext, Date aDate)
    {
        String dateDescription;

        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE");
        dateDescription = dateFormat.format(aDate);

        if (DateUtils.isToday(aDate.getTime()))
        {
            dateDescription = String.format(aContext.getString(R.string.today_date), dateDescription);
        }

        dateDescription = dateDescription.toUpperCase();

        return dateDescription;
    }

    public static String getMainDisplayPosition(Context aContext, List<String> aDisplayPositions)
    {
        String mainDisplayPosition = "";

        if (aDisplayPositions.size() > 0)
        {
            String position = aDisplayPositions.get(0);

            if (position.equals("C"))
            {
                mainDisplayPosition = aContext.getString(R.string.centre);
            }
            else if (position.equals("LW"))
            {
                mainDisplayPosition = aContext.getString(R.string.leftWing);
            }
            else if (position.equals("RW"))
            {
                mainDisplayPosition = aContext.getString(R.string.rightWing);
            }
            else if (position.equals("D"))
            {
                mainDisplayPosition = aContext.getString(R.string.defenseman);
            }
            else if (position.equals("G"))
            {
                mainDisplayPosition = aContext.getString(R.string.goaltender);
            }
        }

        Log.v(TAG, "getMainDisplayPosition(): " + mainDisplayPosition);

        return mainDisplayPosition;
    }

    public static int getTeamLogoDrawableId(Context aContext, String aTeamAbbr)
    {
        return aContext.getResources().getIdentifier(aTeamAbbr.toLowerCase(), "drawable", aContext.getPackageName());
    }

    public static String getTransactionDate(long aTimestamp)
    {
        String transactionDate = (DateUtils.formatSameDayTime(aTimestamp * 1000, System.currentTimeMillis(), DateFormat.DEFAULT, DateFormat.DEFAULT)).toString();
        Log.v(TAG, "getTransactionDate(): " + transactionDate);

        return transactionDate;
    }

    public static String getStatNameById(Hashtable<String, StatisticsCategory> aStatsTable, String aStatId, boolean aDisplayName)
    {
        Log.v(TAG, "getStatNameById(): " + aStatId);

        StatisticsCategory statCategory = aStatsTable.get(aStatId);

        String name = "";

        if (aDisplayName)
        {
            name = statCategory.getDisplayName();
        }
        else
        {
            name = statCategory.getName();
        }

        return name;
    }

    public static Date[] getRosterDatesArray()
    {
        Date[] dates = new Date[5];

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);
        Date yesterday = calendar.getTime();

        dates[0] = yesterday;

        for (int i = 0; i < 4; i++)
        {
            calendar = Calendar.getInstance();
            calendar.add(Calendar.DATE, i);
            dates[i + 1] = calendar.getTime();
        }

        return dates;
    }

    public static String getFormattedPoints(String aPointsString)
    {
        String pointsString = "";

        try
        {
            if (!isNullOrEmptyString(aPointsString))
            {
                Float points = new Float(aPointsString);
                DecimalFormat df = new DecimalFormat("#.0#"); //0 means "always show the digit in this position", # means "show the digit in this position unless it's zero"
                pointsString = df.format(points);
            }
        }
        catch (NumberFormatException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return pointsString;
    }

    private static class DateComparator implements Comparator<Calendar>
    {
        public int compare(Calendar c1, Calendar c2)
        {
            if (c1.get(Calendar.YEAR) != c2.get(Calendar.YEAR))
            {
                return c1.get(Calendar.YEAR) - c2.get(Calendar.YEAR);
            }
            if (c1.get(Calendar.MONTH) != c2.get(Calendar.MONTH))
            {
                return c1.get(Calendar.MONTH) - c2.get(Calendar.MONTH);
            }
            return c1.get(Calendar.DAY_OF_MONTH) - c2.get(Calendar.DAY_OF_MONTH);
        }
    }
}
