package com.thedon.PuckDroid.view;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public interface FragmentSwitchListener
{
    public void onSwitchRequest(boolean aRequestFromStandingsFragment);
}
