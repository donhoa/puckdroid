package com.thedon.PuckDroid.view;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class ScoreboardListItem implements CustomListItem
{
    private boolean mIsConsolation;

    private String mTeamName1;
    private String mTeamName2;
    private String mTeamScore1;
    private String mTeamScore2;

    @Override
    public boolean isHeader()
    {
        return false;
    }

    public void setIsConsolation(boolean aIsConsolation)
    {
        mIsConsolation = aIsConsolation;
    }

    public boolean isConsolation()
    {
        return mIsConsolation;
    }

    public void setTeamName1(String aTeamName)
    {
        mTeamName1 = aTeamName;
    }

    public String getTeamName1()
    {
        return mTeamName1;
    }

    public void setTeamName2(String aTeamName)
    {
        mTeamName2 = aTeamName;
    }

    public String getTeamName2()
    {
        return mTeamName2;
    }

    public void setTeamScore1(String aTeamScore)
    {
        mTeamScore1 = aTeamScore;
    }

    public String getTeamScore1()
    {
        return mTeamScore1;
    }

    public void setTeamScore2(String aTeamScore)
    {
        mTeamScore2 = aTeamScore;
    }

    public String getTeamScore2()
    {
        return mTeamScore2;
    }
}
