package com.thedon.PuckDroid.view;

import android.app.ActionBar;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.view.MenuItem;
import com.thedon.PuckDroid.R;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class SettingsActivity extends PreferenceActivity
{
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new PrefsFragment(this)).commit();

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        setTitle(getString(R.string.settings));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static class PrefsFragment extends PreferenceFragment
    {
        private Context mContext;

        public PrefsFragment(Context aContext)
        {
            mContext = aContext;
        }

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            try
            {
                super.onCreate(savedInstanceState);

                addPreferencesFromResource(R.xml.preferences);

                PackageInfo pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
                String versionName = pInfo.versionName;

                ListPreference listPreference = (ListPreference) findPreference("listPreferenceVersion");
                listPreference.setTitle(String.format(mContext.getString(R.string.version), versionName));
            }
            catch (PackageManager.NameNotFoundException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

}