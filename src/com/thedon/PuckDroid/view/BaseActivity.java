package com.thedon.PuckDroid.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.League;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.data.parsers.LeagueSettingsXmlParser;
import com.thedon.PuckDroid.data.parsers.LeagueXmlParser;
import com.thedon.PuckDroid.data.parsers.PoolTeamXmlParser;
import com.thedon.PuckDroid.utils.Utils;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public abstract class BaseActivity extends Activity
{
    private static final String TAG = BaseActivity.class.getSimpleName();

    private ResultReceiver mResultReceiver;

    protected ProgressDialog mLoadingAnimation;

    private boolean mSettingsSaved;
    private boolean mTeamInfoSaved;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        mSettingsSaved = false;
        mTeamInfoSaved = false;

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    if (resultCode == YahooSportsApiService.API_RESULT_OK)
                    {
                        String originalRequest = resultData.getString(BundleConstants.YAHOO_SPORTS_API_REQUEST);
                        String data = resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT);

                        if (originalRequest.equals(BundleConstants.GET_LEAGUE_SETTINGS))
                        {
                            mSettingsSaved = saveLeagueSettings(data);
                        }
                        else if (originalRequest.equals(BundleConstants.GET_TEAM_INFO))
                        {
                            mTeamInfoSaved = saveTeamInfo(data);
                        }
                    }
                }

                if (mSettingsSaved && mTeamInfoSaved) //We have all our needed info
                {
                    onGlobalInfoSaved();
                }
            }
        };
    }

    protected final Preferences getPreferences()
    {
        return Preferences.getInstance(getApplicationContext());
    }

    public boolean isNetworkActive()
    {
        boolean isNetworkActive;
        ConnectivityManager cm =  (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        isNetworkActive = (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected());

        if (!isNetworkActive)
        {
            Toast.makeText(this, getResources().getString(R.string.network_unavailable), Toast.LENGTH_LONG).show();
        }

        return isNetworkActive;
    }

    protected void getGlobalInfo()
    {
        Log.v(TAG, TAG + ".getGlobalInfo()");

        Intent intent = new Intent(this, YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_SETTINGS);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        startService(intent);

        //Get current user's team info
        Preferences preferences = Preferences.getInstance(getApplicationContext());
        String currentUserTeamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TEAM_INFO);
        intent.putExtra(BundleConstants.GET_TEAM_INFO_KEY, currentUserTeamKey);
        startService(intent);
    }

    protected boolean saveLeagueSettings(String aData)
    {
        Log.v(TAG, TAG + ".saveLeagueSettings()");

        boolean success = false;

        try
        {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = parser.getEventType();
            LeagueXmlParser leagueXmlParser = new LeagueXmlParser();

            League league = null;

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(parser.getName().equalsIgnoreCase("league"))
                    {
                        league = leagueXmlParser.readLeague(parser);
                    }
                }
                eventType = parser.next();
            }

            if (league != null)
            {
                ((PuckDroidApplication)getApplication()).setLeague(league);

                Preferences preferences = Preferences.getInstance(getApplicationContext());
                preferences.putString(Preferences.KEY_CURRENT_WEEK, String.valueOf(league.getCurrentWeek()));

                LeagueSettings leagueSettings = league.getLeagueSettings();

                if (leagueSettings != null)
                {
                    Utils.resetPositionsPreferences(preferences);
                    ((PuckDroidApplication)getApplication()).setLeagueSettings(leagueSettings);

                    Hashtable<String, Integer> positions = leagueSettings.getRosterPositions();
                    Enumeration positionsKeys = positions.keys();
                    Object key;
                    int value;

                    while(positionsKeys.hasMoreElements())
                    {
                        key = positionsKeys.nextElement();
                        value = positions.get(key);

                        preferences.putInt(Utils.getPositionsPreferencesKey((String) key), value);
                    }

                    success = true;
                }
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return success;
    }

    protected boolean saveTeamInfo(String aData)
    {
        Log.v(TAG, "saveTeamInfo()");

        boolean success = false;

        Team team = null;
        try
        {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType=parser.getEventType();
            PoolTeamXmlParser poolTeamXmlParser = new PoolTeamXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(parser.getName().equalsIgnoreCase("team"))
                    {
                        team = poolTeamXmlParser.readTeam(parser);
                    }
                }
                eventType = parser.next();
            }

            ((PuckDroidApplication)getApplication()).setCurrentUserTeamInfo(team);
            success = true;
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return success;
    }

    protected void onGlobalInfoSaved()
    {
        Log.v(TAG, TAG + ".onGlobalInfoSaved()");

        Intent intent = new Intent(this, ManagerActivity.class);
        startActivity(intent);
    }
}