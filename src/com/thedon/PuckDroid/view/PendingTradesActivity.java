package com.thedon.PuckDroid.view;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.view.fragments.PendingTradesFragment;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PendingTradesActivity extends FragmentActivity implements ActionBar.TabListener
{
    ViewPager mViewPager;
    PendingTradesPagerAdapter mPendingTradesPagerAdapter;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trade_offers);

        mPendingTradesPagerAdapter = new PendingTradesPagerAdapter(this, getSupportFragmentManager());

        final ActionBar actionBar = getActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.trades_title));

        mViewPager = (ViewPager) findViewById(R.id.trade_offers_pager);
        mViewPager.setAdapter(mPendingTradesPagerAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < mPendingTradesPagerAdapter.getCount(); i++)
        {
            actionBar.addTab(actionBar.newTab()
                 .setText(mPendingTradesPagerAdapter.getPageTitle(i))
                 .setTabListener(this));
        }
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static class PendingTradesPagerAdapter extends FragmentStatePagerAdapter
    {
        Context mContext;

        public PendingTradesPagerAdapter(Context context, FragmentManager fm)
        {
            super(fm);
            mContext = context;
        }

        @Override
        public Fragment getItem(int i)
        {
            Fragment fragment = new PendingTradesFragment();
            Bundle args = new Bundle();

            switch (i)
            {
                case 0:
                    args.putInt(BundleConstants.PENDING_TRADES_FRAGMENT_TYPE, PendingTradesFragment.TYPE_MY_OFFERS);
                    break;
                case 1:
                    args.putInt(BundleConstants.PENDING_TRADES_FRAGMENT_TYPE, PendingTradesFragment.TYPE_LEAGUE);
                    break;
            }

            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public int getCount()
        {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            String[] titles = mContext.getResources().getStringArray(R.array.pending_trades_sections_titles);
            return titles[position];
        }
    }
}