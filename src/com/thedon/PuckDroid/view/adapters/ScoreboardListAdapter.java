package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.League;
import com.thedon.PuckDroid.data.Matchup;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.view.CustomListItem;
import com.thedon.PuckDroid.view.HeaderListItem;
import com.thedon.PuckDroid.view.ScoreboardListItem;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class ScoreboardListAdapter extends BaseAdapter
{
    private ArrayList<CustomListItem> mItems;

    private LayoutInflater mInflater;

    public ScoreboardListAdapter(Context context, ArrayList<CustomListItem> listItems)
    {
        mInflater = LayoutInflater.from(context);
        mItems = listItems;
    }

    public int getCount()
    {
        return mItems.size();
    }

    public Object getItem(int aPosition)
    {
        return mItems.get(aPosition);
    }

    @Override
    public int getViewTypeCount()
    {
        return 2;
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        ScoreboardViewHolder viewHolder;

        CustomListItem item = mItems.get(aPosition);

        if (aView == null)
        {
            viewHolder = new ScoreboardViewHolder();

            if (item.isHeader())
            {
                aView = mInflater.inflate(R.layout.section_header, aParent, false);
                viewHolder.headerTitle = (TextView)aView.findViewById(android.R.id.title);
            }
            else
            {
                aView = mInflater.inflate(R.layout.scoreboard_list_item, aParent, false);

                viewHolder.team1Name = (TextView)aView.findViewById(R.id.team1Name);
                viewHolder.team2Name = (TextView)aView.findViewById(R.id.team2Name);
                viewHolder.team1Score = (TextView)aView.findViewById(R.id.team1Score);
                viewHolder.team2Score = (TextView)aView.findViewById(R.id.team2Score);
                aView.setTag(viewHolder);
            }
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ScoreboardViewHolder)aView.getTag();
        }

        if (item.isHeader())
        {
            HeaderListItem currentItem = (HeaderListItem)item;

            viewHolder.headerTitle.setText(currentItem.getHeaderTitle());
        }
        else
        {
            ScoreboardListItem currentItem = (ScoreboardListItem)item;

            viewHolder.team1Name.setText(currentItem.getTeamName1());
            viewHolder.team2Name.setText(currentItem.getTeamName2());

            viewHolder.team1Score.setText(currentItem.getTeamScore1());
            viewHolder.team2Score.setText(currentItem.getTeamScore2());
        }

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    static class ScoreboardViewHolder
    {
        TextView headerTitle;
        TextView team1Name;
        TextView team2Name;
        TextView team1Score;
        TextView team2Score;
    }
}
