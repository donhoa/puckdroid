package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.Player;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class RosterPlayersSelectionListAdapter extends BaseAdapter
{
    private ArrayList<PlayerItem> mPlayersArrayList;

    private LayoutInflater mInflater;

    private Context mContext;

    private RosterPlayersSelectionListListener mListener;

    public interface RosterPlayersSelectionListListener
    {
        public void onItemCheckBoxClicked(int position, boolean isChecked);
    }

    public RosterPlayersSelectionListAdapter(Context context, ArrayList<PlayerItem> players, RosterPlayersSelectionListListener aListener)
    {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mPlayersArrayList = players;
        mListener = aListener;
    }

    public int getCount()
    {
        return mPlayersArrayList.size();
    }

    public Object getItem(int aPosition)
    {
        return mPlayersArrayList.get(aPosition);
    }

    public View getView(final int aPosition, View aView, ViewGroup aParent)
    {
        PlayerViewHolder viewHolder;

        if (aView == null)
        {
            aView = mInflater.inflate(R.layout.roster_player_selection_list_item, aParent, false);
            viewHolder = new PlayerViewHolder();
            viewHolder.playerName = (TextView)aView.findViewById(R.id.playerSelectionName);
            //viewHolder.playerTeam = (TextView)aView.findViewById(R.id.playerTeam);
            //viewHolder.positions = (TextView)aView.findViewById(R.id.positions);
            //viewHolder.pointsTotal = (TextView)aView.findViewById(R.id.pointsTotal);
            viewHolder.checkBox = (CheckBox)aView.findViewById(R.id.playerSelectionCheckbox);
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (PlayerViewHolder)aView.getTag();
        }

        PlayerItem currentPlayer = mPlayersArrayList.get(aPosition);

        viewHolder.playerName.setText(currentPlayer.playerName);
        viewHolder.checkBox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mPlayersArrayList.get(aPosition).isSelected = ((CheckBox)view).isChecked();
                mListener.onItemCheckBoxClicked(aPosition, ((CheckBox)view).isChecked());
            }
        });
        viewHolder.checkBox.setChecked(currentPlayer.isSelected);
        //viewHolder.playerTeam.setText(currentPlayer.getTeamFullName());
        //viewHolder.positions.setText(currentPlayer.getEligiblePositionsString());
        //viewHolder.pointsTotal.setText(currentPlayer.getPlayerStatistics().getPlayerPoints().getTotal());

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    public ArrayList<PlayerItem> getSelectedPlayers()
    {
        ArrayList<PlayerItem> selectedPlayers = new ArrayList<PlayerItem>();

        for (PlayerItem playerItem : mPlayersArrayList)
        {
            if (playerItem.isSelected)
            {
                selectedPlayers.add(playerItem);
            }
        }
        return selectedPlayers;
    }

    static class PlayerViewHolder
    {
        TextView playerName;
        TextView playerTeam;
        TextView positions;
        TextView pointsTotal;
        CheckBox checkBox;
    }

    public static class PlayerItem
    {
        public String playerKey;
        public String playerName;
        public String pointsTotal;
        public boolean isSelected;
    }
}
