package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.League;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class LeagueListAdapter extends BaseAdapter
{
    private static ArrayList<League> sLeagueArrayList;

    private LayoutInflater mInflater;

    public LeagueListAdapter(Context context,  ArrayList<League> leagues)
    {
        mInflater = LayoutInflater.from(context);
        sLeagueArrayList = leagues;
    }

    public int getCount()
    {
        return sLeagueArrayList.size();
    }

    public Object getItem(int aPosition)
    {
        return sLeagueArrayList.get(aPosition);
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        LeagueViewHolder viewHolder;

        if (aView == null)
        {
            aView = mInflater.inflate(R.layout.league_list_item, aParent, false);
            viewHolder = new LeagueViewHolder();
            viewHolder.leagueName = (TextView)aView.findViewById(R.id.leagueName);
            viewHolder.teamName = (TextView)aView.findViewById(R.id.teamName);
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (LeagueViewHolder)aView.getTag();
        }

        League currentLeague = sLeagueArrayList.get(aPosition);

        viewHolder.leagueName.setText(currentLeague.getName());

        if (currentLeague.getTeams().size() > 0)
        {
            viewHolder.teamName.setText(currentLeague.getTeams().get(0).getName());
        }

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    static class LeagueViewHolder
    {
        TextView leagueName;
        TextView teamName;
    }

}
