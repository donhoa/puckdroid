package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.Transaction;
import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class TransactionsListAdapter extends BaseAdapter
{
    private static final int ROW_TYPE_WIRE_TRANSACTION = 0;
    private static final int ROW_TYPE_TRADE = 1;

    private ArrayList<Transaction> mTransactionsArrayList;

    private LayoutInflater mInflater;

    private Context mContext;

    public TransactionsListAdapter(Context context,  ArrayList<Transaction> transactions)
    {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mTransactionsArrayList = transactions;
    }

    public int getCount()
    {
        return mTransactionsArrayList.size();
    }

    public Object getItem(int aPosition)
    {
        return mTransactionsArrayList.get(aPosition);
    }

    @Override
    public int getItemViewType(int position)
    {
        Transaction currentTransaction = mTransactionsArrayList.get(position);
        return currentTransaction.isTrade() ? ROW_TYPE_TRADE : ROW_TYPE_WIRE_TRANSACTION;
    }

    @Override
    public int getViewTypeCount()
    {
        return 2;
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        TransactionsViewHolder viewHolder;

        int viewType = getItemViewType(aPosition);

        if (aView == null)
        {
            viewHolder = new TransactionsViewHolder();

            switch (viewType)
            {
                case ROW_TYPE_TRADE:
                    aView = mInflater.inflate(R.layout.trade_transaction_list_item, aParent, false);
                    viewHolder.tradeTitle = (TextView)aView.findViewById(R.id.tradeTitle);
                    viewHolder.tradeTeamName1 = (TextView)aView.findViewById(R.id.tradeTeamName1);
                    viewHolder.tradeTeamName2 = (TextView)aView.findViewById(R.id.tradeTeamName2);
                    viewHolder.tradePlayersTeam1 = (TextView)aView.findViewById(R.id.tradePlayersTeam1);
                    viewHolder.tradePlayersTeam2 = (TextView)aView.findViewById(R.id.tradePlayersTeam2);
                    aView.setTag(viewHolder);
                    break;
                case ROW_TYPE_WIRE_TRANSACTION:
                    aView = mInflater.inflate(R.layout.wire_transaction_list_item, aParent, false);
                    viewHolder = new TransactionsViewHolder();
                    viewHolder.transactionAuthor = (TextView)aView.findViewById(R.id.transactionAuthor);
                    viewHolder.transactionType = (TextView)aView.findViewById(R.id.transactionType);
                    viewHolder.playerAddedName = (TextView)aView.findViewById(R.id.playerAddedName);
                    viewHolder.playerDroppedName = (TextView)aView.findViewById(R.id.playerDroppedName);
                    viewHolder.transactionTime = (TextView)aView.findViewById(R.id.transactionTime);
                    aView.setTag(viewHolder);
                    break;
            }
        }
        else
        {
            viewHolder = (TransactionsViewHolder)aView.getTag();
        }

        Transaction currentTransaction = mTransactionsArrayList.get(aPosition);

        if (currentTransaction.isTrade())
        {
            if (currentTransaction.getState() == Transaction.TRANSACTION_VETOED_STATE)
            {
                viewHolder.tradeTitle.setText(mContext.getResources().getString(R.string.trade_vetoed_title));
            }
            else
            {
                viewHolder.tradeTitle.setText(mContext.getResources().getString(R.string.trade_title));
            }

            viewHolder.tradeTeamName1.setText(currentTransaction.getTraderTeamName());
            viewHolder.tradeTeamName2.setText(currentTransaction.getTradeeTeamName());

            viewHolder.tradePlayersTeam1.setText(currentTransaction.getTraderPlayersString());
            viewHolder.tradePlayersTeam2.setText(currentTransaction.getTradeePlayersString());
        }
        else //Wire transaction
        {
            String type = currentTransaction.getType();
            if (currentTransaction.isFaabBid())
            {
                type = type + " (" + currentTransaction.getFaabBid() + "$)";
            }
            viewHolder.transactionType.setText(type);
            viewHolder.playerAddedName.setVisibility(View.GONE);
            viewHolder.playerDroppedName.setVisibility(View.GONE);

            ArrayList<Transaction.Player> players = currentTransaction.getPlayers();

            for (Transaction.Player player : players)
            {
                if (player.getTransactionData().isPlayerAdded())
                {
                    viewHolder.playerAddedName.setVisibility(View.VISIBLE);
                    viewHolder.playerAddedName.setText(player.getName().getFullName());
                    viewHolder.transactionAuthor.setText(player.getTransactionData().getDestinationTeamName());
                }
                else
                {
                    viewHolder.playerDroppedName.setVisibility(View.VISIBLE);
                    viewHolder.playerDroppedName.setText(player.getName().getFullName());
                    viewHolder.transactionAuthor.setText(player.getTransactionData().getSourceTeamName());
                }
            }

            viewHolder.transactionTime.setText(Utils.getTransactionDate(currentTransaction.getTimeStamp()));
        }

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    static class TransactionsViewHolder
    {
        //Wire transaction
        TextView transactionAuthor;
        TextView transactionType;
        TextView playerAddedName;
        TextView playerDroppedName;
        TextView transactionTime;

        //Trades
        TextView tradeTitle;
        TextView tradeTeamName1;
        TextView tradeTeamName2;
        TextView tradePlayersTeam1;
        TextView tradePlayersTeam2;
    }
}
