package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.thedon.PuckDroid.R;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class SortBySpinnerAdapter extends BaseAdapter
{
    private LayoutInflater mInflater;
    ArrayList<String> mValues;

    public SortBySpinnerAdapter(Context context,  ArrayList<String> aValues)
    {
        mInflater = LayoutInflater.from(context);
        mValues = aValues;
    }

    public int getCount()
    {
        return mValues.size();
    }

    public Object getItem(int aPosition)
    {
        return mValues.get(aPosition);
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        ViewHolder viewHolder;

        if (aView == null)
        {
            aView = mInflater.inflate(android.R.layout.simple_list_item_1, aParent, false);
            viewHolder = new ViewHolder();
            viewHolder.value = (TextView)aView.findViewById(android.R.id.text1);
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (ViewHolder)aView.getTag();
        }

        String value = mValues.get(aPosition);

        viewHolder.value.setText(value);

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    public void setValues(ArrayList<String> aValues)
    {
        mValues = aValues;
    }

    static class ViewHolder
    {
        TextView value;
    }
}
