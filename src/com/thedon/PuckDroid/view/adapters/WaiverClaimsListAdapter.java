package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.data.Transaction;
import com.thedon.PuckDroid.data.WaiverTransaction;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class WaiverClaimsListAdapter extends BaseAdapter
{
    private ArrayList<WaiverTransaction> mTransactions;

    private LayoutInflater mInflater;

    private Context mContext;

    private WaiverClaimsListener mListener;

    public interface WaiverClaimsListener
    {
        public void onCancelWaiver(String aTransactionKey);
    }

    public WaiverClaimsListAdapter(Context context, ArrayList<WaiverTransaction> transactions, WaiverClaimsListener aListener)
    {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mTransactions = transactions;
        mListener = aListener;
    }

    public int getCount()
    {
        return mTransactions.size();
    }

    public Object getItem(int aPosition)
    {
        return mTransactions.get(aPosition);
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        WaiverTransactionViewHolder viewHolder;

        if (aView == null)
        {
            aView = mInflater.inflate(R.layout.waiver_claims_list_item, aParent, false);
            viewHolder = new WaiverTransactionViewHolder();
            viewHolder.waiverPriority = (Spinner)aView.findViewById(R.id.waiverClaimPriority);
            viewHolder.waiverDate = (TextView)aView.findViewById(R.id.waiverClaimDate);
            viewHolder.waiverAddPlayer = (TextView)aView.findViewById(R.id.waiverClaimAddPlayer);
            viewHolder.waiverDropPlayer = (TextView)aView.findViewById(R.id.waiverClaimDropPlayer);
            viewHolder.waiverFaabBid = (TextView)aView.findViewById(R.id.waiverClaimFaabBid);
            viewHolder.cancelWaiverButton = (Button)aView.findViewById(R.id.waiverClaimCancelButton);
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (WaiverTransactionViewHolder)aView.getTag();
        }

        final WaiverTransaction transaction = mTransactions.get(aPosition);

        ArrayList<Transaction.Player> players = transaction.getPlayers();

        final List<String> waiverPriorityOptions = transaction.getWaiverPriorityOptions();
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, waiverPriorityOptions);

        viewHolder.waiverPriority.setAdapter(spinnerArrayAdapter);
        viewHolder.waiverPriority.setSelection(waiverPriorityOptions.indexOf(transaction.getWaiverPriority()));
        viewHolder.waiverPriority.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                transaction.setWaiverPriority(spinnerArrayAdapter.getItem(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {
            }
        });

        //Disable the spinner if there is only one priority possible for the waiver claim
        if (waiverPriorityOptions.size() < 2)
        {
            viewHolder.waiverPriority.setEnabled(false);
        }

        viewHolder.waiverDate.setText(transaction.getWaiverDate());
        viewHolder.waiverAddPlayer.setText(players.get(0).getName().getFullName());
        viewHolder.waiverDropPlayer.setText(players.get(1).getName().getFullName());
        viewHolder.waiverFaabBid.setText(transaction.getFaabBid());
        viewHolder.cancelWaiverButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mListener.onCancelWaiver(transaction.getTransactionKey());
            }
        });

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    public ArrayList<WaiverTransaction> getTransactions()
    {
        return mTransactions;
    }

    static class WaiverTransactionViewHolder
    {
        Spinner waiverPriority;
        TextView waiverDate;
        TextView waiverAddPlayer;
        TextView waiverDropPlayer;
        Button cancelWaiverButton;
        TextView waiverFaabBid;
    }
}
