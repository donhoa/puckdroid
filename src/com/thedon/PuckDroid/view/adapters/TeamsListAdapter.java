package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.Team;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class TeamsListAdapter extends BaseAdapter
{
    private ArrayList<Team> mTeamsArrayList;

    private LayoutInflater mInflater;

    public TeamsListAdapter(Context context,  ArrayList<Team> leagues)
    {
        mInflater = LayoutInflater.from(context);
        mTeamsArrayList = leagues;
    }

    public int getCount()
    {
        return mTeamsArrayList.size();
    }

    public Object getItem(int aPosition)
    {
        return mTeamsArrayList.get(aPosition);
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        TeamViewHolder viewHolder;

        if (aView == null)
        {
            aView = mInflater.inflate(R.layout.team_standings_list_item, aParent, false);
            viewHolder = new TeamViewHolder();
            viewHolder.teamStandingsListItemLayout = (LinearLayout)aView.findViewById(R.id.teamStandingsListItemLayout);
            viewHolder.teamName = (TextView)aView.findViewById(R.id.teamName);
            viewHolder.teamRank = (TextView)aView.findViewById(R.id.teamRank);
            viewHolder.pointsChange = (TextView)aView.findViewById(R.id.pointsChange);
            viewHolder.pointsBack = (TextView)aView.findViewById(R.id.pointsBack);
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (TeamViewHolder)aView.getTag();
        }

        Team currentTeam = mTeamsArrayList.get(aPosition);

        viewHolder.teamName.setText(currentTeam.getName());
        viewHolder.teamRank.setText(currentTeam.getStandings().getRank());
        viewHolder.pointsChange.setText(currentTeam.getStandings().getPointsChange());
        viewHolder.pointsBack.setText(currentTeam.getStandings().getPointsBack());

        if (currentTeam.isCurrentUser())
        {
            viewHolder.teamName.setTextColor(Color.RED);
        }
        else
        {
            viewHolder.teamName.setTextColor(Color.WHITE);
        }

        if (currentTeam.clinchedPlayoffs())
        {
            viewHolder.teamStandingsListItemLayout.setBackgroundColor(Color.BLUE);
        }
        else
        {
            viewHolder.teamStandingsListItemLayout.setBackgroundColor(Color.BLACK);
        }

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    static class TeamViewHolder
    {
        LinearLayout teamStandingsListItemLayout;
        TextView teamName;
        TextView teamRank;
        TextView pointsChange;
        TextView pointsBack;
    }
}
