package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TableRow;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.Transaction;
import com.thedon.PuckDroid.utils.Utils;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PendingTradesListAdapter extends BaseAdapter
{
    private ArrayList<Transaction> mPendingTradesList;

    private LayoutInflater mInflater;

    private static String sCurrentUserTeamKey;

    private Context mContext;

    private ResultReceiver mResultReceiver;

    private PendingTradeOperationListener mListener;

    public interface PendingTradeOperationListener
    {
        public void onAcceptTrade(Transaction aTransaction);

        public void onRejectTrade(Transaction aTransaction);

        public void onCounterOffer(Transaction aTransaction);

        public void onCancelTrade(Transaction aTransaction);

        public void onVoteAgainstTrade(Transaction aTransaction);
    }

    public PendingTradesListAdapter(Context context,  ArrayList<Transaction> pendingTrades, PendingTradeOperationListener aListener)
    {
        mInflater = LayoutInflater.from(context);
        mPendingTradesList = pendingTrades;
        mContext = context;
        mListener = aListener;

        Preferences preferences = Preferences.getInstance(context);
        sCurrentUserTeamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    if (resultCode == YahooSportsApiService.API_RESULT_OK)
                    {
                    }
                }
            }
        };
    }

    public int getCount()
    {
        return mPendingTradesList.size();
    }

    public Object getItem(int aPosition)
    {
        return mPendingTradesList.get(aPosition);
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        PendingTradeViewHolder viewHolder;

        if (aView == null)
        {
            viewHolder = new PendingTradeViewHolder();

            aView = mInflater.inflate(R.layout.pending_trades_list_item, aParent, false);
            viewHolder.pendingTradeTitle = (TextView)aView.findViewById(R.id.pendingTradeTitle);
            viewHolder.pendingTradeTeamName1 = (TextView)aView.findViewById(R.id.pendingTradeTeamName1);
            viewHolder.pendingTradeTeamName2 = (TextView)aView.findViewById(R.id.pendingTradeTeamName2);
            viewHolder.pendingTradePlayersTeam1 = (TextView)aView.findViewById(R.id.pendingTradePlayersTeam1);
            viewHolder.pendingTradePlayersTeam2 = (TextView)aView.findViewById(R.id.pendingTradePlayersTeam2);
            viewHolder.pendingTradeDivider2 = aView.findViewById(R.id.pendingTradeDivider2);
            viewHolder.pendingTradeNote = (TextView)aView.findViewById(R.id.pendingTradeNote);

            viewHolder.tradeActionButtonRow = (TableRow)aView.findViewById(R.id.tradeActionButtonRow);
            viewHolder.cancelTradeButton = (Button)aView.findViewById(R.id.cancelTradeButton);
            viewHolder.voteAgainstTradeButton = (Button)aView.findViewById(R.id.voteAgainstTradeButton);
            viewHolder.acceptTradeButton = (Button)aView.findViewById(R.id.acceptTradeButton);
            viewHolder.rejectTradeButton = (Button)aView.findViewById(R.id.rejectTradeButton);
            viewHolder.counterTradeButton = (Button)aView.findViewById(R.id.counterTradeButton);

            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (PendingTradeViewHolder)aView.getTag();
        }

        if (mPendingTradesList != null && mPendingTradesList.size() > 0)
        {
            final Transaction currentPendingTrade = mPendingTradesList.get(aPosition);
            String title = "";

            viewHolder.cancelTradeButton.setVisibility(View.GONE);
            viewHolder.voteAgainstTradeButton.setVisibility(View.GONE);
            viewHolder.acceptTradeButton.setVisibility(View.GONE);
            viewHolder.rejectTradeButton.setVisibility(View.GONE);
            viewHolder.counterTradeButton.setVisibility(View.GONE);

            if (currentPendingTrade.getTraderTeamKey().equals(sCurrentUserTeamKey))
            {
                viewHolder.cancelTradeButton.setVisibility(View.VISIBLE);

                title = String.format(mContext.getResources().getString(R.string.trade_offer_sent_to_title), currentPendingTrade.getTradeeTeamName());
                viewHolder.cancelTradeButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        mListener.onCancelTrade(currentPendingTrade);
                    }
                });
            }
            else if (currentPendingTrade.getTradeeTeamKey().equals(sCurrentUserTeamKey))
            {
                title = String.format(mContext.getResources().getString(R.string.trade_offer_from_title), currentPendingTrade.getTraderTeamName());
                //viewHolder.acceptTradeButton.setVisibility(View.VISIBLE);
                viewHolder.rejectTradeButton.setVisibility(View.VISIBLE);
                viewHolder.rejectTradeButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View view)
                    {
                        mListener.onRejectTrade(currentPendingTrade);
                    }
                });
                //viewHolder.counterTradeButton.setVisibility(View.VISIBLE);
            }
            else
            {
                title = String.format(mContext.getResources().getString(R.string.trade_between_title), currentPendingTrade.getTraderTeamName(), currentPendingTrade.getTradeeTeamName());
                viewHolder.voteAgainstTradeButton.setVisibility(View.VISIBLE);
            }

            viewHolder.pendingTradeTitle.setText(title);

            viewHolder.pendingTradeTeamName1.setText(currentPendingTrade.getTraderTeamName());
            viewHolder.pendingTradeTeamName2.setText(currentPendingTrade.getTradeeTeamName());

            viewHolder.pendingTradePlayersTeam1.setText(currentPendingTrade.getTraderPlayersString());
            viewHolder.pendingTradePlayersTeam2.setText(currentPendingTrade.getTradeePlayersString());

            String tradeNote = currentPendingTrade.getTradeNote();

            if (!Utils.isNullOrEmptyString(tradeNote))
            {
                viewHolder.pendingTradeDivider2.setVisibility(View.VISIBLE);
                viewHolder.pendingTradeNote.setVisibility(View.VISIBLE);

                viewHolder.pendingTradeNote.setText(currentPendingTrade.getTradeNote());
            }
            else
            {
                viewHolder.pendingTradeDivider2.setVisibility(View.GONE);
                viewHolder.pendingTradeNote.setVisibility(View.GONE);
            }
        }

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    static class PendingTradeViewHolder
    {
        TextView pendingTradeTitle;
        TextView pendingTradeTeamName1;
        TextView pendingTradeTeamName2;
        TextView pendingTradePlayersTeam1;
        TextView pendingTradePlayersTeam2;
        View pendingTradeDivider2;
        TextView pendingTradeNote;
        TableRow tradeActionButtonRow;
        Button cancelTradeButton;
        Button voteAgainstTradeButton;
        Button acceptTradeButton;
        Button rejectTradeButton;
        Button counterTradeButton;
    }
}
