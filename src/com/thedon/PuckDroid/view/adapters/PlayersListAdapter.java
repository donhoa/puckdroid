package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.view.WireTransactionActivity;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PlayersListAdapter extends BaseAdapter
{
    private ArrayList<Player> mPlayersArrayList;

    private LayoutInflater mInflater;
    private Context mContext;

    private PlayersListListener mListener;

    private boolean mCanAddPlayers;

    public interface PlayersListListener
    {
        public void onAddPlayerClick(Player aPlayer);
    }

    public PlayersListAdapter(Context context,  ArrayList<Player> players, PlayersListListener aListener, boolean aCanAddPlayers)
    {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mPlayersArrayList = players;
        mListener = aListener;
        mCanAddPlayers = aCanAddPlayers;
    }

    public int getCount()
    {
        return mPlayersArrayList.size();
    }

    public Object getItem(int aPosition)
    {
        return mPlayersArrayList.get(aPosition);
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        PlayerViewHolder viewHolder;

        if (aView == null)
        {
            aView = mInflater.inflate(R.layout.player_list_item, aParent, false);
            viewHolder = new PlayerViewHolder();
            viewHolder.playerName = (TextView)aView.findViewById(R.id.playerName);
            viewHolder.playerTeam = (TextView)aView.findViewById(R.id.playerTeam);
            viewHolder.positions = (TextView)aView.findViewById(R.id.positions);
            viewHolder.pointsTotal = (TextView)aView.findViewById(R.id.pointsTotal);
            viewHolder.waiverDate = (TextView)aView.findViewById(R.id.waiverDate);
            viewHolder.addPlayerButton = (Button)aView.findViewById(R.id.addPlayerButton);
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (PlayerViewHolder)aView.getTag();
        }

        final Player currentPlayer = mPlayersArrayList.get(aPosition);

        viewHolder.playerName.setText(currentPlayer.getFullName());
        viewHolder.playerTeam.setText(currentPlayer.getTeamFullName());
        viewHolder.positions.setText(currentPlayer.getEligiblePositionsString());
        //viewHolder.pointsTotal.setText(currentPlayer.getPlayerStatistics().getPlayerPoints().getTotal());
        if (currentPlayer.getPlayerOwnership().isOnWaivers())
        {
            viewHolder.waiverDate.setVisibility(View.VISIBLE);
            viewHolder.waiverDate.setText(String.format(mContext.getResources().getString(R.string.playerWaiverDate), currentPlayer.getPlayerOwnership().getSimpleWaiverDate()));
        }
        else
        {
            viewHolder.waiverDate.setVisibility(View.GONE);
        }

        if (currentPlayer.getPlayerOwnership().isFreeAgent() || currentPlayer.getPlayerOwnership().isOnWaivers())
        {
            viewHolder.addPlayerButton.setVisibility(View.VISIBLE);
            viewHolder.addPlayerButton.setEnabled(mCanAddPlayers);
            viewHolder.addPlayerButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    mListener.onAddPlayerClick(currentPlayer);
                }
            });
        }
        else
        {
            viewHolder.addPlayerButton.setVisibility(View.GONE);
        }

        return aView;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    static class PlayerViewHolder
    {
        TextView playerName;
        TextView playerTeam;
        TextView positions;
        TextView pointsTotal;
        TextView waiverDate;
        Button addPlayerButton;
    }
}
