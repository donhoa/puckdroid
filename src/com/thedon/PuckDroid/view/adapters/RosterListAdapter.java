package com.thedon.PuckDroid.view.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.data.StatisticsCategory;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.CustomListItem;
import com.thedon.PuckDroid.view.HeaderListItem;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class RosterListAdapter extends BaseAdapter
{
    private static final String TAG = RosterListAdapter.class.getSimpleName();

    private static final int ROW_TYPE_PLAYER = 0;
    private static final int ROW_TYPE_EMPTY = 1;
    private static final int ROW_TYPE_HEADER = 2;

    private ArrayList<CustomListItem> mRosterPlayersList;

    private LayoutInflater mInflater;

    private Context mContext;

    private boolean mEditMode;

    private static Hashtable<String, StatisticsCategory> sLeagueStats;

    public RosterListAdapter(Context context,  ArrayList<CustomListItem> players, Hashtable<String, StatisticsCategory> aLeagueStats)
    {
        mInflater = LayoutInflater.from(context);
        mContext = context;
        mRosterPlayersList = players;
        sLeagueStats = aLeagueStats;
        mEditMode = false;
    }

    public int getCount()
    {
        return mRosterPlayersList.size();
    }

    public Object getItem(int aPosition)
    {
        return mRosterPlayersList.get(aPosition);
    }

    public View getView(int aPosition, View aView, ViewGroup aParent)
    {
        RosterPlayerViewHolder viewHolder = new RosterPlayerViewHolder();

        int viewType = getItemViewType(aPosition);

        if (aView == null)
        {
            switch (viewType)
            {
                case ROW_TYPE_PLAYER:
                    aView = mInflater.inflate(R.layout.roster_list_item, aParent, false);
                    viewHolder.rosterPlayerName = (TextView)aView.findViewById(R.id.rosterPlayerName);
                    viewHolder.currentPosition = (TextView)aView.findViewById(R.id.currentPosition);
                    viewHolder.rosterPlayerPointsTotal = (TextView)aView.findViewById(R.id.rosterPlayerPointsTotal);
                    viewHolder.eligiblePositions = (Spinner)aView.findViewById(R.id.eligiblePositionsSpinner);
                    viewHolder.rosterPlayerStatsTable = (TableLayout)aView.findViewById(R.id.rosterPlayerStatsTable);
                    viewHolder.statsCategoryRow = (TableRow)aView.findViewById(R.id.statsCategoryRow);
                    viewHolder.statsValueRow = (TableRow)aView.findViewById(R.id.statsValueRow);
                    break;
                case ROW_TYPE_EMPTY:
                    aView = mInflater.inflate(R.layout.empty_spot_roster_list, null);
                    viewHolder.emptySpotPosition = (TextView)aView.findViewById(R.id.emptySpotPosition);
                    viewHolder.emptySpotDescription = (TextView)aView.findViewById(R.id.emptySpotDescription);
                    break;
                case ROW_TYPE_HEADER:
                    aView = mInflater.inflate(R.layout.section_header, null);
                    viewHolder.headerTitle = (TextView)aView.findViewById(android.R.id.title);
                    break;
            }
            aView.setTag(viewHolder);
        }
        else
        {
            viewHolder = (RosterPlayerViewHolder)aView.getTag();
        }

        final CustomListItem currentItem = mRosterPlayersList.get(aPosition);

        if (currentItem.isHeader())
        {
            viewHolder.headerTitle.setText(((HeaderListItem)currentItem).getHeaderTitle());
        }
        else
        {
            final Player currentPlayer = (Player)currentItem;
            if (!currentPlayer.isEmptyPlayer())
            {
                viewHolder.rosterPlayerName.setText(currentPlayer.getFullName());
                viewHolder.currentPosition.setText(currentPlayer.getSelectedPosition().getPosition());

                if (currentPlayer.getPlayerStatistics().getPlayerPoints() != null)
                {
                    viewHolder.rosterPlayerPointsTotal.setText(currentPlayer.getPlayerStatistics().getPlayerPoints().getTotal());
                }

                if (mEditMode)
                {
                    viewHolder.eligiblePositions.setVisibility(View.VISIBLE);
                    viewHolder.rosterPlayerStatsTable.setVisibility(View.GONE);

                    final List<String> eligiblePositions = currentPlayer.getEligiblePositions();
                    final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_dropdown_item, eligiblePositions);

                    viewHolder.eligiblePositions.setAdapter(spinnerArrayAdapter);
                    viewHolder.eligiblePositions.setSelection(eligiblePositions.indexOf(currentPlayer.getSelectedPosition().getPosition()));
                    viewHolder.eligiblePositions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                    {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
                        {
                            currentPlayer.getSelectedPosition().setPosition(spinnerArrayAdapter.getItem(i));
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView)
                        {
                        }
                    });
                }
                else
                {
                    viewHolder.eligiblePositions.setVisibility(View.GONE);
                    viewHolder.rosterPlayerStatsTable.setVisibility(View.VISIBLE);

                    Map<Integer, String> statsTable = currentPlayer.getPlayerStatistics().getStatsTable();
                    buildStatsTable(viewHolder.statsCategoryRow, viewHolder.statsValueRow, statsTable);
                }
            }
            else
            {
                viewHolder.emptySpotPosition.setText(currentPlayer.getSelectedPosition().getPosition());
                viewHolder.emptySpotPosition.setTextColor(Color.RED);
                viewHolder.emptySpotDescription.setText("EMPTY!");
            }
        }

        return aView;
    }

    @Override
    public int getItemViewType(int position)
    {
        CustomListItem currentItem = mRosterPlayersList.get(position);

        if (currentItem.isHeader())
        {
            return ROW_TYPE_HEADER;
        }
        else
        {
            Player currentPlayer = (Player)currentItem;
            return currentPlayer.isEmptyPlayer() ? ROW_TYPE_EMPTY : ROW_TYPE_PLAYER;
        }
    }

    @Override
    public int getViewTypeCount()
    {
        return 3;
    }

    @Override
    public long getItemId(int aLocation)
    {
        return aLocation;
    }

    public void setEditMode(boolean aEditMode)
    {
        mEditMode = aEditMode;
    }

    public ArrayList<CustomListItem> getRosterPlayerList()
    {
        return mRosterPlayersList;
    }

    private void buildStatsTable(TableRow aStatsCatRow, TableRow aStatsValueRow, Map<Integer, String> aStatsTable)
    {
        Log.v(TAG, "buildStatsTable()");

        aStatsCatRow.removeAllViews();
        aStatsValueRow.removeAllViews();

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f);

        for (Hashtable.Entry<Integer, String> entry : aStatsTable.entrySet())
        {
            String statName = Utils.getStatNameById(sLeagueStats, entry.getKey().toString(), true);

            TextView statNameView = new TextView(mContext);
            statNameView.setLayoutParams(layoutParams);
            statNameView.setGravity(Gravity.CENTER_HORIZONTAL);
            statNameView.setText(statName);

            aStatsCatRow.addView(statNameView);

            TextView statValueView = new TextView(mContext);
            statValueView.setLayoutParams(layoutParams);
            statValueView.setGravity(Gravity.CENTER_HORIZONTAL);
            statValueView.setText(entry.getValue());

            aStatsValueRow.addView(statValueView);
        }
    }

    static class RosterPlayerViewHolder
    {
        TextView rosterPlayerName;
        TextView currentPosition;
        TextView rosterPlayerPointsTotal;
        Spinner eligiblePositions;
        TableLayout rosterPlayerStatsTable;
        TableRow statsCategoryRow;
        TableRow statsValueRow;

        TextView emptySpotPosition;
        TextView emptySpotDescription;

        TextView headerTitle;
    }
}
