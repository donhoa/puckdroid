package com.thedon.PuckDroid.view;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.*;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.adapters.RosterPlayersSelectionListAdapter;
import com.thedon.PuckDroid.view.fragments.ConfirmationDialogFragment;
import com.thedon.PuckDroid.view.fragments.RosterPlayerSelectionFragment;
import org.scribe.model.Verb;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class WireTransactionActivity extends FragmentActivity implements ConfirmationDialogFragment.ConfirmDialogListener, RosterPlayerSelectionFragment.RosterPlayerSaveListener
{
    private static final String TAG = WireTransactionActivity.class.getSimpleName();

    private String mTeamKey;
    private TextView mPlayerToAdd;
    private String mPlayerToAddName;
    private String mPlayerToAddKey;
    private boolean mPlayerToAddIsOnWaivers;

    private EditText mFaabBidValueEditText;
    private String mFaabBidValue;

    private RosterPlayerSelectionFragment mPlayerSelectionFragment;

    private ResultReceiver mResultReceiver;

    private ConfirmationDialogFragment mConfirmationDialog;

    private Context mContext;

    private static ArrayList<RosterPlayersSelectionListAdapter.PlayerItem> sSelectedPlayers;

    private LeagueSettings mLeagueSettings;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wire_transaction);

        mContext = this;

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true) ;
        actionBar.setDisplayShowTitleEnabled(false);

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    Log.v(TAG, "RESULT!!!: " + resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT));
                    finish();
                }
            }
        };

        mLeagueSettings = ((PuckDroidApplication)getApplication()).getLeagueSettings();

        mPlayerToAdd = (TextView)findViewById(R.id.wireTransactionPlayerToAdd);

        Bundle extras = getIntent().getExtras();

        if (extras != null)
        {
            mPlayerToAddName = extras.getString(BundleConstants.PLAYER_NAME);
            mPlayerToAddKey= extras.getString(BundleConstants.PLAYER_KEY);
            mPlayerToAddIsOnWaivers = extras.getBoolean(BundleConstants.PLAYER_ON_WAIVERS);
        }

        mConfirmationDialog = new ConfirmationDialogFragment(this, this, getString(R.string.confirm_transaction), "", getString(R.string.ok_button), getString(R.string.cancel_button));

        Preferences preferences = Preferences.getInstance(getApplicationContext());
        mTeamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

        mPlayerToAdd.setText(mPlayerToAddName);

        mPlayerSelectionFragment = new RosterPlayerSelectionFragment(this, RosterPlayerSelectionFragment.SELECTION_TYPE_WIRE_TRANSACTION);

        RelativeLayout faabContainer = (RelativeLayout)findViewById(R.id.faabContainer);

        if (mLeagueSettings.usesFaab() && mPlayerToAddIsOnWaivers)
        {
            faabContainer.setVisibility(View.VISIBLE);
            mFaabBidValueEditText = (EditText)findViewById(R.id.faabValue);
        }

        Bundle args = new Bundle();
        args.putString(BundleConstants.GET_ROSTER_TEAM_KEY, mTeamKey);
        mPlayerSelectionFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction().add(R.id.rosterPlayerSelectionListContainer, mPlayerSelectionFragment).commit();
    }

    @Override
    public void onPositiveButtonClick()
    {
        Log.v(TAG, TAG + ".onPositiveButtonClick()");

        saveTransaction();
    }

    @Override
    public void onSave()
    {
        Log.v(TAG, TAG + ".onSave()");

        sSelectedPlayers = mPlayerSelectionFragment.getSelectedPlayers();
        int selectedPlayersCount = sSelectedPlayers.size();

        if (selectedPlayersCount > 0)
        {
            if (mPlayerToAddIsOnWaivers)
            {
                //TODO: Handle empty textfield and also non numeric or integer values (no letters, no floating values, ...)
                if (mFaabBidValueEditText != null && !Utils.isNullOrEmptyString(mFaabBidValueEditText.getText()))
                {
                    mFaabBidValue = mFaabBidValueEditText.getText().toString();
                }
            }

            if (selectedPlayersCount == 1)
            {
                String transactionDetails = String.format(getString(R.string.wire_transaction_add_drop_details), mPlayerToAddName, sSelectedPlayers.get(0).playerName);
                mConfirmationDialog.setMessage(transactionDetails);
                mConfirmationDialog.show(getSupportFragmentManager(), "WireTransactionConfirmationDialog");
            }
            else
            {
                Toast.makeText(mContext, getString(R.string.error_select_one_player_only), Toast.LENGTH_LONG).show();
            }
        }
        else
        {
            Toast.makeText(mContext, getString(R.string.error_no_player_selected), Toast.LENGTH_LONG).show();
        }
    }

    private void saveTransaction()
    {
        Log.v(TAG, TAG + ".saveTransaction()");

        if (sSelectedPlayers.size() > 0)
        {
            RosterPlayersSelectionListAdapter.PlayerItem player = sSelectedPlayers.get(0);

            Intent intent = new Intent(this, YahooSportsApiService.class);

            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.POST_WIRE_TRANSACTION);
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.POST.name());
            intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
            intent.putExtra(BundleConstants.USER_MOVE_DATA, YahooSportsApiXmlWriter.addDropTransaction(player.playerKey, mPlayerToAddKey, mTeamKey, mFaabBidValue));
            startService(intent);
        }
    }
}