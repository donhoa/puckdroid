package com.thedon.PuckDroid.view;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.*;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.data.NavigationDrawerItem;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.view.adapters.NavigationDrawerAdapter;
import com.thedon.PuckDroid.view.fragments.*;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class ManagerActivity extends FragmentActivity
{
    private static final String TAG = ManagerActivity.class.getSimpleName();

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private ListView mDrawerList;
    private String[] mDrawerListTitles;
    private ArrayList<NavigationDrawerItem> mDrawerListItems = new ArrayList<NavigationDrawerItem>();
    private String mCurrentUserTeamKey;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.manager);

        initNavigationDrawer();

        if (savedInstanceState == null)
        {
            Fragment overviewFragment = new OverviewFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.navigation_drawer_frame, overviewFragment).commit();
        }

        Preferences preferences = Preferences.getInstance(this);
        mCurrentUserTeamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }

        Intent intent;
        switch (item.getItemId())
        {
            case R.id.settingsOption:
                intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
        }
        return (super.onOptionsItemSelected(item));
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
    }

    private void initNavigationDrawer()
    {
        mDrawerListTitles = getResources().getStringArray(R.array.nav_drawer_items);

        for (int i =0; i < mDrawerListTitles.length; i++)
        {
            mDrawerListItems.add(new NavigationDrawerItem(mDrawerListTitles[i]));
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.navigation_drawer_list_view);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                selectItem(position);
                mDrawerLayout.closeDrawer(mDrawerList);
            }
        });

        mDrawerList.setAdapter(new NavigationDrawerAdapter(this, mDrawerListItems));

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.about, R.string.about)
        {
            public void onDrawerClosed(View view)
            {
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView)
            {
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
    }

    private void selectItem(int aPosition)
    {
        mDrawerList.setItemChecked(aPosition, true);

        Intent intent;

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        switch (aPosition)
        {
            case 0:
                Fragment overviewFragment = new OverviewFragment();
                ft.replace(R.id.navigation_drawer_frame, overviewFragment).commit();
                break;
            case 1:
                intent = new Intent(this, TeamProfileActivity.class);
                intent.putExtra(BundleConstants.IS_CURRENT_USER, true);
                intent.putExtra(BundleConstants.GET_TEAM_INFO_KEY, mCurrentUserTeamKey);

                Team currentUserTeam = ((PuckDroidApplication)getApplication()).getCurrentUserTeamInfo();
                intent.putExtra(BundleConstants.GET_TEAM_INFO_NAME, currentUserTeam.getName());

                startActivity(intent);
                break;
            case 2:
                intent = new Intent(this, PendingTradesActivity.class);
                startActivity(intent);
                break;
            case 3:
                Fragment playersFragment = new PlayersFragment();
                ft.replace(R.id.navigation_drawer_frame, playersFragment).commit();
                break;
            case 4:
                intent = new Intent(this, LeagueListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
        }
    }
}
