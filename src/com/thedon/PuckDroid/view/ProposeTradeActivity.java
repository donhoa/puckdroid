package com.thedon.PuckDroid.view;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.*;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.view.adapters.RosterPlayersSelectionListAdapter;
import com.thedon.PuckDroid.view.fragments.RosterPlayerSelectionFragment;
import com.thedon.PuckDroid.view.fragments.TradeConfirmationDialogFragment;
import org.scribe.model.Verb;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class ProposeTradeActivity extends FragmentActivity implements ActionBar.TabListener, TradeConfirmationDialogFragment.TradeConfirmationDialogListener, RosterPlayerSelectionFragment.RosterPlayerSaveListener
{
    private static final String TAG = ProposeTradeActivity.class.getSimpleName();

    private ResultReceiver mResultReceiver;

    private ViewPager mViewPager;
    private Button mSendTradeProposalButton;

    private ProposeTradePagerAdapter mProposeTradePagerAdapter;

    private Team mCurrentUserTeam;
    private String mCurrentUserTeamKey;
    private String mCurrentUserTeamName;
    private String mTradeeTeamKey;
    private String mTradeeTeamName;

    private RosterPlayerSelectionFragment mCurrentUserSelectionFragment;
    private RosterPlayerSelectionFragment mTradeeSelectionFragment;

    ArrayList<RosterPlayersSelectionListAdapter.PlayerItem> mCurrentUserPlayersList;
    ArrayList<RosterPlayersSelectionListAdapter.PlayerItem> mTradeePlayersList;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.propose_trade);

        mCurrentUserTeam = ((PuckDroidApplication)getApplication()).getCurrentUserTeamInfo();
        mCurrentUserTeamName = mCurrentUserTeam.getName();

        Preferences preferences = Preferences.getInstance(getApplicationContext());
        mCurrentUserTeamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

        mTradeeTeamKey = getIntent().getExtras().getString(BundleConstants.PROPOSE_TRADE_TRADEE_TEAM_KEY, "");
        mTradeeTeamName = getIntent().getExtras().getString(BundleConstants.PROPOSE_TRADE_TRADEE_TEAM_NAME, "");

        mCurrentUserSelectionFragment = new RosterPlayerSelectionFragment(this, RosterPlayerSelectionFragment.SELECTION_TYPE_TRADE);
        Bundle currentUserArgs = new Bundle();
        currentUserArgs.putString(BundleConstants.GET_ROSTER_TEAM_KEY, mCurrentUserTeamKey);
        mCurrentUserSelectionFragment.setArguments(currentUserArgs);

        mTradeeSelectionFragment = new RosterPlayerSelectionFragment(this, RosterPlayerSelectionFragment.SELECTION_TYPE_TRADE);
        Bundle tradeeArgs = new Bundle();
        tradeeArgs.putString(BundleConstants.GET_ROSTER_TEAM_KEY, mTradeeTeamKey);
        mTradeeSelectionFragment.setArguments(tradeeArgs);

        mProposeTradePagerAdapter = new ProposeTradePagerAdapter(this, getSupportFragmentManager(), mCurrentUserSelectionFragment, "Don", mTradeeSelectionFragment, mTradeeTeamName);

        final ActionBar actionBar = getActionBar();

        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(String.format(getResources().getString(R.string.propose_trade_title), mTradeeTeamName));

        mViewPager = (ViewPager) findViewById(R.id.propose_trade_pager);
        mViewPager.setAdapter(mProposeTradePagerAdapter);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
        {
            @Override
            public void onPageSelected(int position)
            {
                actionBar.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < mProposeTradePagerAdapter.getCount(); i++)
        {
            actionBar.addTab(actionBar.newTab()
                 .setText(mProposeTradePagerAdapter.getPageTitle(i))
                 .setTabListener(this));
        }

        mSendTradeProposalButton = (Button)findViewById(R.id.sendTradeProposalButton);
        mSendTradeProposalButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showTradeConfirmationDialog();
            }
        });

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    Log.v(TAG, "RESULT!!!: " + resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT));
                    finish();
                }
            }
        };
    }


    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
    {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onPositiveButtonClick(String aTradeNote)
    {
        sendTradeProposal(aTradeNote);

    }

    @Override
    public void onSave()
    {
    }

    private void showTradeConfirmationDialog()
    {
        String currentUserPlayersString = "";
        String tradeePlayersString = "";

        mCurrentUserPlayersList = mCurrentUserSelectionFragment.getSelectedPlayers();
        mTradeePlayersList = mTradeeSelectionFragment.getSelectedPlayers();

        if (mCurrentUserPlayersList.size() == 0)
        {
            Toast.makeText(this, getString(R.string.no_players_selected_current_user), Toast.LENGTH_LONG).show();
            return;
        }

        if (mTradeePlayersList.size() == 0)
        {
            Toast.makeText(this, String.format(getString(R.string.no_players_selected_opposing_user), mTradeeTeamName), Toast.LENGTH_LONG).show();
            return;
        }

        for (RosterPlayersSelectionListAdapter.PlayerItem playerItem : mCurrentUserPlayersList)
        {
            currentUserPlayersString = currentUserPlayersString + playerItem.playerName + "\n";
        }

        for (RosterPlayersSelectionListAdapter.PlayerItem playerItem : mTradeePlayersList)
        {
            tradeePlayersString = tradeePlayersString + playerItem.playerName + "\n";
        }

        FragmentManager fm = getSupportFragmentManager();
        TradeConfirmationDialogFragment confirmationDialog = new TradeConfirmationDialogFragment(mCurrentUserTeamName, mTradeeTeamName, currentUserPlayersString, tradeePlayersString, TradeConfirmationDialogFragment.TRADE_CONFIRMATION_DIALOG_TYPE_PROPOSAL, this);
        confirmationDialog.show(fm, "TradeConfirmationDialogFragment");
    }


    private void sendTradeProposal(String aNote)
    {
        Log.v(TAG, TAG + ".sendTradeProposal()");

        String data = YahooSportsApiXmlWriter.proposeTrade(mCurrentUserTeamKey, mTradeeTeamKey, aNote, mCurrentUserSelectionFragment.getSelectedPlayers(), mTradeeSelectionFragment.getSelectedPlayers());

        Intent intent = new Intent(this, YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.POST_TRADE_PROPOSAL);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.POST.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        intent.putExtra(BundleConstants.USER_MOVE_DATA, data);
        startService(intent);
    }

    public static class ProposeTradePagerAdapter extends FragmentStatePagerAdapter
    {
        Context mContext;

        private RosterPlayerSelectionFragment mCurrentUserSelectionFragment;
        private String mCurrentUserTeamName;
        private RosterPlayerSelectionFragment mTradeeSelectionFragment;
        private String mTradeeTeamName;

        public ProposeTradePagerAdapter(Context context, FragmentManager fm, RosterPlayerSelectionFragment aCurrentUserSelectionFragment, String aCurrentUserTeamName, RosterPlayerSelectionFragment aTradeeSelectionFragment, String aTradeeTeamName)
        {
            super(fm);
            mContext = context;
            mCurrentUserSelectionFragment = aCurrentUserSelectionFragment;
            mCurrentUserTeamName = aCurrentUserTeamName;
            mTradeeSelectionFragment = aTradeeSelectionFragment;
            mTradeeTeamName = aTradeeTeamName;
        }

        @Override
        public Fragment getItem(int i)
        {
            switch (i)
            {
                case 1:
                    return mTradeeSelectionFragment;
                default:
                    return mCurrentUserSelectionFragment;
            }
        }

        @Override
        public int getCount()
        {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position)
            {
                case 1:
                    return mTradeeTeamName;
                default:
                    return mCurrentUserTeamName;
            }
        }
    }
}