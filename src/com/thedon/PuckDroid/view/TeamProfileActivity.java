package com.thedon.PuckDroid.view;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.view.fragments.RosterFragment;
import com.thedon.PuckDroid.view.fragments.TeamProfileInfoFragment;
import com.thedon.PuckDroid.view.fragments.WaiverClaimsFragment;

import java.util.ArrayList;
import java.util.List;


/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class TeamProfileActivity extends FragmentActivity
{
    private static final String TAG = TeamProfileActivity.class.getSimpleName();

    private TeamProfilePagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    private boolean mIsCurrentUser;
    private String mTeamKey;
    private String mTeamName;

    private LeagueSettings mLeagueSettings;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.team_profile);

        mIsCurrentUser = getIntent().getExtras().getBoolean(BundleConstants.IS_CURRENT_USER);
        mTeamKey = getIntent().getExtras().getString(BundleConstants.GET_TEAM_INFO_KEY);
        mTeamName = getIntent().getExtras().getString(BundleConstants.GET_TEAM_INFO_NAME);

        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(mTeamName);

        PagerTitleStrip titleStrip = (PagerTitleStrip)findViewById(R.id.teamProfileViewPagerTitleStrip);
        titleStrip.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

        mLeagueSettings = ((PuckDroidApplication)getApplication()).getLeagueSettings();

        initFragments();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.team_profile_menu, menu);

        MenuItem proposeTradeMenuItem = menu.findItem(R.id.proposeTradeOption);

        if (!mIsCurrentUser)
        {
            proposeTradeMenuItem.setVisible(true);
        }

        proposeTradeMenuItem.setEnabled(!mLeagueSettings.isPastTradeDeadline());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
            case R.id.proposeTradeOption:
                Intent intent = new Intent(this, ProposeTradeActivity.class);
                intent.putExtra(BundleConstants.PROPOSE_TRADE_TRADEE_TEAM_KEY, mTeamKey);
                intent.putExtra(BundleConstants.PROPOSE_TRADE_TRADEE_TEAM_NAME, mTeamName);
                startActivity(intent);
        }

        return (super.onOptionsItemSelected(item));
    }

    private void initFragments()
    {
        List<Fragment> fragments = new ArrayList<Fragment>();

        Bundle teamProfileFragmentArgs = new Bundle();
        teamProfileFragmentArgs.putString(BundleConstants.GET_TEAM_INFO_KEY, mTeamKey);
        TeamProfileInfoFragment teamProfileInfoFragment = new TeamProfileInfoFragment();
        teamProfileInfoFragment.setArguments(teamProfileFragmentArgs);

        fragments.add(teamProfileInfoFragment);

        Bundle rosterFragmentArgs = new Bundle();
        rosterFragmentArgs.putString(BundleConstants.GET_ROSTER_TEAM_KEY, mTeamKey);
        RosterFragment rosterFragment = new RosterFragment();
        rosterFragment.setArguments(rosterFragmentArgs);
        fragments.add(rosterFragment);

        if (mIsCurrentUser)
        {
            fragments.add(new WaiverClaimsFragment());
        }

        mPagerAdapter  = new TeamProfilePagerAdapter(getSupportFragmentManager(), this, fragments);

        mViewPager = (ViewPager) findViewById(R.id.teamProfileViewPager);
        mViewPager.setOffscreenPageLimit(2);

        mViewPager.setAdapter(mPagerAdapter);
    }

    public static class TeamProfilePagerAdapter extends FragmentStatePagerAdapter
    {
        private Context mContext;
        List<Fragment> mFragmentsList = new ArrayList<Fragment>();

        public TeamProfilePagerAdapter(FragmentManager fm, Context aContext, List<Fragment> aFragmentsList)
        {
            super(fm);
            mContext = aContext;
            mFragmentsList = aFragmentsList;
        }

        @Override
        public Fragment getItem(int i)
        {
            return mFragmentsList.get(i);
        }

        @Override
        public int getCount()
        {
            return mFragmentsList.size();
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            String[] titles;

            if (getCount() == 3)
            {
                titles = mContext.getResources().getStringArray(R.array.team_profile_pages_titles);
            }
            else
            {
                titles = mContext.getResources().getStringArray(R.array.team_profile_pages_titles_no_waiver);
            }

            return titles[position];
        }
    }

}