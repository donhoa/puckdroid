package com.thedon.PuckDroid.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.*;
import com.thedon.PuckDroid.utils.Utils;

public class LoginActivity extends BaseActivity
{
    private static final String TAG = LoginActivity.class.getSimpleName();

    private OAuthManager mOAuthManager;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        if (skipLogin())
        {
            return;
        }

        setContentView(R.layout.main);

        Button login = (Button)findViewById(R.id.loginBtn);

        final OAuthGetRequestTokenTask task = new OAuthGetRequestTokenTask(this);

        login.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                task.execute();
            }
        });
    }

    private boolean skipLogin()
    {
        Log.v(TAG, TAG + ".skipLogin()");

        boolean skipLogin = false;

        Preferences preferences = Preferences.getInstance(getApplicationContext());

        String accessToken = preferences.getString(Preferences.KEY_ACCESS_TOKEN, "");
        String accessSecret = preferences.getString(Preferences.KEY_ACCESS_SECRET, "");

        Log.v(TAG, TAG + ".skipLogin() accessToken: " + accessToken + " - accessSecret: " + accessSecret);

        if (!Utils.isNullOrEmptyString(accessToken) && !Utils.isNullOrEmptyString(accessSecret))
        {
            skipLogin = true;
            String leagueKey = preferences.getString(Preferences.KEY_LEAGUE_KEY, "");
            String teamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

            Log.v(TAG, TAG + ".skipLogin() leagueKey: " + leagueKey + " - teamKey: " + teamKey);

            if (!Utils.isNullOrEmptyString(leagueKey) && !Utils.isNullOrEmptyString(teamKey))
            {
                getGlobalInfo();
            }
            else
            {
                Intent intent = new Intent(this, LeagueListActivity.class);
                intent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)); //Clear the activity task which makes LeagueListActivity the root activity
                startActivity(intent);
                finish();
            }
        }

        return skipLogin;
    }

    protected void onGlobalInfoSaved()
    {
        Log.v(TAG, "onLeagueSettingsSaved()");

        super.onGlobalInfoSaved();
        finish();
    }

    private class OAuthGetRequestTokenTask extends AsyncTask<Void, Void, Void>
    {
        private Context mContext;

        public OAuthGetRequestTokenTask(Context aContext)
        {
            mContext = aContext;
            mOAuthManager = OAuthManager.getInstance(mContext);
        }
        @Override
        protected void onPreExecute()
        {
            mLoadingAnimation = ProgressDialog.show(mContext, "", mContext.getResources().getString(R.string.oauth_step_1), true, false, null);
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            Log.v(TAG, "OAuthGetRequestTokenTask.doInBackground()");

            String authorizationURL = null;
            if (mOAuthManager != null)
            {
                authorizationURL = mOAuthManager.getRequestToken();

                Log.v(TAG, "authorizationURL: " + authorizationURL);

                if (!Utils.isNullOrEmptyString(authorizationURL))
                {
                    Intent intent = new Intent(mContext, YahooAuthenticationActivity.class);
                    intent.putExtra(BundleConstants.YAHOO_AUTHORIZATION_URL, authorizationURL);
                    startActivity(intent);
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            if (mLoadingAnimation != null)
            {
                mLoadingAnimation.dismiss();
            }
        }
    }
}
