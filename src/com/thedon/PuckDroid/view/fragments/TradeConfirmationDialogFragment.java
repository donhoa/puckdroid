package com.thedon.PuckDroid.view.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.thedon.PuckDroid.R;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class TradeConfirmationDialogFragment extends DialogFragment
{
    public static final int TRADE_CONFIRMATION_DIALOG_TYPE_PROPOSAL = 0;
    public static final int TRADE_CONFIRMATION_DIALOG_TYPE_ACCEPT = 1;

    private int mType;

    private TextView mTradeConfirmationMessage;
    private EditText mTradeNote;

    private TextView mTradeTeamName1;
    private TextView mTradeTeamName2;
    private TextView mTradePlayersTeam1;
    private TextView mTradePlayersTeam2;

    private Button mPositiveButton;
    private Button mNegativeButton;

    private String mCurrentUserTeamName;
    private String mOpposingOwnerTeamName;
    private String mCurrentUserPlayersList;
    private String mOpposingOwnerPlayersList;

    private TradeConfirmationDialogListener mListener;

    public interface TradeConfirmationDialogListener
    {
        public void onPositiveButtonClick(String aTradeNote);
    }

    public TradeConfirmationDialogFragment(String aCurrentUserTeamName, String aOpposingOwnerTeamName, String aCurrentUserPlayersList, String aOpposingOwnerPlayersList, int aType, TradeConfirmationDialogListener aListener)
    {
        mCurrentUserTeamName = aCurrentUserTeamName;
        mOpposingOwnerTeamName = aOpposingOwnerTeamName;
        mCurrentUserPlayersList = aCurrentUserPlayersList;
        mOpposingOwnerPlayersList = aOpposingOwnerPlayersList;
        mType = aType;
        mListener = aListener;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.trade_confirmation_dialog_fragment, container);

        mTradeConfirmationMessage = (TextView)view.findViewById(R.id.tradeConfirmationMessage);
        mTradeNote = (EditText)view.findViewById(R.id.tradeNoteEditText);
        mPositiveButton = (Button)view.findViewById(R.id.tradeConfirmationPositiveButton);
        mNegativeButton = (Button)view.findViewById(R.id.tradeConfirmationNegativeButton);

        mTradeTeamName1 = (TextView)view.findViewById(R.id.tradeTeamName1);
        mTradeTeamName2 = (TextView)view.findViewById(R.id.tradeTeamName2);
        mTradePlayersTeam1 = (TextView)view.findViewById(R.id.tradePlayersTeam1);
        mTradePlayersTeam2 = (TextView)view.findViewById(R.id.tradePlayersTeam2);

        String message = String.format(getActivity().getResources().getString(R.string.trade_proposal_confirmation_message), mOpposingOwnerTeamName);
        mTradeConfirmationMessage.setText(message);

        mTradeTeamName1.setText(mCurrentUserTeamName);
        mTradeTeamName2.setText(mOpposingOwnerTeamName);
        mTradePlayersTeam1.setText(mCurrentUserPlayersList);
        mTradePlayersTeam2.setText(mOpposingOwnerPlayersList);

        getDialog().setTitle(getActivity().getResources().getString(R.string.confirm_transaction));

        mPositiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String note = mTradeNote.getText().toString();
                mListener.onPositiveButtonClick(note);
                dismiss();
            }
        });

        mNegativeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        return view;
    }

}
