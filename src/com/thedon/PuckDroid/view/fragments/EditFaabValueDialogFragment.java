package com.thedon.PuckDroid.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.utils.Utils;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class EditFaabValueDialogFragment extends DialogFragment
{
    private Context mContext;
    private TextView mEditFaabMessage;
    private EditText mFaabValue;

    private String mTransactionKey;
    private String mPlayerName;

    private EditFaabValueDialogListener mListener;

    private Button mNegativeButton;
    private Button mPositiveButton;

    public interface EditFaabValueDialogListener
    {
        public void onSaveFaabValue(String aTransactionKey, String aFaabValue);
    }

    public EditFaabValueDialogFragment(Context aContext, String aTransactionKey, String aPlayerName, EditFaabValueDialogListener aListener)
    {
        mContext = aContext;
        mTransactionKey = aTransactionKey;
        mPlayerName = aPlayerName;
        mListener = aListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.edit_faab_value_dialog_fragment, container);

        mEditFaabMessage = (TextView)view.findViewById(R.id.editFaabValueMessage);
        mFaabValue = (EditText)view.findViewById(R.id.editFaabValueEditText);
        mNegativeButton = (Button)view.findViewById(R.id.editFaabValueNegativeButton);
        mPositiveButton = (Button)view.findViewById(R.id.editFaabValuePositiveButton);

        mEditFaabMessage.setText(String.format(mContext.getResources().getString(R.string.edit_waiver_claim_message), mPlayerName));

        mNegativeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        mPositiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String faabValue = mFaabValue.getText().toString();
                if (!Utils.isNullOrEmptyString(faabValue) && Utils.isStringNumeric(faabValue))
                {
                    mListener.onSaveFaabValue(mTransactionKey, faabValue);
                    dismiss();
                }
                else
                {
                    Toast.makeText(mContext, mContext.getResources().getString(R.string.faab_bid_invalid_amount), Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }
}
