package com.thedon.PuckDroid.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.StatisticsCategory;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.data.parsers.PoolTeamXmlParser;
import com.thedon.PuckDroid.utils.Utils;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class TeamProfileInfoFragment extends BaseViewPagerFragment
{
    private static final String TAG = TeamProfileInfoFragment.class.getSimpleName();

    private Team mTeam;
    private String mTeamKey;

    private ImageView mTeamImageView;
    private TextView mTeamName;
    private TextView mManagerName;

    private TableLayout mTeamInfoTable;
    private TableLayout mTeamPointsTable;
    private TableLayout mTeamStatsTable;


    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mTeamKey = getArguments().getString(BundleConstants.GET_TEAM_INFO_KEY, "");

        mContext = getActivity();

        mLeagueSettings = ((PuckDroidApplication)getActivity().getApplication()).getLeagueSettings();

        getTeamInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.v(TAG, TAG + ".onCreateView()");

        View view = inflater.inflate(R.layout.team_profile_info_fragment, container, false);

        mTeamName = (TextView)view.findViewById(R.id.teamProfileName);
        mManagerName = (TextView)view.findViewById(R.id.teamProfileManagerName);
        mTeamImageView = (ImageView)view.findViewById(R.id.teamProfileImage);
        mTeamInfoTable = (TableLayout)view.findViewById(R.id.teamInfoTable);
        mTeamPointsTable = (TableLayout)view.findViewById(R.id.teamPointsTable);
        mTeamStatsTable = (TableLayout)view.findViewById(R.id.teamStatsTable);

        setHasOptionsMenu(true);

        return view;
    }

    private void getTeamInfo()
    {
        Intent intent = new Intent(mContext, YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TEAM_INFO);
        intent.putExtra(BundleConstants.GET_TEAM_INFO_KEY, mTeamKey);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        buildList(aData);
    }

    protected void buildList(String aData)
    {
        Log.v(TAG, "buildList()");

        try
        {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType=parser.getEventType();
            PoolTeamXmlParser poolTeamXmlParser = new PoolTeamXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(parser.getName().equalsIgnoreCase("team"))
                    {
                        mTeam = poolTeamXmlParser.readTeam(parser);
                    }
                }
                eventType = parser.next();
            }

            if (mTeam != null)
            {
                buildTeamInfoTable();
                buildTeamStatsTable();

                if (mLeagueSettings.getLeagueType() == LeagueSettings.LEAGUE_TYPE_ROTISSERIE)
                {
                    buildTeamPointsTable();
                }
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void buildTeamInfoTable()
    {
        new GetTeamPictureAsyncTask(mContext, mTeamImageView, mTeam.getTeamLogo().getUrl()).execute();

        mTeamName.setText(mTeam.getName());
        mManagerName.setText(mTeam.getManagers().get(0).getNickName());

        if (mTeam.getStandings() != null && mTeam.getStandings().getRecord() != null)
        {
            String record = mTeam.getStandings().getRecord().getRecord();

            if (!Utils.isNullOrEmptyString(record))
            {
                TableRow recordRow = new TableRow(mContext);
                TextView recordView = new TextView(mContext);
                recordView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                recordView.setGravity(Gravity.CENTER_HORIZONTAL);
                recordView.setText(String.format(getString(R.string.record), record ));
                recordRow.addView(recordView);
                mTeamInfoTable.addView(recordRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            }
        }

        TableRow waiverPriorityRow = new TableRow(mContext);
        TextView waiverPriorityView = new TextView(mContext);
        waiverPriorityView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
        waiverPriorityView.setGravity(Gravity.CENTER_HORIZONTAL);
        waiverPriorityView.setText(String.format(getString(R.string.waiver_priority), mTeam.getWaiverPriority()));
        waiverPriorityRow.addView(waiverPriorityView);
        mTeamInfoTable.addView(waiverPriorityRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        TableRow numberOfMovesRow = new TableRow(mContext);
        TextView numberOfMovesView = new TextView(mContext);
        numberOfMovesView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
        numberOfMovesView.setGravity(Gravity.CENTER_HORIZONTAL);
        numberOfMovesView.setText(String.format(getString(R.string.number_of_moves), mTeam.getNumberOfMoves()));
        numberOfMovesRow.addView(numberOfMovesView);
        mTeamInfoTable.addView(numberOfMovesRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        TableRow numberOfTradesRow = new TableRow(mContext);
        TextView numberOfTradesView = new TextView(mContext);
        numberOfTradesView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
        numberOfTradesView.setGravity(Gravity.CENTER_HORIZONTAL);
        numberOfTradesView.setText(String.format(getString(R.string.number_of_trades), mTeam.getNumberOfTrades()));
        numberOfTradesRow.addView(numberOfTradesView);
        mTeamInfoTable.addView(numberOfTradesRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

        if (mLeagueSettings.usesFaab())
        {
            TableRow faabBalanceRow = new TableRow(mContext);
            TextView faabBalanceView = new TextView(mContext);
            faabBalanceView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
            faabBalanceView.setGravity(Gravity.CENTER_HORIZONTAL);
            faabBalanceView.setText(String.format(getString(R.string.free_agent_budget), mTeam.getFaabBalance()));
            faabBalanceRow.addView(faabBalanceView);
            mTeamInfoTable.addView(faabBalanceRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
        }
    }

    private void buildTeamPointsTable()
    {
        ArrayList<Team.Statistic> pointsStats = mTeam.getPoints().getStats();

        if (pointsStats != null)
        {
            mTeamPointsTable.setVisibility(View.VISIBLE);

            Hashtable<String, StatisticsCategory> leagueStats = mLeagueSettings.getStats();

            for (Team.Statistic stat : pointsStats)
            {
                if (!stat.getStatId().equals(String.valueOf(StatisticsCategory.STAT_ID_SAVES)) && !stat.getStatId().equals(String.valueOf(StatisticsCategory.STAT_ID_SHOTS_AGAINST)))
                {
                    TableRow pointsRow = new TableRow(mContext);
                    TextView statName = new TextView(mContext);
                    statName.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    statName.setText(Utils.getStatNameById(leagueStats, stat.getStatId(), false));
                    pointsRow.addView(statName);

                    TextView statValue = new TextView(mContext);
                    statValue.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    statValue.setText(stat.getValue());
                    pointsRow.addView(statValue);

                    mTeamPointsTable.addView(pointsRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }
            }
        }
    }

    private void buildTeamStatsTable()
    {
        ArrayList<Team.Statistic> stats = mTeam.getTeamStats();

        Hashtable<String, StatisticsCategory> leagueStats = mLeagueSettings.getStats();

        if (stats != null)
        {
            for (Team.Statistic stat : stats)
            {
                if (!stat.getStatId().equals(String.valueOf(StatisticsCategory.STAT_ID_SAVES)) && !stat.getStatId().equals(String.valueOf(StatisticsCategory.STAT_ID_SHOTS_AGAINST)))
                {
                    TableRow pointsRow = new TableRow(mContext);
                    TextView statName = new TextView(mContext);
                    statName.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    statName.setText(Utils.getStatNameById(leagueStats, stat.getStatId(), false));
                    pointsRow.addView(statName);

                    TextView statValue = new TextView(mContext);
                    statValue.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                    statValue.setText(stat.getValue());
                    pointsRow.addView(statValue);

                    mTeamStatsTable.addView(pointsRow, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }
            }
        }
    }

    private class GetTeamPictureAsyncTask extends AsyncTask<Void, Void, Bitmap>
    {
        private Context mContext;
        private ImageView mImageView;
        private String mUrl;

        public GetTeamPictureAsyncTask(Context aContext, ImageView aImageView, String aUrl)
        {
            mContext = aContext;
            mImageView = aImageView;
            mUrl = aUrl;
        }

        @Override
        protected Bitmap doInBackground(Void... voids)
        {
            Log.v(TAG, "GetTeamPictureAsyncTask.doInBackground()");
            Bitmap img = null;
            try
            {
                URL url = new URL(mUrl);
                HttpURLConnection connection  = (HttpURLConnection) url.openConnection();

                InputStream is = connection.getInputStream();
                img = BitmapFactory.decodeStream(is);
            }
            catch (IOException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            return img;
        }

        @Override
        protected void onPostExecute(Bitmap result)
        {
            if (result != null)
            {
                mImageView.setImageBitmap(result);
            }
        }
    }

}
