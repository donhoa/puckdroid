package com.thedon.PuckDroid.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.application.YahooSportsApiXmlWriter;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.view.WireTransactionActivity;
import org.scribe.model.Verb;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PickAddTypeDialogFragment extends DialogFragment implements ConfirmationDialogFragment.ConfirmDialogListener
{
    private static final String TAG = PickAddTypeDialogFragment.class.getSimpleName();

    private Context mContext;
    private Player mPlayer;

    private String mTeamKey;

    private TextView mPickAddTypeMessage;
    private RadioGroup mRadioGroup;
    private Button mPositiveButton;
    private Button mNegativeButton;

    private ConfirmationDialogFragment mConfirmationDialogFragment;

    private ResultReceiver mResultReceiver;

    private ConfirmationDialogFragment.ConfirmDialogListener mConfirmDialogListener;

    public PickAddTypeDialogFragment(Context aContext, String aTeamKey, Player aPlayer, ResultReceiver aResultReceiver, ConfirmationDialogFragment.ConfirmDialogListener aListener)
    {
        mContext = aContext;
        mTeamKey = aTeamKey;
        mPlayer = aPlayer;
        mResultReceiver = aResultReceiver;
        mConfirmDialogListener = aListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);

        mConfirmationDialogFragment = new ConfirmationDialogFragment(mContext, this, mContext.getString(R.string.confirm_transaction), "", mContext.getString(R.string.ok_button), mContext.getString(R.string.cancel_button));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.pick_add_type_dialog_fragment, container);

        mPickAddTypeMessage = (TextView)view.findViewById(R.id.pickAddTypeMessage);
        mPickAddTypeMessage.setText(String.format(mContext.getString(R.string.pick_add_type_message), mPlayer.getFullName()));
        mRadioGroup = (RadioGroup)view.findViewById(R.id.pickAddTypeRadioGroup);

        mNegativeButton = (Button)view.findViewById(R.id.pickAddTypeNegativeButton);
        mPositiveButton = (Button)view.findViewById(R.id.pickAddTypePositiveButton);

        mNegativeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        mPositiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int pickedId = mRadioGroup.getCheckedRadioButtonId();

                if (pickedId == R.id.addOnlyRadioButton)
                {
                    mConfirmationDialogFragment.setMessage(String.format(mContext.getString(R.string.wire_transaction_add_details), mPlayer.getFullName()));
                    mConfirmationDialogFragment.show(getActivity().getSupportFragmentManager(), "WireTransactionConfirmationDialog");
                }
                else if (pickedId == R.id.addAndDropRadioButton)
                {
                    Intent intent = new Intent(mContext, WireTransactionActivity.class);
                    intent.putExtra(BundleConstants.PLAYER_KEY, mPlayer.getPlayerKey());
                    intent.putExtra(BundleConstants.PLAYER_NAME, mPlayer.getFullName());
                    intent.putExtra(BundleConstants.PLAYER_ON_WAIVERS, mPlayer.getPlayerOwnership().isOnWaivers());
                    mContext.startActivity(intent);
                }
            }
        });

        return view;
    }

    @Override
    public void onPositiveButtonClick()
    {
        Intent intent = new Intent(mContext, YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.POST_WIRE_TRANSACTION);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.POST.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        intent.putExtra(BundleConstants.USER_MOVE_DATA, YahooSportsApiXmlWriter.addTransaction(mPlayer.getPlayerKey(), mTeamKey, ""));
        getActivity().startService(intent);

        mConfirmDialogListener.onPositiveButtonClick();

        dismiss();
    }
}
