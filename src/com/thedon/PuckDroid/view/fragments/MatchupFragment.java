package com.thedon.PuckDroid.view.fragments;

import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.*;
import com.thedon.PuckDroid.data.parsers.GameXmlParser;
import com.thedon.PuckDroid.data.parsers.MatchupXmlParser;
import com.thedon.PuckDroid.data.parsers.PoolTeamXmlParser;
import com.thedon.PuckDroid.utils.Utils;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class MatchupFragment extends BaseViewPagerFragment
{
    private static final String TAG = MatchupFragment.class.getSimpleName();

    private TableLayout mMatchupTable;
    private TextView mMatchupDate;
    private TextView mMatchupTeam1;
    private TextView mMatchupTeam2;
    private TextView mMatchupTeamRecord1;
    private TextView mMatchupTeamRecord2;
    private TextView mMatchupTeamTotal1;
    private TextView mMatchupTeamTotal2;

    private Matchup mMatchup;

    private Team mMatchupOpponentTeam;

    private String mCurrentWeek;


    public static Fragment newInstance()
    {
        return new MatchupFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        Preferences preferences = Preferences.getInstance(mContext);
        mCurrentWeek = preferences.getString(Preferences.KEY_CURRENT_WEEK, "");

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TEAM_MATCHUP);
        intent.putExtra(BundleConstants.MATCHUP_WEEK, mCurrentWeek);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);

        mCurrentUserTeamInfo = ((PuckDroidApplication)getActivity().getApplication()).getCurrentUserTeamInfo();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.v(TAG, TAG + ".onCreateView()");

        View view = inflater.inflate(R.layout.matchup_fragment, container, false);

        mMatchupTable = (TableLayout)view.findViewById(R.id.matchupTable);

        mMatchupDate = (TextView)view.findViewById(R.id.matchupDate);
        mMatchupTeam1 = (TextView)view.findViewById(R.id.matchupTeamName1);
        mMatchupTeam2 = (TextView)view.findViewById(R.id.matchupTeamName2);

        mMatchupTeamRecord1 = (TextView)view.findViewById(R.id.matchupTeamRecord1);
        mMatchupTeamRecord2 = (TextView)view.findViewById(R.id.matchupTeamRecord2);

        mMatchupTeamTotal1 = (TextView)view.findViewById(R.id.matchupTeamTotal1);
        mMatchupTeamTotal2 = (TextView)view.findViewById(R.id.matchupTeamTotal2);

        setHasOptionsMenu(true);

        mMatchupTeamRecord1.setText(mCurrentUserTeamInfo.getStandings().getRecord().getRecord());

//        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.standingsLoadingAnimation);
//        mLoadingSpinner.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        if (aOriginalRequest.equals(BundleConstants.GET_TEAM_MATCHUP))
        {
            buildList(aData);
        }
        else if (aOriginalRequest.equals(BundleConstants.GET_TEAM_INFO))
        {
            parseMatchupOpponentTeamInfo(aData);
        }
    }

    protected void buildList(String aData)
    {
        Log.v(TAG, TAG + ".buildList()");

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            MatchupXmlParser matchupXmlParser = new MatchupXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("matchup"))
                    {
                        mMatchup = matchupXmlParser.readMatchup(mXmlPullParser);
                    }
                }
                eventType = mXmlPullParser.next();
            }

            if (mMatchup != null)
            {
                buildMatchupTable();
            }
            //mLoadingSpinner.setVisibility(View.GONE);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        super.onCreateOptionsMenu(menu, inflater);
    }

    private void getMatchupWeekDates(String aData)
    {
        Log.v(TAG, TAG + ".getMatchupWeekDates()");

        Game game = null;

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            GameXmlParser gameXmlParser = new GameXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("game"))
                    {
                        game = gameXmlParser.readGame(mXmlPullParser);
                    }
                }
                eventType = mXmlPullParser.next();
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void buildMatchupTable()
    {
        Log.v(TAG, TAG + ".buildMatchupTable()");

        ArrayList<Team> teams = mMatchup.getTeams();

        Team team1;
        Team team2;

        if (teams.size() == 2)
        {
            team1 = teams.get(0);
            team2 = teams.get(1);

            mMatchupTeam1.setText(team1.getName());
            mMatchupTeam2.setText(team2.getName());

            int statsCount = team1.getTeamStats().size();

            mMatchupTeamTotal1.setText(team1.getPoints().getTotal());
            mMatchupTeamTotal2.setText(team2.getPoints().getTotal());

            getMatchupOpponentTeamInfo(team2.getTeamKey());

            mLeagueSettings = ((PuckDroidApplication)(getActivity().getApplication())).getLeagueSettings();
            Hashtable<String, StatisticsCategory> statsTable = mLeagueSettings.getStats();

            for (int i = 0; i < statsCount; i++)
            {
                Team.Statistic statTeam1 = team1.getTeamStats().get(i);
                Team.Statistic statTeam2 = team2.getTeamStats().get(i);
                String statName = Utils.getStatNameById(statsTable, statTeam1.getStatId(), true);

                String currentStatId = statTeam1.getStatId();

                if (!currentStatId.equals(String.valueOf(StatisticsCategory.STAT_ID_SAVES)) && !currentStatId.equals(String.valueOf(StatisticsCategory.STAT_ID_SHOTS_AGAINST)))
                {
                    TableRow row = new TableRow(mContext);

                    TextView statTeam1View = new TextView(mContext);
                    statTeam1View.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
                    statTeam1View.setGravity(Gravity.CENTER_HORIZONTAL);

                    TextView statTeam2View = new TextView(mContext);
                    statTeam2View.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 1f));
                    statTeam2View.setGravity(Gravity.CENTER_HORIZONTAL);

                    TextView statNameView = new TextView(mContext);
                    statNameView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 0.5f));
                    statNameView.setGravity(Gravity.CENTER_HORIZONTAL);

                    statNameView.setText(statName);

                    String statTeam1String = statTeam1.getValue();
                    String statTeam2String = statTeam2.getValue();

                    //TODO: Clean up
                    try
                    {
                        if (currentStatId.equals(String.valueOf(StatisticsCategory.STAT_ID_GAA)))
                        {
                            Float stat1 = 0.0f;
                            Float stat2 = 0.0f;

                            if (!Utils.isNullOrEmptyString(statTeam1String))
                            {
                                stat1 = Float.valueOf(statTeam1String);
                            }
                            else
                            {
                                statTeam1String = "-";
                            }

                            if (!Utils.isNullOrEmptyString(statTeam2String))
                            {
                                stat2 = Float.valueOf(statTeam2String);
                            }
                            else
                            {
                                statTeam2String = "-";
                            }

                            if ((statTeam2String.equals("-") && !statTeam1String.equals("-")) || stat1 < stat2)
                            {
                                statTeam1View.setBackgroundColor(Color.BLUE);
                            }
                            else if ((statTeam1String.equals("-") && !statTeam2String.equals("-")) || stat2 < stat1)
                            {
                                statTeam2View.setBackgroundColor(Color.BLUE);
                            }
                        }
                        else
                        {
                            int statTeam1Value = 0;
                            int statTeam2Value = 0;

                            if (!Utils.isNullOrEmptyString(statTeam1String))
                            {
                                if (currentStatId.equals(String.valueOf(StatisticsCategory.STAT_ID_SAVE_PERCENTAGE)))
                                {
                                    statTeam1Value = Integer.parseInt(statTeam1String.substring(1));
                                }
                                else
                                {
                                    statTeam1Value = Integer.parseInt(statTeam1String);
                                }
                            }
                            else
                            {
                                statTeam1String = "-";
                            }

                            if (!Utils.isNullOrEmptyString(statTeam2String))
                            {
                                if (currentStatId.equals(String.valueOf(StatisticsCategory.STAT_ID_SAVE_PERCENTAGE)))
                                {
                                    statTeam2Value = Integer.parseInt(statTeam2String.substring(1));
                                }
                                else
                                {
                                    statTeam2Value = Integer.parseInt(statTeam2String);
                                }
                            }
                            else
                            {
                                statTeam2String = "-";
                            }

                            if ((statTeam2String.equals("-") && !statTeam1String.equals("-")) || statTeam1Value > statTeam2Value)
                            {
                                statTeam1View.setBackgroundColor(Color.BLUE);
                            }
                            else if ((statTeam1String.equals("-") && !statTeam2String.equals("-"))  || statTeam1Value < statTeam2Value)
                            {
                                statTeam2View.setBackgroundColor(Color.BLUE);
                            }
                        }

                        statTeam1View.setText(statTeam1String);
                        statTeam2View.setText(statTeam2String);
                    }
                    catch (NumberFormatException e)
                    {
                        e.printStackTrace();
                    }
                    catch (IndexOutOfBoundsException e)
                    {
                        e.printStackTrace();
                    }

                    row.addView(statTeam1View);
                    row.addView(statNameView);
                    row.addView(statTeam2View);

                    mMatchupTable.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                }
            }

            String dateString = getResources().getString(R.string.matchup_date);
            mMatchupDate.setText(String.format(dateString, mMatchup.getWeek(), mMatchup.getWeekStartSimple() + " - " + mMatchup.getWeekEndSimple()));
        }
    }

    private void getMatchupOpponentTeamInfo(String aTeamKey)
    {
        Intent intent = new Intent(mContext, YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TEAM_INFO);
        intent.putExtra(BundleConstants.GET_TEAM_INFO_KEY, aTeamKey);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);
    }

    private void parseMatchupOpponentTeamInfo(String aData)
    {
        try
        {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType=parser.getEventType();
            PoolTeamXmlParser poolTeamXmlParser = new PoolTeamXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(parser.getName().equalsIgnoreCase("team"))
                    {
                        mMatchupOpponentTeam = poolTeamXmlParser.readTeam(parser);
                    }
                }
                eventType = parser.next();
            }

            if (mMatchupOpponentTeam != null)
            {
                mMatchupTeamRecord2.setText(mMatchupOpponentTeam.getStandings().getRecord().getRecord());
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
