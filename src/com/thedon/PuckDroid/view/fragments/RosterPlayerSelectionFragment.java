package com.thedon.PuckDroid.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.data.parsers.PlayersXmlParser;
import com.thedon.PuckDroid.view.adapters.RosterPlayersSelectionListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class RosterPlayerSelectionFragment extends ListFragment implements RosterPlayersSelectionListAdapter.RosterPlayersSelectionListListener
{
    private static final String TAG = RosterPlayerSelectionFragment.class.getSimpleName();

    private int mFragmentType;

    public static final int SELECTION_TYPE_WIRE_TRANSACTION = 0;
    public static final int SELECTION_TYPE_TRADE = 1;

    private Context mContext;
    private ResultReceiver mResultReceiver;

    private ArrayList<RosterPlayersSelectionListAdapter.PlayerItem> mPlayers = new ArrayList<RosterPlayersSelectionListAdapter.PlayerItem>();

    private RosterPlayersSelectionListAdapter mAdapter;

    private RosterPlayerSaveListener mListener;

    private ProgressBar mLoadingSpinner;

    private String mTeamKey;

    public interface RosterPlayerSaveListener
    {
        public void onSave();
    }

    public RosterPlayerSelectionFragment(RosterPlayerSaveListener aListener, int aType)
    {
        mListener = aListener;
        mFragmentType = aType;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        mTeamKey = getArguments().getString(BundleConstants.GET_ROSTER_TEAM_KEY, "");

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    if (resultCode == YahooSportsApiService.API_RESULT_OK)
                    {
                        buildList(resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT));
                    }
                }
            }
        };

        mContext = getActivity();

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_ROSTER);
        intent.putExtra(BundleConstants.GET_ROSTER_TEAM_KEY, mTeamKey);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.roster_player_selection_fragment, container, false);

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.rosterPlayerSelectionLoadingSpinner);
        mLoadingSpinner.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        RosterPlayersSelectionListAdapter.PlayerItem temp = (RosterPlayersSelectionListAdapter.PlayerItem)l.getItemAtPosition(position);
        l.setItemChecked(position, !temp.isSelected);
    }

    private void initContextualActionMode()
    {
        final ListView listView = getListView();
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener()
        {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked)
            {
                mPlayers.get(position).isSelected = checked;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item)
            {
                switch (item.getItemId())
                {
                    case R.id.saveActionItem:
                        mListener.onSave();
                        mode.finish(); // Action picked, so close the CAB
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu)
            {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.transaction_menu, menu);
                return true;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode)
            {
                for (RosterPlayersSelectionListAdapter.PlayerItem playerItem : mPlayers)
                {
                    playerItem.isSelected = false;
                }

                mAdapter.notifyDataSetChanged();
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu)
            {
                return false;
            }
        });
    }

    @Override
    public void onItemCheckBoxClicked(int position, boolean isChecked)
    {
        Log.v(TAG, "onItemCheckBoxClicked(): " + position + " - " + isChecked);
        getListView().setItemChecked(position, isChecked);
    }

    private void buildList(String aData)
    {
        Player player = null;

        try
        {
            XmlPullParserFactory pullParserFactory = XmlPullParserFactory.newInstance();
            pullParserFactory.setNamespaceAware(true);
            XmlPullParser xmlPullParser = pullParserFactory.newPullParser();
            xmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");


            int eventType = xmlPullParser.getEventType();
            PlayersXmlParser playersXmlParser = new PlayersXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(xmlPullParser.getName().equalsIgnoreCase("player"))
                    {
                        player = playersXmlParser.readPlayer(xmlPullParser);
                        final RosterPlayersSelectionListAdapter.PlayerItem playerItem = new RosterPlayersSelectionListAdapter.PlayerItem();
                        playerItem.playerKey = player.getPlayerKey();
                        playerItem.playerName = player.getFullName();
                        //playerItem.pointsTotal = player.getPlayerStatistics().getPlayerPoints().getTotal();
                        playerItem.isSelected = false;
                        mPlayers.add(playerItem);
                    }
                }
                eventType = xmlPullParser.next();
            }

            mAdapter = new RosterPlayersSelectionListAdapter(mContext, mPlayers, this);
            setListAdapter(mAdapter);

            if (mFragmentType == SELECTION_TYPE_WIRE_TRANSACTION)
            {
                initContextualActionMode();
            }
            else if (mFragmentType == SELECTION_TYPE_TRADE)
            {
                getListView().setOnItemClickListener(new AdapterView.OnItemClickListener()
                {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                    {
                        mPlayers.get(i).isSelected = !mPlayers.get(i).isSelected;
                        mAdapter.notifyDataSetChanged();
                    }
                });
            }

            mLoadingSpinner.setVisibility(View.GONE);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public ArrayList<RosterPlayersSelectionListAdapter.PlayerItem> getSelectedPlayers()
    {
        return mAdapter.getSelectedPlayers();
    }
}
