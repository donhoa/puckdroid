package com.thedon.PuckDroid.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.Transaction;
import com.thedon.PuckDroid.data.parsers.TransactionsXmlParser;
import com.thedon.PuckDroid.view.adapters.PendingTradesListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PendingTradesFragment extends BaseViewPagerFragment implements PendingTradesListAdapter.PendingTradeOperationListener, ConfirmationDialogFragment.ConfirmDialogListener
{
    private static final String TAG = PendingTradesFragment.class.getSimpleName();

    private ArrayList<Transaction> mTransactions = new ArrayList<Transaction>();

    private int PENDING_TRADES_LIST_TYPE;

    public static final int TYPE_MY_OFFERS = 0;
    public static final int TYPE_LEAGUE = 1;

    private ConfirmationDialogFragment mConfirmationDialog;

    private Transaction mCurrentTransaction;


    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        PENDING_TRADES_LIST_TYPE = getArguments().getInt(BundleConstants.PENDING_TRADES_FRAGMENT_TYPE, 0);

        getPendingTrades(false);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.pending_trades_fragment, container, false);

        setHasOptionsMenu(true);

        mList = (ListView)view.findViewById(R.id.pendingTradesList);

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.pendingTradesLoadingAnimation);

        return view;
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        if (aOriginalRequest.equals(BundleConstants.GET_LEAGUE_PENDING_TRADES) || aOriginalRequest.equals(BundleConstants.GET_TEAM_PENDING_TRADES))
        {
            buildList(aData);
        }
        else if (aOriginalRequest.equals(BundleConstants.DELETE_TRANSACTION))
        {
            getPendingTrades(true);
        }
    }

    @Override
    protected void buildList(String aData)
    {
        Log.v(TAG, "aData: " + aData);

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            TransactionsXmlParser transactionsXmlParser = new TransactionsXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("transaction"))
                    {
                        Transaction transaction = transactionsXmlParser.readTransaction(mXmlPullParser);

                        if (PENDING_TRADES_LIST_TYPE == TYPE_MY_OFFERS)
                        {
                            if (transaction.getState() == Transaction.TRANSACTION_PROPOSED_STATE)
                            {
                                mTransactions.add(transaction);
                            }
                        }
                        else if (PENDING_TRADES_LIST_TYPE == TYPE_LEAGUE)
                        {
                            if (transaction.getState() == Transaction.TRANSACTION_ACCEPTED_STATE)
                            {
                                mTransactions.add(transaction);
                            }
                        }
                    }
                }
                eventType = mXmlPullParser.next();
            }

            mAdapter = new PendingTradesListAdapter(mContext, mTransactions, this);
            mList.setAdapter(mAdapter);
            mLoadingSpinner.setVisibility(View.GONE);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    @Override
    public void onAcceptTrade(Transaction Transaction)
    {
    }

    @Override
    public void onCancelTrade(Transaction aTransaction)
    {
        String message = String.format(mContext.getResources().getString(R.string.cancel_trade_confirmation), aTransaction.getTradeeTeamName());

        mConfirmationDialog = new ConfirmationDialogFragment(mContext, this, getString(R.string.confirm_transaction), message, getString(R.string.ok_button), getString(R.string.cancel_button));
        mConfirmationDialog.show(getActivity().getSupportFragmentManager(), "CancelPendingTradeConfirmationDialog");
        mCurrentTransaction = aTransaction;
    }

    @Override
    public void onRejectTrade(Transaction aTransaction)
    {
        String message = String.format(mContext.getResources().getString(R.string.reject_trade_confirmation), aTransaction.getTraderTeamName());

        mConfirmationDialog = new ConfirmationDialogFragment(mContext, this, getString(R.string.confirm_transaction), message, getString(R.string.ok_button), getString(R.string.cancel_button));
        mConfirmationDialog.show(getActivity().getSupportFragmentManager(), "RejectPendingTradeConfirmationDialog");
        mCurrentTransaction = aTransaction;
    }

    @Override
    public void onCounterOffer(Transaction aTransaction)
    {
    }

    @Override
    public void onVoteAgainstTrade(Transaction aTransaction)
    {
    }

    @Override
    public void onPositiveButtonClick()
    {
        if (mLoadingSpinner != null)
        {
            mLoadingSpinner.setVisibility(View.VISIBLE);
        }

        deletePendingTrade();
    }

    private void getPendingTrades(boolean aIsRefresh)
    {
        Log.v(TAG, TAG + ".getPendingTrades(): " + aIsRefresh);

        if (aIsRefresh)
        {
            mTransactions.clear();
            mAdapter.notifyDataSetChanged();
        }

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        if (PENDING_TRADES_LIST_TYPE == TYPE_LEAGUE)
        {
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_PENDING_TRADES);
        }
        else
        {
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TEAM_PENDING_TRADES);
        }

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        mContext.startService(intent);
    }

    private void deletePendingTrade()
    {
        Log.v(TAG, TAG + ".deletePendingTrade()");

        Intent intent = new Intent(mContext, YahooSportsApiService.class);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.DELETE_TRANSACTION);
        intent.putExtra(BundleConstants.TRANSACTION_KEY, mCurrentTransaction.getTransactionKey());
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.DELETE.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        mContext.startService(intent);
    }
}
