package com.thedon.PuckDroid.view.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerTitleStrip;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.view.FragmentSwitchListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class OverviewFragment extends Fragment implements FragmentSwitchListener
{
    private ManagerSectionsPagerAdapter mPagerAdapter;
    private ViewPager mViewPager;

    private static boolean sIsHeadToHeadLeague = false;

    private String mCurrentUserTeamKey;

    private LeagueSettings mLeagueSettings;

    private Fragment mStandingsFragment;
    private Fragment mScoreboardFragment;
    private Fragment mMatchupFragment;
    private Fragment mRosterFragment;
    private Fragment mTransactionsFragment;

    private Activity mParentActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);

        mParentActivity = getActivity();

        View view = inflater.inflate(R.layout.overview, null);

        mLeagueSettings = ((PuckDroidApplication)mParentActivity.getApplication()).getLeagueSettings();

        PagerTitleStrip titleStrip = (PagerTitleStrip)view.findViewById(R.id.pager_title_strip);
        titleStrip.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12);

        if (mLeagueSettings != null)
        {
            sIsHeadToHeadLeague = (mLeagueSettings.getLeagueType() == LeagueSettings.LEAGUE_TYPE_HEAD_TO_HEAD);
        }

        Preferences preferences = Preferences.getInstance(mParentActivity);
        mCurrentUserTeamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

        initFragments(view);

        return view;
    }

    @Override
    public void onSwitchRequest(boolean aRequestFromStandingsFragment)
    {
        if (aRequestFromStandingsFragment)
        {
            mPagerAdapter.switchLeaguePageFragment(mScoreboardFragment);
        }
        else
        {
            mPagerAdapter.switchLeaguePageFragment(mStandingsFragment);
        }
    }

//    @Override
//    public void onBackPressed()
//    {
//        if (mPagerAdapter.isRosterFragment(mViewPager.getCurrentItem()))
//        {
//            if (!mRosterFragment.onBackPressed())
//            {
//                return;
//            }
//        }
//
//        super.onBackPressed();
//    }


    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        //Reset the options menu to let the child fragments handle the display of the menu items
        MenuItem item;

        for (int i = 0; i < menu.size(); i++)
        {
            item = menu.getItem(i);
            if (item.getItemId() != R.id.settingsOption)
            {
                item.setVisible(false);
            }
        }

        super.onPrepareOptionsMenu(menu);
    }

    private void initFragments(View aView)
    {
        mStandingsFragment = StandingsFragment.newInstance();
        mScoreboardFragment = ScoreboardFragment.newInstance();


        mRosterFragment = RosterFragment.newInstance(mCurrentUserTeamKey);

        mTransactionsFragment = TransactionsFragment.newInstance();

        List<Fragment> fragments = new ArrayList<Fragment>();

        if (sIsHeadToHeadLeague)
        {
            mMatchupFragment = MatchupFragment.newInstance();

            if (mLeagueSettings.isPlayoffWeek())
            {
                fragments.add(mScoreboardFragment);
            }
            else
            {
                fragments.add(mStandingsFragment);
            }
            fragments.add(mMatchupFragment);
        }
        else
        {
            fragments.add(mStandingsFragment);
        }

        fragments.add(mRosterFragment);
        fragments.add(mTransactionsFragment);

        mPagerAdapter  = new ManagerSectionsPagerAdapter(getActivity().getSupportFragmentManager(), mParentActivity, fragments);

        mViewPager = (ViewPager)aView.findViewById(R.id.viewPager);

        if (sIsHeadToHeadLeague)
        {
            mViewPager.setOffscreenPageLimit(4);
        }
        else
        {
            mViewPager.setOffscreenPageLimit(3);
        }

        mViewPager.setAdapter(mPagerAdapter);
    }


    public static class ManagerSectionsPagerAdapter extends FragmentStatePagerAdapter
    {
        List<Fragment> mFragmentsList = new ArrayList<Fragment>();
        private Context mContext;

        public ManagerSectionsPagerAdapter(FragmentManager fm, Context aContext, List<Fragment> aFragmentsList)
        {
            super(fm);
            mContext = aContext;

            mFragmentsList = aFragmentsList;
        }

        @Override
        public Fragment getItem(int i)
        {
            return mFragmentsList.get(i);
        }

        @Override
        public int getCount()
        {
            return mFragmentsList.size();
        }

        @Override
        public CharSequence getPageTitle(int position)
        {
            String[] titles;

            if (sIsHeadToHeadLeague)
            {
                titles = mContext.getResources().getStringArray(R.array.sections_titles_with_matchup);
            }
            else
            {
                titles = mContext.getResources().getStringArray(R.array.sections_titles_no_matchup);
            }

            return titles[position];
        }

        public void switchLeaguePageFragment(android.support.v4.app.Fragment aNewFragment)
        {
            mFragmentsList.set(0, aNewFragment);
            notifyDataSetChanged();
        }

        public boolean isRosterFragment(int aIndex)
        {
            return mFragmentsList.get(aIndex) instanceof RosterFragment;
        }

        @Override
        public int getItemPosition(Object object)
        {
            int index = mFragmentsList.indexOf(object);

            if (index == -1) //Fragment is no longer present
            {
                index = POSITION_NONE;
            }
            else
            {
                index = POSITION_UNCHANGED;
            }
            return index;
        }
    }
}
