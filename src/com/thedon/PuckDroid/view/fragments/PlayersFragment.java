package com.thedon.PuckDroid.view.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.commonsware.cwac.endless.EndlessAdapter;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.League;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.data.parsers.PlayersXmlParser;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.WireTransactionActivity;
import com.thedon.PuckDroid.view.adapters.PlayersListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PlayersFragment extends BaseViewPagerFragment implements PlayersSearchFilterDialogFragment.FilterPlayersSearchListener, PlayersListAdapter.PlayersListListener, ConfirmationDialogFragment.ConfirmDialogListener
{
    private static final String TAG = PlayersFragment.class.getSimpleName();

    public static final int MAX_PLAYERS_PER_PAGE = 20;

    private String mFilterString;

    private ArrayList<Player> mPlayers = new ArrayList<Player>();
    private EndlessPlayersListAdapter mEndlessAdapter;

    private MenuItem mSearchViewItem;
    private MenuItem mFilterMenuItem;

    private PlayersListAdapter.PlayersListListener mPlayersListListener;

    private boolean mCanAddPlayers;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, "PlayersFragment.onCreate()");

        mContext = getActivity();
        mPlayersListListener = this;

        mFilterString = "status=A;sort=OR";

        mLeague = ((PuckDroidApplication)getActivity().getApplication()).getLeague();
        mLeagueSettings = ((PuckDroidApplication)getActivity().getApplication()).getLeagueSettings();
        mCurrentUserTeamInfo = ((PuckDroidApplication)getActivity().getApplication()).getCurrentUserTeamInfo();

        mCanAddPlayers = (mLeagueSettings.getMaxAdds() == -1) || mCurrentUserTeamInfo.getNumberOfMoves() < mLeagueSettings.getMaxAdds();
        sendGetLeaguePlayersRequest();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.v(TAG, TAG + ".onCreateView()");
        View view = inflater.inflate(R.layout.players_fragment, container, false);

        setHasOptionsMenu(true);

        mList = (ListView)view.findViewById(R.id.playersList);
        mList.setEmptyView(view.findViewById(R.id.playersListEmptyView));
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                FragmentManager fm = getActivity().getSupportFragmentManager();
                PlayerProfileDialogFragment dialogFragment = new PlayerProfileDialogFragment(mContext, mPlayers.get(i).getPlayerKey(), null, mCanAddPlayers);
                dialogFragment.show(fm, "PlayerProfileDialogFragment");
            }
        });

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.playersLoadingAnimation);
        mLoadingSpinner.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        //Reset the options menu and let this fragment handle the display of the menu items
        MenuItem item;

        for (int i = 0; i < menu.size(); i++)
        {
            item = menu.getItem(i);
            if (item.getItemId() != R.id.settingsOption)
            {
                item.setVisible(false);
            }
        }

        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        mSearchViewItem = menu.findItem(R.id.searchOption);
        mSearchViewItem.setVisible(true);
        mSearchViewItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener()
        {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem)
            {
                mFilterMenuItem.setVisible(false);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem)
            {
                mFilterMenuItem.setVisible(true);
                return true;
            }
        });

        SearchView searchView = (SearchView)mSearchViewItem.getActionView();
        searchView.setQueryHint(mContext.getString(R.string.searchPlayerQueryHint));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener()
        {
            @Override
            public boolean onQueryTextSubmit(String s)
            {
                try
                {
                    Log.v(TAG, "onQueryTextSubmit(): " + s);
                    mEndlessAdapter.clearData();
                    mFilterString = ";search=" + URLEncoder.encode(s, "UTF-8");
                    sendGetLeaguePlayersRequest();
                }
                catch (UnsupportedEncodingException e)
                {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                return true;
            }

            @Override
            public boolean onQueryTextChange(String s)
            {
                return false;
            }
        });

        mFilterMenuItem = menu.findItem(R.id.filterOption);
        mFilterMenuItem.setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.v(TAG, "onOptionsItemSelected()");

        switch (item.getItemId())
        {
            case R.id.filterOption:
                FragmentManager fm = getActivity().getSupportFragmentManager();
                PlayersSearchFilterDialogFragment searchFilterDialogFragment = new PlayersSearchFilterDialogFragment(mContext, this, mLeagueSettings);
                searchFilterDialogFragment.show(fm, "PlayersSearchFilterDialogFragment");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFilter(String aFilterString)
    {
        Log.v(TAG, TAG + ".onFilter(): " + aFilterString);

        mFilterString = aFilterString;

        mEndlessAdapter.clearData();

        sendGetLeaguePlayersRequest();
    }

    @Override
    public void onAddPlayerClick(Player aPlayer)
    {
        Log.v(TAG, TAG + ".onAddPlayerClick()");

        int totalRosterPlayersCount;

        if (mLeague.getWeeklyDeadlineType() == League.WEEKLY_DEADLINE_TYPE_TOMORROW)
        {
            totalRosterPlayersCount = ((PuckDroidApplication)getActivity().getApplication()).getTomorrowsRosterPlayersCount();
        }
        else
        {
            totalRosterPlayersCount = ((PuckDroidApplication)getActivity().getApplication()).getRosterPlayersCount();
        }

        if (totalRosterPlayersCount < mLeagueSettings.getRosterTotalPlayersCount())
        {
            FragmentManager fm = getActivity().getSupportFragmentManager();
            PickAddTypeDialogFragment dialogFragment = new PickAddTypeDialogFragment(mContext, mCurrentUserTeamInfo.getTeamKey(), aPlayer, mResultReceiver, this);
            dialogFragment.show(fm, "PickAddTypeDialogFragment");
        }
        else
        {
            Intent intent = new Intent(mContext, WireTransactionActivity.class);
            intent.putExtra(BundleConstants.PLAYER_KEY, aPlayer.getPlayerKey());
            intent.putExtra(BundleConstants.PLAYER_NAME, aPlayer.getFullName());
            intent.putExtra(BundleConstants.PLAYER_ON_WAIVERS, aPlayer.getPlayerOwnership().isOnWaivers());
            mContext.startActivity(intent);
        }
    }

    @Override
    public void onPositiveButtonClick()
    {
        mFilterString = "status=A;sort=OR";
        mEndlessAdapter.clearData();
        sendGetLeaguePlayersRequest();
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        if (aOriginalRequest.equals(BundleConstants.GET_LEAGUE_PLAYERS))
        {
            buildList(aData);
        }
        else if (aOriginalRequest.equals(BundleConstants.POST_WIRE_TRANSACTION))
        {
            Toast.makeText(mContext, mContext.getString(R.string.transaction_success), Toast.LENGTH_SHORT);
        }
    }

    protected void buildList(String aData)
    {
        Log.v(TAG, TAG + ".buildList()");

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            PlayersXmlParser teamXmlParser = new PlayersXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("player"))
                    {
                        mPlayers.add(teamXmlParser.readPlayer(mXmlPullParser));
                    }
                }
                eventType = mXmlPullParser.next();
            }

            mAdapter = new PlayersListAdapter(mContext, mPlayers, mPlayersListListener, mCanAddPlayers);

            if (mPlayers.size() < MAX_PLAYERS_PER_PAGE)
            {
                mList.setAdapter(mAdapter);
            }
            else
            {
                mEndlessAdapter = new EndlessPlayersListAdapter(mAdapter);
                mEndlessAdapter.setRunInBackground(false);
                mList.setAdapter(mEndlessAdapter);
            }

            mLoadingSpinner.setVisibility(View.GONE);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void sendGetLeaguePlayersRequest()
    {
        Log.v(TAG, "sendGetLeaguePlayersRequest");
        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_PLAYERS);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.GET_LEAGUE_PLAYERS_FILTER, mFilterString);
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);
    }

    private ResultReceiver mPlayerReceiver = new ResultReceiver(new Handler())
    {
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData)
        {
            Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

            if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
            {
                if (resultCode == YahooSportsApiService.API_RESULT_OK)
                {
                    String data = resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT);
                    new FetchNextPlayersTask(mEndlessAdapter).execute(data);
                }
            }
        }
    };

    class EndlessPlayersListAdapter extends EndlessAdapter implements IItemsReadyListener
    {
        private boolean mHasMoreData = true;
        private int pageCount;

        EndlessPlayersListAdapter(ListAdapter aWrappedAdapter)
        {
            super(aWrappedAdapter);
            pageCount = 0;
        }

        @Override
        protected View getPendingView(ViewGroup parent)
        {
            Log.v(TAG, "EndlessPlayersListAdapter.getPendingView()");
            View pendingRow = getActivity().getLayoutInflater().inflate(R.layout.loading_animation, null);

            pageCount ++;

            return(pendingRow);
        }

        @Override
        protected boolean cacheInBackground() throws Exception
        {
            Log.v(TAG, "EndlessPlayersListAdapter.cacheInBackground(): " + pageCount);
            Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_PLAYERS);
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
            intent.putExtra(BundleConstants.GET_LEAGUE_PLAYERS_PAGE_COUNT, pageCount);
            intent.putExtra(BundleConstants.GET_LEAGUE_PLAYERS_FILTER, mFilterString);
            intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mPlayerReceiver);
            getActivity().startService(intent);

            Log.v(TAG, "cacheInBackground hasMoreData: " + mHasMoreData);
            return mHasMoreData;
        }

        @Override
        public void onItemsReady(ArrayList<Player> data)
        {
            Log.v(TAG, "EndlessPlayersListAdapter.onItemsReady(): " + data.size());
            mPlayers.addAll(data);
            mEndlessAdapter.onDataReady();
            mHasMoreData = !data.isEmpty();
        }

        @Override
        protected void appendCachedData()
        {
            Log.v(TAG, "EndlessPlayersListAdapter.appendCachedData()" );
        }

        public void clearData()
        {
            mPlayers.clear();
            mEndlessAdapter.notifyDataSetChanged();
            pageCount = 0;
        }
    }

    class FetchNextPlayersTask extends AsyncTask<String, Void, ArrayList<Player>>
    {
        IItemsReadyListener mListener;

        protected FetchNextPlayersTask(IItemsReadyListener listener)
        {
            this.mListener = listener;
        }

        @Override
        protected ArrayList<Player> doInBackground(String... params)
        {
            Log.v(TAG, "FetchNextPlayersTask.doInBackground()");

            ArrayList<Player> result = new ArrayList<Player>();

            try
            {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser parser = factory.newPullParser();

                parser.setInput(new ByteArrayInputStream(params[0].getBytes()), "UTF_8");
                int eventType = parser.getEventType();
                PlayersXmlParser teamXmlParser = new PlayersXmlParser();

                while(eventType != XmlPullParser.END_DOCUMENT)
                {
                    if(eventType == XmlPullParser.START_TAG)
                    {
                        if(parser.getName().equalsIgnoreCase("player"))
                        {
                            result.add(teamXmlParser.readPlayer(parser));
                        }
                    }
                    eventType = parser.next();
                }

            }
            catch (XmlPullParserException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            catch (IOException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            return(result);
        }

        @Override
        protected void onPostExecute(ArrayList<Player> result)
        {
            mListener.onItemsReady(result);
        }
    }

    interface IItemsReadyListener
    {
        public void onItemsReady(ArrayList<Player> data);
    }
}
