package com.thedon.PuckDroid.view.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.League;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.data.YahooApiError;
import com.thedon.PuckDroid.data.parsers.YahooApiErrorXmlParser;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public abstract class BaseViewPagerFragment extends Fragment
{
    private static final String TAG = BaseViewPagerFragment.class.getSimpleName();

    protected ListView mList;
    protected ProgressBar mLoadingSpinner;
    protected Context mContext;

    protected XmlPullParserFactory mPullParserFactory;
    protected XmlPullParser mXmlPullParser;
    protected BaseAdapter mAdapter;

    protected MenuItem mRefreshMenuItem;

    protected ResultReceiver mResultReceiver;

    protected ActionBar mActionBar;

    protected League mLeague;
    protected LeagueSettings mLeagueSettings;
    protected Team mCurrentUserTeamInfo;
    protected Preferences mPreferences;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        try
        {
            mPullParserFactory = XmlPullParserFactory.newInstance();
            mPullParserFactory.setNamespaceAware(true);
            mXmlPullParser = mPullParserFactory.newPullParser();

            mContext = getActivity();

            mActionBar = getActivity().getActionBar();

            mResultReceiver = new ResultReceiver(new Handler())
            {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData)
                {
                    Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                    if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                    {
                        String originalRequest = resultData.getString(BundleConstants.YAHOO_SPORTS_API_REQUEST);
                        String data = resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT);

                        Log.v(TAG, "ResultReceiver originalRequest: " + originalRequest);

                        if (resultCode == YahooSportsApiService.API_RESULT_OK)
                        {
                            handleResponse(originalRequest, data);
                        }
                        else if (resultCode == YahooSportsApiService.API_RESULT_ERROR)
                        {
                            handleError(data);
                        }
                    }
                }
            };
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void handleError(String aData)
    {
        YahooApiError error = null;
        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            YahooApiErrorXmlParser errorXmlParser = new YahooApiErrorXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("error"))
                    {
                        error = errorXmlParser.readError(mXmlPullParser);
                    }
                }
                eventType = mXmlPullParser.next();
            }

            if (error != null)
            {
                Toast.makeText(mContext, error.getDescription(), Toast.LENGTH_LONG).show();
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    protected abstract void handleResponse(String aOriginalRequest, String aData);
    protected abstract void buildList(String aData);
}
