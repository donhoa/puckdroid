package com.thedon.PuckDroid.view.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.Team;
import com.thedon.PuckDroid.data.parsers.PoolTeamXmlParser;
import com.thedon.PuckDroid.view.FragmentSwitchListener;
import com.thedon.PuckDroid.view.TeamProfileActivity;
import com.thedon.PuckDroid.view.adapters.TeamsListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class StandingsFragment extends BaseViewPagerFragment
{
    private static final String TAG = StandingsFragment.class.getSimpleName();

    private ArrayList<Team> mTeams = new ArrayList<Team>();

    private boolean mIsHeadToHeadLeague;

    private FragmentSwitchListener mSwitchListener;

    public static Fragment newInstance()
    {
        return new StandingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        getStandings();

        mLeagueSettings = ((PuckDroidApplication)getActivity().getApplication()).getLeagueSettings();

        mIsHeadToHeadLeague = mLeagueSettings.getLeagueType() == LeagueSettings.LEAGUE_TYPE_HEAD_TO_HEAD;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.v(TAG, TAG + ".onCreateView()");

        View view = inflater.inflate(R.layout.standings_fragment, container, false);

        mList = (ListView)view.findViewById(R.id.standingsList);
        mList.setEmptyView(view.findViewById(R.id.playersListEmptyView));
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Team selectedTeam = mTeams.get(i);

                Intent intent = new Intent(mContext, TeamProfileActivity.class);
                intent.putExtra(BundleConstants.IS_CURRENT_USER, selectedTeam.isCurrentUser());
                intent.putExtra(BundleConstants.GET_TEAM_INFO_KEY, selectedTeam.getTeamKey());
                intent.putExtra(BundleConstants.GET_TEAM_INFO_NAME, selectedTeam.getName());
                startActivity(intent);
            }
        });

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.standingsLoadingAnimation);
        mLoadingSpinner.setVisibility(View.VISIBLE);

        setHasOptionsMenu(true);

        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            mSwitchListener = (FragmentSwitchListener)activity;
        }
        catch (ClassCastException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        if (mIsHeadToHeadLeague)
        {
            MenuItem viewScoreboard = menu.findItem(R.id.viewScoreboard);
            viewScoreboard.setVisible(true);
        }

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.v(TAG, "onOptionsItemSelected()");

        if (item.getItemId() == R.id.viewScoreboard)
        {
            mSwitchListener.onSwitchRequest(true);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        buildList(aData);
    }

    protected void buildList(String aData)
    {
        Log.v(TAG, TAG + ".buildList()");

        Team team = null;

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            PoolTeamXmlParser teamXmlParser = new PoolTeamXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("team"))
                    {
                        team = teamXmlParser.readTeam(mXmlPullParser);
                        mTeams.add(team);
                    }
                }
                eventType = mXmlPullParser.next();
            }
            mAdapter = new TeamsListAdapter(mContext, mTeams);
            mList.setAdapter(mAdapter);
            mLoadingSpinner.setVisibility(View.GONE);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void getStandings()
    {
        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_STANDINGS);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);

        if (mTeams.size() > 0)
        {
            mTeams.clear();
            mAdapter.notifyDataSetChanged();
        }

    }
}
