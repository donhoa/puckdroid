package com.thedon.PuckDroid.view.fragments;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.*;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.commonsware.cwac.endless.EndlessAdapter;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.Transaction;
import com.thedon.PuckDroid.data.parsers.TransactionsXmlParser;
import com.thedon.PuckDroid.view.adapters.TransactionsListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class TransactionsFragment extends BaseViewPagerFragment
{
    private static final String TAG = TransactionsFragment.class.getSimpleName();

    private ArrayList<Transaction> mTransactions = new ArrayList<Transaction>();
    private EndlessTransactionsListAdapter mEndlessAdapter;

    public static Fragment newInstance()
    {
        return new TransactionsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        mContext = getActivity();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.transactions_fragment, container, false);

        setHasOptionsMenu(true);

        mList = (ListView)view.findViewById(R.id.transactionsList);

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.transactionsLoadingAnimation);

        loadTransactions(false);

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);

        mRefreshMenuItem = menu.findItem(R.id.refreshOption);
        mRefreshMenuItem.setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.v(TAG, "onOptionsItemSelected()");

        if (item.getItemId() == R.id.refreshOption)
        {
            mRefreshMenuItem.setActionView(R.layout.loading_animation);
            loadTransactions(true);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        buildList(aData);
    }

    protected void buildList(String aData)
    {
        Log.v(TAG, TAG + ".buildList()");

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            TransactionsXmlParser transactionsXmlParser = new TransactionsXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("transaction"))
                    {
                        mTransactions.add(transactionsXmlParser.readTransaction(mXmlPullParser));
                    }
                }
                eventType = mXmlPullParser.next();
            }

            mEndlessAdapter = new EndlessTransactionsListAdapter(mContext, mTransactions) ;
            mEndlessAdapter.setRunInBackground(false);
            mList.setAdapter(mEndlessAdapter);
            mLoadingSpinner.setVisibility(View.GONE);

            if (mRefreshMenuItem != null)
            {
                mRefreshMenuItem.setActionView(null);
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void loadTransactions(boolean aIsRefresh)
    {
        if (aIsRefresh)
        {
            mEndlessAdapter.clearData();
        }
        else
        {
            if (mLoadingSpinner != null)
            {
                mLoadingSpinner.setVisibility(View.VISIBLE);
            }
        }

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_TRANSACTIONS);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);
    }

    private ResultReceiver mTransactionReceiver = new ResultReceiver(new Handler())
    {
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData)
        {
            Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

            if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
            {
                if (resultCode == YahooSportsApiService.API_RESULT_OK)
                {
                    String data = resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT);
                    new FetchNextTransactionTask(mEndlessAdapter).execute(data);
                }
            }
        }
    };

    class EndlessTransactionsListAdapter extends EndlessAdapter implements IItemsReadyListener
    {
        private boolean mHasMoreData = true;
        private int pageCount = 0;

        EndlessTransactionsListAdapter(Context context, ArrayList<Transaction> list)
        {
            super(new TransactionsListAdapter(context, list));
        }

        @Override
        protected View getPendingView(ViewGroup parent)
        {
            Log.v(TAG, "EndlessTransactionsListAdapter.getPendingView()");
            View pendingRow = getActivity().getLayoutInflater().inflate(R.layout.loading_animation, null);

            pageCount ++;

            return(pendingRow);
        }

        @Override
        protected boolean cacheInBackground() throws Exception
        {
            Log.v(TAG, "EndlessTransactionsListAdapter.cacheInBackground(): " + pageCount);
            Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_TRANSACTIONS);
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
            intent.putExtra(BundleConstants.GET_LEAGUE_TRANSACTIONS_PAGE_COUNT, pageCount);
            intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mTransactionReceiver);
            getActivity().startService(intent);

            Log.v(TAG, "cacheInBackground hasMoreData: " + mHasMoreData);
            return mHasMoreData;
        }

        @Override
        public void onItemsReady(ArrayList<Transaction> data)
        {
            Log.v(TAG, "EndlessTransactionsListAdapter.onItemsReady(): " + data.size());
            mTransactions.addAll(data);
            mEndlessAdapter.onDataReady();
            mHasMoreData = !data.isEmpty();
        }

        @Override
        protected void appendCachedData()
        {
            Log.v(TAG, "EndlessTransactionsListAdapter.appendCachedData()" );
        }

        public void clearData()
        {
            mTransactions.clear();
            mEndlessAdapter.notifyDataSetChanged();
            pageCount = 0;
        }
    }

    class FetchNextTransactionTask extends AsyncTask<String, Void, ArrayList<Transaction>>
    {
        IItemsReadyListener mListener;

        protected FetchNextTransactionTask(IItemsReadyListener listener)
        {
            this.mListener = listener;
        }

        @Override
        protected ArrayList<Transaction> doInBackground(String... params)
        {
            Log.v(TAG, "FetchNextTransactionTask.doInBackground()");

            ArrayList<Transaction> result = new ArrayList<Transaction>();

            try
            {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser parser = factory.newPullParser();

                parser.setInput(new ByteArrayInputStream(params[0].getBytes()), "UTF_8");
                int eventType = parser.getEventType();
                TransactionsXmlParser transactionsXmlParser = new TransactionsXmlParser();

                while(eventType != XmlPullParser.END_DOCUMENT)
                {
                    if(eventType == XmlPullParser.START_TAG)
                    {
                        if(parser.getName().equalsIgnoreCase("transaction"))
                        {
                            result.add(transactionsXmlParser.readTransaction(parser));
                        }
                    }
                    eventType = parser.next();
                }

            }
            catch (XmlPullParserException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            catch (IOException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            return(result);
        }

        @Override
        protected void onPostExecute(ArrayList<Transaction> result)
        {
            mListener.onItemsReady(result);
        }
    }

    interface IItemsReadyListener
    {
        public void onItemsReady(ArrayList<Transaction> data);
    }
}
