package com.thedon.PuckDroid.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.Game;
import com.thedon.PuckDroid.utils.Utils;

import java.util.Hashtable;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class WeekPickerDialogFragment extends DialogFragment
{
    private Context mContext;
    private int mCurrentWeek;
    private int mStartWeek;
    private int mEndWeek;
    private int mPlayoffStartWeek;
    private TextView mWeekDates;
    private NumberPicker mWeekPicker;

    private Button mNegativeButton;
    private Button mPositiveButton;

    private Hashtable<String, Game.GameWeek> mGameWeeks;

    private WeekPickerDialogFragmentListener mListener;

    public interface WeekPickerDialogFragmentListener
    {
        public void onWeekPicked(int aWeek);
    }

    public WeekPickerDialogFragment(Context aContext, int aCurrentWeek, int aStartWeek, int aEndWeek, int aPlayoffStartWeek, Hashtable<String, Game.GameWeek> aGameWeeks, WeekPickerDialogFragmentListener aListener)
    {
        mContext = aContext;

        mCurrentWeek = aCurrentWeek;
        mStartWeek = aStartWeek;
        mEndWeek = aEndWeek;
        mPlayoffStartWeek = aPlayoffStartWeek;
        mGameWeeks = aGameWeeks;

        mListener = aListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.week_picker_dialog_fragment, container);

        getDialog().setTitle(mContext.getString(R.string.select_week_title));

        mWeekDates = (TextView)view.findViewById(R.id.weekDates);

        mWeekPicker = (NumberPicker)view.findViewById(R.id.weekNumberPicker);
        mWeekPicker.setMinValue(mStartWeek);
        mWeekPicker.setMaxValue(mEndWeek);
        mWeekPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener()
        {
            @Override
            public void onValueChange(NumberPicker numberPicker, int originalValue, int newValue)
            {
                Game.GameWeek gameWeek = mGameWeeks.get(String.valueOf(newValue));

                if (gameWeek != null)
                {
                    mWeekDates.setText(gameWeek.getStartSimple() + " - " + gameWeek.getEndSimple());
                }
            }
        });

        mWeekPicker.setValue(mCurrentWeek);

        mNegativeButton = (Button)view.findViewById(R.id.weekPickerNegativeButton);
        mPositiveButton = (Button)view.findViewById(R.id.weekPickerPositiveButton);

        mNegativeButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                dismiss();
            }
        });

        mPositiveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                mListener.onWeekPicked(mWeekPicker.getValue());
                dismiss();
            }
        });

        return view;
    }
}
