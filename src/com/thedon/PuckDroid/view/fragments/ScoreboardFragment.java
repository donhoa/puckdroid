package com.thedon.PuckDroid.view.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.*;
import android.widget.ListView;
import android.widget.ProgressBar;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.*;
import com.thedon.PuckDroid.data.parsers.GameXmlParser;
import com.thedon.PuckDroid.data.parsers.MatchupXmlParser;
import com.thedon.PuckDroid.view.*;
import com.thedon.PuckDroid.view.adapters.ScoreboardListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class ScoreboardFragment extends BaseViewPagerFragment implements WeekPickerDialogFragment.WeekPickerDialogFragmentListener
{
    private static final String TAG = ScoreboardFragment.class.getSimpleName();

    ArrayList<CustomListItem> mScoreboardListItems = new ArrayList<CustomListItem>();
    private String mSelectedWeek;

    private Game mGame;

    private boolean mIsHeadToHeadLeague;

    private FragmentSwitchListener mSwitchListener;

    public static Fragment newInstance()
    {
        return new ScoreboardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        Preferences preferences = Preferences.getInstance(mContext);
        mSelectedWeek = preferences.getString(Preferences.KEY_CURRENT_WEEK, "");

        getScoreboard(false);

        mLeagueSettings = ((PuckDroidApplication)getActivity().getApplication()).getLeagueSettings();

        mIsHeadToHeadLeague = mLeagueSettings.getLeagueType() == LeagueSettings.LEAGUE_TYPE_HEAD_TO_HEAD;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.v(TAG, TAG + ".onCreateView()");

        View view = inflater.inflate(R.layout.scoreboard_fragment, container, false);

        setHasOptionsMenu(true);

        mList = (ListView)view.findViewById(R.id.scoreboardList);
        mList.setEmptyView(view.findViewById(R.id.scoreboardEmptyView));

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.scoreboardLoadingAnimation);
        mLoadingSpinner.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            mSwitchListener = (FragmentSwitchListener)activity;
        }
        catch (ClassCastException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        if (!(mContext instanceof ManagerActivity))
        {
            return;
        }

        if (mIsHeadToHeadLeague)
        {
            MenuItem viewStandings = menu.findItem(R.id.viewStandings);
            viewStandings.setVisible(true);
        }

        MenuItem pickWeek = menu.findItem(R.id.pickWeekOption);
        pickWeek.setVisible(true);

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.v(TAG, "onOptionsItemSelected()");

        if (item.getItemId() == R.id.pickWeekOption)
        {
            League league = ((PuckDroidApplication)getActivity().getApplication()).getLeague();
            FragmentManager fm = getActivity().getSupportFragmentManager();
            WeekPickerDialogFragment dialogFragment = new WeekPickerDialogFragment(mContext, Integer.parseInt(mSelectedWeek), league.getStartWeek(), league.getEndWeek(), mLeagueSettings.getPlayoffStartWeek(), mGame.getGameWeeks(), this);
            dialogFragment.show(fm, "WeekPickerDialogFragment");
        }
        else if (item.getItemId() == R.id.viewStandings)
        {
            mSwitchListener.onSwitchRequest(false);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onWeekPicked(int aWeek)
    {
        mSelectedWeek = String.valueOf(aWeek);

        getScoreboard(true);
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        if (aOriginalRequest.equals(BundleConstants.GET_LEAGUE_SCOREBOARD))
        {
            buildList(aData);
        }
        else if (aOriginalRequest.equals(BundleConstants.GET_GAME_WEEKS))
        {
            getMatchupWeekDates(aData);
        }
    }

    protected void buildList(String aData)
    {
        Log.v(TAG, TAG + ".buildList()");

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            MatchupXmlParser matchupXmlParser = new MatchupXmlParser();

            int matchupWeek = -1;

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("matchup"))
                    {
                        Matchup matchup = matchupXmlParser.readMatchup(mXmlPullParser);

                        ArrayList<Team> teams = matchup.getTeams();
                        Team team1 = teams.get(0);
                        Team team2 = teams.get(1);

                        ScoreboardListItem item = new ScoreboardListItem();
                        item.setIsConsolation(matchup.isConsolation());
                        item.setTeamName1(team1.getName());
                        item.setTeamName2(team2.getName());
                        item.setTeamScore1(team1.getPoints().getTotal());
                        item.setTeamScore2(team2.getPoints().getTotal());

                        matchupWeek = matchup.getWeek();

                        mScoreboardListItems.add(item);
                    }
                }
                eventType = mXmlPullParser.next();
            }

            if (matchupWeek >= mLeagueSettings.getPlayoffStartWeek())
            {
                mAdapter = new ScoreboardListAdapter(mContext, buildListWithHeaders(mScoreboardListItems));
            }
            else
            {
                mAdapter = new ScoreboardListAdapter(mContext, mScoreboardListItems);
            }

            mList.setAdapter(mAdapter);
            mLoadingSpinner.setVisibility(View.GONE);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    private ArrayList<CustomListItem> buildListWithHeaders(ArrayList<CustomListItem> aList)
    {
        ArrayList<CustomListItem> list = new ArrayList<CustomListItem>();

        HeaderListItem championshipHeader = new HeaderListItem();
        championshipHeader.setHeaderTitle(mContext.getString(R.string.championship_header_title));
        list.add(championshipHeader);

        for (CustomListItem item : aList)
        {
            if (!((ScoreboardListItem)item).isConsolation())
            {
                list.add(item);
            }
        }

        HeaderListItem consolationHeader = new HeaderListItem();
        consolationHeader.setHeaderTitle(mContext.getString(R.string.consolation_header_title));
        list.add(consolationHeader);

        for (CustomListItem item : aList)
        {
            if (((ScoreboardListItem)item).isConsolation())
            {
                list.add(item);
            }
        }

        return list;
    }

    private void getScoreboard(boolean aRefresh)
    {
        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUE_SCOREBOARD);
        intent.putExtra(BundleConstants.SCOREBOARD_WEEK, mSelectedWeek);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);

        if (!aRefresh)
        {
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_GAME_WEEKS);
            getActivity().startService(intent);
        }

        if (mScoreboardListItems.size() > 0)
        {
            mScoreboardListItems.clear();
            mAdapter.notifyDataSetChanged();
        }
    }

    private void getMatchupWeekDates(String aData)
    {
        Log.v(TAG, TAG + ".getMatchupWeekDates()");

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            GameXmlParser gameXmlParser = new GameXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("game"))
                    {
                        mGame = gameXmlParser.readGame(mXmlPullParser);
                    }
                }
                eventType = mXmlPullParser.next();
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
}
