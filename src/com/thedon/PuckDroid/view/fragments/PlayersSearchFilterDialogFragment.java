package com.thedon.PuckDroid.view.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.adapters.SortBySpinnerAdapter;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PlayersSearchFilterDialogFragment extends DialogFragment
{

    private static final String TAG = PlayersSearchFilterDialogFragment.class.getSimpleName();

    private Spinner mPositionsSpinner;
    private Spinner mStatusSpinner;
    private Spinner mStatsSpinner;
    private Spinner mSortBySpinner;

    private Context mContext;

    private LeagueSettings mLeagueSettings;

    private FilterPlayersSearchListener mListener;

    private ArrayList<String> mPlayerSortValues;

    private SortBySpinnerAdapter mSortBySpinnerAdapter;

    public interface FilterPlayersSearchListener
    {
        public void onFilter(String aFilterString);
    }

    public PlayersSearchFilterDialogFragment(Context context, FilterPlayersSearchListener aListener, LeagueSettings aLeagueSettings)
    {
        mContext = context;
        mListener = aListener;

        mLeagueSettings = aLeagueSettings;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.filter_players_search, container);

        //Positions
        String[] positions = getResources().getStringArray(R.array.players_position_filter_names);
        mPositionsSpinner = (Spinner)view.findViewById(R.id.spinnerFilterPosition);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, positions);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mPositionsSpinner.setAdapter(dataAdapter);
        mPositionsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l)
            {
                if (i != 5)
                {
                    mPlayerSortValues = Utils.getPlayersSortSpinnerValues(mContext, mLeagueSettings, true, true);
                    mSortBySpinnerAdapter.setValues(Utils.getPlayersSortSpinnerValues(mContext, mLeagueSettings, true, false));
                    mSortBySpinnerAdapter.notifyDataSetChanged();
                }
                else
                {
                    mPlayerSortValues = Utils.getPlayersSortSpinnerValues(mContext, mLeagueSettings, false, true);
                    mSortBySpinnerAdapter.setValues(Utils.getPlayersSortSpinnerValues(mContext, mLeagueSettings, false, false));
                    mSortBySpinnerAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView)
            {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        //Status
        String[] status = getResources().getStringArray(R.array.players_status_filter_names);

        mStatusSpinner = (Spinner)view.findViewById(R.id.spinnerFilterStatus);
        dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, status);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


        mStatusSpinner.setAdapter(dataAdapter);

        //Stats
        String[] stats = getResources().getStringArray(R.array.players_stats_filter_names);

        mStatsSpinner= (Spinner)view.findViewById(R.id.spinnerFilterStats);
        dataAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, stats);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mStatsSpinner.setAdapter(dataAdapter);

        //Sort By
        ArrayList<String> sortBy = Utils.getPlayersSortSpinnerValues(mContext, mLeagueSettings, true, false);
        mPlayerSortValues = Utils.getPlayersSortSpinnerValues(mContext, mLeagueSettings, true, true);

        mSortBySpinner = (Spinner)view.findViewById(R.id.sortBySpinner);
        mSortBySpinnerAdapter = new SortBySpinnerAdapter(mContext, sortBy);

        mSortBySpinner.setAdapter(mSortBySpinnerAdapter);

        Button filterButton = (Button)view.findViewById(R.id.searchPlayersFilterButton);
        filterButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                buildFilterString();
            }
        });

        return view;
    }

    private void buildFilterString()
    {
        Log.v(TAG, "buildFilterString()");

        String filter = "";
        String[] positionsParams = getResources().getStringArray(R.array.players_position_filter_params);
        String[] statusParams = getResources().getStringArray(R.array.players_status_filter_params);
        String[] statsParams = getResources().getStringArray(R.array.players_stats_filter_params);

        String positionFilter = "";
        if (mPositionsSpinner.getSelectedItemPosition() > 0) // 0 = all
        {
            positionFilter = "position=" + positionsParams[mPositionsSpinner.getSelectedItemPosition()];
        }
        String statusFilter = "status=" + statusParams[mStatusSpinner.getSelectedItemPosition()];
        String statsFilter = "sort_type=" + statsParams[mStatsSpinner.getSelectedItemPosition()];
        String sortFilter = "sort=" + mPlayerSortValues.get(mSortBySpinner.getSelectedItemPosition());
        filter = positionFilter + ";" + statusFilter + ";" + statsFilter + ";" + sortFilter;

        Log.v(TAG, "filter: " + filter);

        mListener.onFilter(filter);

        getDialog().dismiss();
    }
}
