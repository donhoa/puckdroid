package com.thedon.PuckDroid.view.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class ConfirmationDialogFragment extends DialogFragment
{
    private Context mContext;
    private String mTitle;
    private String mMessage;
    private String mPositiveButton;
    private String mNegativeButton;
    private ConfirmDialogListener mListener;


    public interface ConfirmDialogListener
    {
        public void onPositiveButtonClick();
    }

    public ConfirmationDialogFragment(Context aContext, ConfirmDialogListener aListener, String aTitle, String aMessage, String aPositiveButton, String aNegativeButton)
    {
        mContext = aContext;
        mListener = aListener;
        mTitle = aTitle;
        mMessage = aMessage;
        mPositiveButton = aPositiveButton;
        mNegativeButton = aNegativeButton;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(mTitle);
        alertDialogBuilder.setMessage(mMessage);
        alertDialogBuilder.setPositiveButton(mPositiveButton, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                mListener.onPositiveButtonClick();
            }
        });
        alertDialogBuilder.setNegativeButton(mNegativeButton, new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        return alertDialogBuilder.create();
    }

    public void setMessage(String aMessage)
    {
        mMessage = aMessage;
    }
}
