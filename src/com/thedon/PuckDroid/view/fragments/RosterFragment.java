package com.thedon.PuckDroid.view.fragments;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.*;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.*;
import com.thedon.PuckDroid.data.*;
import com.thedon.PuckDroid.data.parsers.PlayersXmlParser;
import com.thedon.PuckDroid.data.parsers.PoolTeamXmlParser;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.CustomListItem;
import com.thedon.PuckDroid.view.HeaderListItem;
import com.thedon.PuckDroid.view.ManagerActivity;
import com.thedon.PuckDroid.view.adapters.RosterListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class RosterFragment extends BaseViewPagerFragment implements PlayerProfileDialogFragment.PlayerProfileDialogListener
{
    private static final String TAG = RosterFragment.class.getSimpleName();

    private View mEditRosterButtonsRow;
    private Button mSaveButton;
    private Button mCancelButton;
    private TextView mStartingLineupTotal;

    private MenuItem mEditMenuItem;
    private String mTeamKey;
    private Date[] mRosterDates;
    private String mCurrentRosterDate;
    private boolean mAttachedToManagerActivity;
    private boolean mEditMode;
    private int mLeagueWeeklyDeadlineType;

    private Roster mTomorrowsRoster;

    private PlayerProfileDialogFragment.PlayerProfileDialogListener mPlayerProfileDialogListener;

    public static Fragment newInstance(String aCurrentUserTeamKey)
    {
        Bundle args = new Bundle();
        args.putString(BundleConstants.GET_ROSTER_TEAM_KEY, aCurrentUserTeamKey);

        Fragment fragment = new RosterFragment();
        fragment.setArguments(args);

        return fragment;
    }

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, TAG + ".onCreate()");

        mTeamKey = getArguments().getString(BundleConstants.GET_ROSTER_TEAM_KEY, "");

        mEditMode = false;

        mPlayerProfileDialogListener = this;

        mRosterDates = Utils.getRosterDatesArray();

        mPreferences = Preferences.getInstance(mContext);

        mLeagueSettings = ((PuckDroidApplication)getActivity().getApplication()).getLeagueSettings();

        mLeagueWeeklyDeadlineType = ((PuckDroidApplication)getActivity().getApplication()).getLeague().getWeeklyDeadlineType();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.v(TAG, TAG + ".onCreateView()");

        View view = inflater.inflate(R.layout.roster_fragment, container, false);

        setHasOptionsMenu(true);

        mList = (ListView)view.findViewById(R.id.rosterList);
        mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Player player = (Player)mAdapter.getItem(i);

                if (!player.isEmptyPlayer())
                {
                    FragmentManager fm = getActivity().getSupportFragmentManager();

                    PlayerProfileDialogFragment dialogFragment = new PlayerProfileDialogFragment(mContext, player.getPlayerKey(), mPlayerProfileDialogListener, false);
                    dialogFragment.show(fm, "PlayerProfileDialogFragment");
                }
            }
        });

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.rosterLoadingAnimation);
        mStartingLineupTotal = (TextView)view.findViewById(R.id.startingLineupTotal);

        if (mLeagueSettings.getLeagueType() == LeagueSettings.LEAGUE_TYPE_POINTS)
        {
            mStartingLineupTotal.setVisibility(View.VISIBLE);
        }
        else
        {
            mStartingLineupTotal.setVisibility(View.GONE);
        }

        mEditRosterButtonsRow = view.findViewById(R.id.editRosterButtonsRow);

        mSaveButton = (Button)view.findViewById(R.id.saveRosterButton);

        mSaveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                ArrayList<CustomListItem> rosterWithHeaders = ((RosterListAdapter)mAdapter).getRosterPlayerList();
                saveRoster(getRosterWithoutHeaders(rosterWithHeaders));
            }
        });

        mCancelButton = (Button)view.findViewById(R.id.cancelEditRosterButton);
        mCancelButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                cancelEditMode();
            }
        });

        mCurrentRosterDate = Utils.getTodayFormattedDate();

        getRoster(false, false, mCurrentRosterDate);

        return view;
    }

    @Override
    public void onAttach(Activity activity)
    {
        Log.v(TAG, TAG + ".onAttach()");
        super.onAttach(activity);

        mContext = activity;

        mAttachedToManagerActivity = (activity instanceof ManagerActivity);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu)
    {
        if (!mAttachedToManagerActivity)
        {
            return;
        }

        mRefreshMenuItem = menu.findItem(R.id.refreshOption);
        mRefreshMenuItem.setVisible(true);

        mEditMenuItem = menu.findItem(R.id.editOption);
        mEditMenuItem.setVisible(true);

        RosterDateSpinnerAdapter adapter = new RosterDateSpinnerAdapter(mContext, mRosterDates);
        ActionBar.OnNavigationListener navigationListener = new ActionBar.OnNavigationListener()
        {
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId)
            {
                String selectedDate = Utils.getFormattedDate(mRosterDates[itemPosition]);

                Log.v(TAG, "Selected: " + selectedDate + " - Current: " + mCurrentRosterDate);
                if (!selectedDate.equals(mCurrentRosterDate))
                {
                    mCurrentRosterDate = selectedDate;
                    getRoster(false, true, mCurrentRosterDate);

                    mEditMenuItem.setVisible(!Utils.isDateBeforeToday(mCurrentRosterDate));
                }

                return false;
            }
        };

        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        mActionBar.setListNavigationCallbacks(adapter, navigationListener);
        mActionBar.setSelectedNavigationItem(1); //0 = yesterday, 1 = today, 2 = tomorrow, ...

        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        Log.v(TAG, "onOptionsItemSelected()");

        if (item.getItemId() == R.id.refreshOption)
        {
            mRefreshMenuItem.setActionView(R.layout.loading_animation);
            getRoster(true, false, mCurrentRosterDate);
        }
        else if (item.getItemId() == R.id.editOption)
        {
            mEditMode = true;

            mEditRosterButtonsRow.setVisibility(View.VISIBLE);

            ((RosterListAdapter)mAdapter).setEditMode(mEditMode);
            mAdapter.notifyDataSetChanged();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        if (aOriginalRequest.equals(BundleConstants.GET_TEAM_STATS_BY_DATE))
        {
            readTeamStats(aData);
        }
        else if (aOriginalRequest.equals(BundleConstants.GET_ROSTER) || aOriginalRequest.equals(BundleConstants.GET_ROSTER_SEASON_STATS))
        {
            buildList(aData);
        }
        else if (aOriginalRequest.equals(BundleConstants.GET_TOMORROWS_ROSTER))
        {
            mTomorrowsRoster = buildRoster(aData);
            ((PuckDroidApplication)getActivity().getApplication()).setTomorrowsRosterPlayersCount(mTomorrowsRoster.getRosterPlayersCount());
        }
        else if (aOriginalRequest.equals(BundleConstants.PUT_ROSTER))
        {
            Toast.makeText(mContext, mContext.getString(R.string.lineup_saved_toast), Toast.LENGTH_LONG).show();
            getRoster(false, true, mCurrentRosterDate);
        }
    }

    protected void buildList(String aData)
    {
        Log.v(TAG, TAG + ".buildList()");

        Roster roster = buildRoster(aData);

        if (roster != null)
        {
            mAdapter = new RosterListAdapter(mContext, getRosterWithHeaders(roster.getLineup()), mLeagueSettings.getStats());
            mList.setAdapter(mAdapter);

            if (mRefreshMenuItem != null)
            {
                mRefreshMenuItem.setActionView(null);
            }

            Log.v(TAG, "Frequent NPE crash!!! Activity: " + (getActivity() ==  null) + " - Application: " + (getActivity().getApplication() ==  null) + " - isAdded: " + isAdded());

            ((PuckDroidApplication)getActivity().getApplication()).setRosterPlayersCount(roster.getRosterPlayersCount());
        }

        mLoadingSpinner.setVisibility(View.GONE);
        mEditRosterButtonsRow.setVisibility(View.GONE);
    }

    @Override
    public void onPlayerDropSuccessful()
    {
        Toast.makeText(mContext, getString(R.string.player_dropped_success), Toast.LENGTH_SHORT).show();
        getRoster(false, true, mCurrentRosterDate);
    }

    public boolean onBackPressed()
    {
        if (mEditMode)
        {
            cancelEditMode();
            return false;
        }
        else
        {
            return true;
        }
    }

    private Roster buildRoster(String aData)
    {
        Player player = null;
        Roster roster = null;

        ArrayList<Player> playersList = new ArrayList<Player>();

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            PlayersXmlParser playersXmlParser = new PlayersXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("player"))
                    {
                        player = playersXmlParser.readPlayer(mXmlPullParser);
                        playersList.add(player);
                    }
                }
                eventType = mXmlPullParser.next();
            }

            roster = new Roster(playersList, mPreferences);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return roster;
    }

    private void cancelEditMode()
    {
        mEditRosterButtonsRow.setVisibility(View.GONE);

        mEditMode = false;
        ((RosterListAdapter)mAdapter).setEditMode(mEditMode);
        mAdapter.notifyDataSetChanged();
    }

    private void getRoster(boolean aIsRefresh, boolean aIsReload, String aDate)
    {
        Log.v(TAG, TAG + ".loadRoster(" + aIsRefresh + ", " + aDate + ")" );

        if (aIsRefresh || aIsReload)
        {
            mAdapter.notifyDataSetChanged();
        }

        if (mLoadingSpinner != null)
        {
            if (!aIsRefresh || aIsReload)
            {
                mLoadingSpinner.setVisibility(View.VISIBLE);
            }
        }

        if (mLeagueSettings.getLeagueType() == LeagueSettings.LEAGUE_TYPE_POINTS)
        {
            getRosterStats(aDate);
        }

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        if (mAttachedToManagerActivity)
        {
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_ROSTER);
        }
        else
        {
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_ROSTER_SEASON_STATS);
        }

        intent.putExtra(BundleConstants.GET_ROSTER_TEAM_KEY, mTeamKey);

        if (!Utils.isNullOrEmptyString(aDate))
        {
            intent.putExtra(BundleConstants.GET_ROSTER_DATE, aDate);
        }
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);

        if (mAttachedToManagerActivity && mLeagueWeeklyDeadlineType == League.WEEKLY_DEADLINE_TYPE_TOMORROW)
        {
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TOMORROWS_ROSTER);
            intent.putExtra(BundleConstants.GET_ROSTER_DATE, Utils.getTomorrowFormattedDate());
            getActivity().startService(intent);
        }
    }

    private ArrayList<CustomListItem> getRosterWithHeaders(ArrayList<Player> aRoster)
    {
        ArrayList<CustomListItem> rosterWithHeaders = new ArrayList<CustomListItem>();

        HeaderListItem skatersHeader = new HeaderListItem();
        skatersHeader.setHeaderTitle(mContext.getString(R.string.skaters_header));

        rosterWithHeaders.add(skatersHeader);
        boolean benchHeaderAdded = false;
        boolean goalieHeaderAdded = false;
        boolean irHeaderAdded = false;

        for (Player player : aRoster)
        {
            if (!benchHeaderAdded && player.getSelectedPosition().getPosition().equals("BN"))
            {
                HeaderListItem benchHeader = new HeaderListItem();

                benchHeader.setHeaderTitle(mContext.getString(R.string.bench_header));
                rosterWithHeaders.add(benchHeader);
                benchHeaderAdded = true;
            }
            else if (!goalieHeaderAdded && player.getSelectedPosition().getPosition().equals("G"))
            {
                HeaderListItem goalieHeader = new HeaderListItem();
                goalieHeader.setHeaderTitle(mContext.getString(R.string.goalie_header));
                rosterWithHeaders.add(goalieHeader);
                goalieHeaderAdded = true;
            }
            else if (!irHeaderAdded && player.getSelectedPosition().getPosition().equals("IR"))
            {
                HeaderListItem irHeader = new HeaderListItem();
                irHeader.setHeaderTitle(mContext.getString(R.string.ir_header));
                rosterWithHeaders.add(irHeader);
                irHeaderAdded = true;
            }

            rosterWithHeaders.add(player);
        }

        return rosterWithHeaders;
    }

    private ArrayList<Player> getRosterWithoutHeaders(ArrayList<CustomListItem> aListWithHeaders)
    {
        ArrayList<Player> rosterWithoutHeaders = new ArrayList<Player>();

        for (CustomListItem item : aListWithHeaders)
        {
            if (!item.isHeader())
            {
                rosterWithoutHeaders.add((Player)item);
            }
        }

        return rosterWithoutHeaders;
    }

    private void getRosterStats(String aDate)
    {
        Log.v(TAG, TAG + ".getRosterStats() date: " + aDate);

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TEAM_STATS_BY_DATE);

        if (!Utils.isNullOrEmptyString(aDate))
        {
            intent.putExtra(BundleConstants.GET_TEAM_STATS_DATE_VALUE, aDate);
        }
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);

    }

    private void readTeamStats(String aData)
    {
        Log.v(TAG, TAG + ".readTeamStats()");

        try
        {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType=parser.getEventType();
            PoolTeamXmlParser poolTeamXmlParser = new PoolTeamXmlParser();

            Team team = null;
            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(parser.getName().equalsIgnoreCase("team"))
                    {
                        team = poolTeamXmlParser.readTeam(parser);
                    }
                }
                eventType = parser.next();
            }

            if (team != null && team.getPoints() != null)
            {
                mStartingLineupTotal.setVisibility(View.VISIBLE);
                mStartingLineupTotal.setText(String.format(mContext.getString(R.string.starting_lineup_total), team.getPoints().getTotal()));
            }
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void saveRoster(ArrayList<Player> aRoster)
    {
        Log.v(TAG, TAG + ".saveRoster date: " + mCurrentRosterDate);

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.PUT_ROSTER);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.PUT.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        intent.putExtra(BundleConstants.USER_MOVE_DATA, YahooSportsApiXmlWriter.saveRoster(aRoster, mCurrentRosterDate));
        getActivity().startService(intent);
    }

    private class RosterDateSpinnerAdapter extends BaseAdapter implements SpinnerAdapter
    {
        private Context mContext;
        Date[] mDates;

        public RosterDateSpinnerAdapter(Context context, Date[] dates)
        {
            mContext = context;
            mDates = dates;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent)
        {
            return super.getDropDownView(position, convertView, parent);
        }

        @Override
        public int getCount()
        {
            return mDates.length;
        }

        @Override
        public Object getItem(int i)
        {
            return mDates[i];
        }

        @Override
        public long getItemId(int i)
        {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup)
        {
            if(view == null)
            {
                view = LayoutInflater.from(mContext).inflate(R.layout.two_line_spinner, viewGroup, false);
            }

            TextView topLine = (TextView) view.findViewById(R.id.topLine);
            topLine.setText(Utils.getDateDescription(mContext, mDates[i]));
            TextView bottomLine = (TextView) view.findViewById(R.id.bottomLine);
            bottomLine.setText(Utils.getSimpleDate(mDates[i]));

            return view;
        }
    }
}
