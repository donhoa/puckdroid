package com.thedon.PuckDroid.view.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.*;
import com.thedon.PuckDroid.data.LeagueSettings;
import com.thedon.PuckDroid.data.Player;
import com.thedon.PuckDroid.data.parsers.PlayersXmlParser;
import com.thedon.PuckDroid.utils.Utils;
import com.thedon.PuckDroid.view.WireTransactionActivity;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class PlayerProfileDialogFragment extends DialogFragment implements ConfirmationDialogFragment.ConfirmDialogListener
{
    private static final String TAG = PlayerProfileDialogFragment.class.getSimpleName();

    private TableLayout mTable;
    private ImageView mPlayerImageView;
    private ImageView mPlayerTeamLogo;
    private TextView mPlayerInfo;
    private TextView mPlayerOwner;
    private Button mActionButton;
    private String mPlayerKey;
    private Player mPlayer;

    private ResultReceiver mResultReceiver;

    private Context mContext;

    private String mCurrentUserTeamKey;
    private static LeagueSettings sLeagueSettings;

    private ConfirmationDialogFragment mConfirmationDialog;

    private PlayerProfileDialogListener mListener;

    private boolean mCanAddPlayers;

    public interface PlayerProfileDialogListener
    {
        public void onPlayerDropSuccessful();
    }

    public PlayerProfileDialogFragment(Context aContext, String aPlayerKey, PlayerProfileDialogListener aListener, boolean aCanAddPlayers)
    {
        mContext = aContext;
        mPlayerKey = aPlayerKey;
        mListener = aListener;
        mCanAddPlayers = aCanAddPlayers;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);

        sLeagueSettings = ((PuckDroidApplication)getActivity().getApplication()).getLeagueSettings();

        Preferences preferences = Preferences.getInstance(mContext);
        mCurrentUserTeamKey = preferences.getString(Preferences.KEY_CURRENT_USER_TEAM_KEY, "");

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    if (resultCode == YahooSportsApiService.API_RESULT_OK)
                    {
                        String originalRequest = resultData.getString(BundleConstants.YAHOO_SPORTS_API_REQUEST);

                        if (originalRequest.equals(BundleConstants.GET_PLAYER_INFO))
                        {
                            buildPlayerProfile(resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT));
                        }
                        else if (originalRequest.equals(BundleConstants.POST_WIRE_TRANSACTION))
                        {
                            dismiss();

                            if (mListener != null)
                            {
                                mListener.onPlayerDropSuccessful();
                            }
                        }
                    }
                }
            }
        };

        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_PLAYER_INFO);
        intent.putExtra(BundleConstants.GET_PLAYER_INFO_KEY, mPlayerKey);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.player_profile_fragment, container);

        mTable = (TableLayout)view.findViewById(R.id.playerProfileTable);
        mPlayerImageView = (ImageView)view.findViewById(R.id.playerPicture);
        mPlayerTeamLogo = (ImageView)view.findViewById(R.id.playerTeamLogo);
        mPlayerInfo = (TextView)view.findViewById(R.id.playerInfo);
        mPlayerOwner = (TextView)view.findViewById(R.id.playerOwner);
        mActionButton = (Button)view.findViewById(R.id.playerActionButton);

        return view;
    }

    @Override
    public void onPositiveButtonClick()
    {
        Intent intent = new Intent(mContext, YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.POST_WIRE_TRANSACTION);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.POST.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        intent.putExtra(BundleConstants.USER_MOVE_DATA, YahooSportsApiXmlWriter.dropTransaction(mPlayer.getPlayerKey(), mCurrentUserTeamKey));
        getActivity().startService(intent);
    }

    private void buildPlayerProfile(String aData)
    {
        Log.v(TAG, TAG + ".buildPlayerProfile()");

        try
        {
            XmlPullParserFactory mPullParserFactory = XmlPullParserFactory.newInstance();
            mPullParserFactory.setNamespaceAware(true);
            XmlPullParser mXmlPullParser = mPullParserFactory.newPullParser();
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();

            PlayersXmlParser teamXmlParser = new PlayersXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("player"))
                    {
                        mPlayer = teamXmlParser.readPlayer(mXmlPullParser);
                    }
                }
                eventType = mXmlPullParser.next();
            }

            setPlayerInfo();
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void setPlayerInfo()
    {
        Log.v(TAG, TAG + ".setPlayerInfo()");

        String playerInfo = mPlayer.getFullName() + " | " + mPlayer.getUniformNumber() + " | " + Utils.getMainDisplayPosition(mContext, mPlayer.getEligiblePositions());

        mPlayerInfo.setText(playerInfo);

        mPlayerTeamLogo.setImageResource(Utils.getTeamLogoDrawableId(mContext, mPlayer.getEditorialTeamAbbr()));

        if (mPlayer.getPlayerOwnership() != null)
        {
            if (!mPlayer.getPlayerOwnership().isOnWaivers())
            {
                mPlayerOwner.setText(mPlayer.getPlayerOwnership().getOwnerTeamName());
            }
            else
            {
                mPlayerOwner.setText(String.format(mContext.getResources().getString(R.string.playerWaiverDate), mPlayer.getPlayerOwnership().getSimpleWaiverDate()));
            }
        }

        if (mPlayer.getPlayerStatistics() != null)
        {
            Map<Integer, String> treeMap = mPlayer.getPlayerStatistics().getStatsTable();
            for (Hashtable.Entry<Integer, String> entry : treeMap.entrySet())
            {
                String statName = Utils.getStatNameById(sLeagueSettings.getStats(), entry.getKey().toString(), false);
                Log.v(TAG,  "stat: " + statName + "/" + entry.getValue());

                TableRow row = new TableRow(mContext);

                TextView statNameView = new TextView(mContext);
                statNameView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                statNameView.setGravity(Gravity.CENTER_HORIZONTAL);
                statNameView.setText(statName);

                TextView statValueView = new TextView(mContext);
                statValueView.setLayoutParams(new TableRow.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
                statValueView.setGravity(Gravity.CENTER_HORIZONTAL);
                statValueView.setText(entry.getValue());

                row.addView(statNameView);
                row.addView(statValueView);

                mTable.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
            }

            new GetPlayerPictureAsyncTask(mContext, mPlayerImageView ,mPlayer.getHighResImageUrl()).execute();
        }

        if (mCanAddPlayers && (mPlayer.getPlayerOwnership().isFreeAgent() || mPlayer.getPlayerOwnership().isOnWaivers()))
        {
            mActionButton.setVisibility(View.VISIBLE);
            mActionButton.setText(mContext.getString(R.string.add_player_button));
            mActionButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    Intent intent = new Intent(mContext, WireTransactionActivity.class);
                    intent.putExtra(BundleConstants.PLAYER_KEY, mPlayer.getPlayerKey());
                    intent.putExtra(BundleConstants.PLAYER_NAME, mPlayer.getFullName());
                    intent.putExtra(BundleConstants.PLAYER_ON_WAIVERS, mPlayer.getPlayerOwnership().isOnWaivers());
                    mContext.startActivity(intent);
                }
            });
        }
        else if (!mPlayer.isUndroppable() &&mPlayer.getPlayerOwnership().getOwnerTeamKey().equals(mCurrentUserTeamKey))
        {
            mActionButton.setVisibility(View.VISIBLE);
            mActionButton.setText(mContext.getString(R.string.drop_player_button));

            String message = String.format(getString(R.string.wire_transaction_drop_details), mPlayer.getFullName());
            mConfirmationDialog = new ConfirmationDialogFragment(mContext, this, getString(R.string.confirm_transaction), message, getString(R.string.ok_button), getString(R.string.cancel_button));

            mActionButton.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    mConfirmationDialog.show(getActivity().getSupportFragmentManager(), "DropPlayerConfirmationDialog");
                }
            });
        }
    }

    private class GetPlayerPictureAsyncTask extends AsyncTask<Void, Void, Bitmap>
    {
        private Context mContext;
        private ImageView mImageView;
        private String mUrl;

        public GetPlayerPictureAsyncTask(Context aContext, ImageView aImageView, String aUrl)
        {
            mContext = aContext;
            mImageView = aImageView;
            mUrl = aUrl;
        }

        @Override
        protected Bitmap doInBackground(Void... voids)
        {
            Log.v(TAG, "GetPlayerPictureAsyncTask.doInBackground()");
            Bitmap img = null;
            try
            {
                URL url = new URL(mUrl);
                HttpURLConnection connection  = (HttpURLConnection) url.openConnection();

                InputStream is = connection.getInputStream();
                img = BitmapFactory.decodeStream(is);
            }
            catch (IOException e)
            {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            return img;
        }

        @Override
        protected void onPostExecute(Bitmap result)
        {
            if (result != null)
            {
                mImageView.setImageBitmap(result);
            }
        }
    }
}
