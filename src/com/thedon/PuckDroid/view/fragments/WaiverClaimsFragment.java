package com.thedon.PuckDroid.view.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.PuckDroidApplication;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.application.YahooSportsApiXmlWriter;
import com.thedon.PuckDroid.data.WaiverTransaction;
import com.thedon.PuckDroid.data.parsers.TransactionsXmlParser;
import com.thedon.PuckDroid.view.adapters.WaiverClaimsListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class WaiverClaimsFragment extends BaseViewPagerFragment implements WaiverClaimsListAdapter.WaiverClaimsListener, EditFaabValueDialogFragment.EditFaabValueDialogListener
{
    private static final String TAG = WaiverClaimsFragment.class.getSimpleName();

    private ArrayList<WaiverTransaction> mWaiverTransactions = new ArrayList<WaiverTransaction>();

    private Button mSaveButton;

    private EditFaabValueDialogFragment.EditFaabValueDialogListener mListener;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Log.v(TAG, "WaiverClaimsFragment.onCreate()");

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    if (resultCode == YahooSportsApiService.API_RESULT_OK)
                    {
                        String originalRequest = (String)resultData.get(BundleConstants.YAHOO_SPORTS_API_REQUEST);

                        if (originalRequest.equals(BundleConstants.DELETE_TRANSACTION) || originalRequest.equals(BundleConstants.PUT_TRANSACTION))
                        {
                            mWaiverTransactions.clear();
                            mAdapter.notifyDataSetChanged();
                            getWaiverclaims();
                        }
                        else if (originalRequest.equals(BundleConstants.GET_TEAM_WAIVER_CLAIMS))
                        {
                            buildList(resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT));
                        }
                    }
                }
            }
        };

        getWaiverclaims();

        mContext = getActivity();

        mListener = this;

        mLeagueSettings = ((PuckDroidApplication)getActivity().getApplication()).getLeagueSettings();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        Log.v(TAG, TAG + ".onCreateView()");
        View view = inflater.inflate(R.layout.waiver_claims_fragment, container, false);

        setHasOptionsMenu(true);

        mList = (ListView)view.findViewById(R.id.waiverClaimsList);
        mList.setEmptyView(view.findViewById(R.id.waiverClaimsListEmptyView));

        if (mLeagueSettings != null && mLeagueSettings.usesFaab())
        {
            mList.setOnItemClickListener(new AdapterView.OnItemClickListener()
            {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
                {
                    FragmentManager fm = getActivity().getSupportFragmentManager();
                    EditFaabValueDialogFragment dialogFragment = new EditFaabValueDialogFragment(mContext, mWaiverTransactions.get(i).getTransactionKey(), mWaiverTransactions.get(i).getPlayers().get(0).getName().getFullName(), mListener);
                    dialogFragment.show(fm, "EditFaabValueDialogFragment");
                }
            });
        }

        mSaveButton = (Button)view.findViewById(R.id.changePriorityButton);
        mSaveButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                saveWaiverClaims();
            }
        });

        mLoadingSpinner = (ProgressBar)view.findViewById(R.id.waiverClaimsLoadingAnimation);
        mLoadingSpinner.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    protected void handleResponse(String aOriginalRequest, String aData)
    {
        Log.v(TAG, TAG + ".handleResponse(): " + aOriginalRequest);

        if (aOriginalRequest.equals(BundleConstants.DELETE_TRANSACTION) || aOriginalRequest.equals(BundleConstants.PUT_TRANSACTION))
        {
            mWaiverTransactions.clear();
            mAdapter.notifyDataSetChanged();
            getWaiverclaims();
        }
        else if (aOriginalRequest.equals(BundleConstants.GET_TEAM_WAIVER_CLAIMS))
        {
            buildList(aData);
        }
    }

    @Override
    protected void buildList(String aData)
    {
        Log.v(TAG, TAG + ".buildList()");

        try
        {
            mXmlPullParser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType = mXmlPullParser.getEventType();
            TransactionsXmlParser transactionsXmlParser = new TransactionsXmlParser();

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(mXmlPullParser.getName().equalsIgnoreCase("transaction"))
                    {
                        mWaiverTransactions.add(transactionsXmlParser.readWaiverTransaction(mXmlPullParser));
                    }
                }
                eventType = mXmlPullParser.next();
            }

            mAdapter = new WaiverClaimsListAdapter(mContext, mWaiverTransactions, this);
            mList.setAdapter(mAdapter);
            mLoadingSpinner.setVisibility(View.GONE);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    @Override
    public void onCancelWaiver(String aTransactionKey)
    {
        deleteWaiverClaim(aTransactionKey);
    }

    @Override
    public void onSaveFaabValue(String aTransactionKey, String aFaabValue)
    {
        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.PUT_TRANSACTION);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.PUT.name());
        intent.putExtra(BundleConstants.TRANSACTION_KEY, aTransactionKey);
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        intent.putExtra(BundleConstants.USER_MOVE_DATA, YahooSportsApiXmlWriter.editWaiverClaimFaabValue(aTransactionKey, aFaabValue));
        getActivity().startService(intent);
    }

    private void getWaiverclaims()
    {
        Log.v(TAG, TAG + ".getWaiverclaims()");
        Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_TEAM_WAIVER_CLAIMS);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        getActivity().startService(intent);
    }

    private void saveWaiverClaims()
    {
        ArrayList<WaiverTransaction> transactions = ((WaiverClaimsListAdapter)mAdapter).getTransactions();

        for (WaiverTransaction transaction : transactions)
        {
            Intent intent = new Intent(getActivity(), YahooSportsApiService.class);

            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.PUT_TRANSACTION);
            intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.PUT.name());
            intent.putExtra(BundleConstants.TRANSACTION_KEY, transaction.getTransactionKey());
            intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
            intent.putExtra(BundleConstants.USER_MOVE_DATA, YahooSportsApiXmlWriter.editWaiversPriority(transaction));
            getActivity().startService(intent);
        }
    }

    private void deleteWaiverClaim(String aTransactionKey)
    {
        Log.v(TAG, TAG + ".deleteWaiverClaim(): " + aTransactionKey);

        Intent intent = new Intent(mContext, YahooSportsApiService.class);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.DELETE_TRANSACTION);
        intent.putExtra(BundleConstants.TRANSACTION_KEY, aTransactionKey);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.DELETE.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        mContext.startService(intent);
    }
}
