package com.thedon.PuckDroid.view;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public interface CustomListItem
{
    public boolean isHeader();
}
