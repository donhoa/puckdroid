package com.thedon.PuckDroid.view;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class HeaderListItem implements CustomListItem
{
    String mHeaderTitle;

    @Override
    public boolean isHeader()
    {
        return true;
    }

    public void setHeaderTitle(String aHeaderTitle)
    {
        mHeaderTitle = aHeaderTitle;
    }

    public String getHeaderTitle()
    {
        return mHeaderTitle;
    }
}
