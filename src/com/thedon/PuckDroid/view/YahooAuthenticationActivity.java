package com.thedon.PuckDroid.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class YahooAuthenticationActivity extends BaseActivity
{
    private static final String TAG = YahooAuthenticationActivity.class.getSimpleName();

    private ProgressBar mLoadingMessage;

    private Context mContext;
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.yahoo_authentication);

        mLoadingMessage = (ProgressBar)findViewById(R.id.yahooAuthenticationLoadingAnimation);
        mContext = this;
        WebView wv = (WebView) findViewById(R.id.yahooAuthenticationWebView);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setSupportZoom(true);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);

        Bundle extras = getIntent().getExtras();

        if (extras != null)
        {
            String authorizationURL = extras.getString(BundleConstants.YAHOO_AUTHORIZATION_URL);
            Log.v(TAG, "authorizationURL: " + authorizationURL);
            wv.setWebViewClient(new WebViewClient()
            {

                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon)
                {
                    if (mLoadingMessage != null)
                    {
                        mLoadingMessage.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url)
                {
                    Log.v(TAG, "shouldOverrideUrlLoading URL: " + url);

                    if (url.startsWith("puckdroid"))
                    {
                        Intent intent = new Intent(mContext, LeagueListActivity.class);
                        intent.setFlags((Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)); //Clear the activity task which makes LeagueListActivity the root activity
                        intent.putExtra(BundleConstants.YAHOO_OAUTH_VERIFIER_URL, url);
                        startActivity(intent);
                    }

                    return false;
                }

                @Override
                public void onPageFinished(WebView view, String url)
                {
                    if (mLoadingMessage != null)
                    {
                        mLoadingMessage.setVisibility(View.GONE);
                    }
                }
            });
            wv.loadUrl(authorizationURL);
        }
    }
}