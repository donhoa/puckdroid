package com.thedon.PuckDroid.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.thedon.PuckDroid.R;
import com.thedon.PuckDroid.application.BundleConstants;
import com.thedon.PuckDroid.application.OAuthManager;
import com.thedon.PuckDroid.application.Preferences;
import com.thedon.PuckDroid.application.YahooSportsApiService;
import com.thedon.PuckDroid.data.League;
import com.thedon.PuckDroid.data.parsers.LeagueXmlParser;
import com.thedon.PuckDroid.view.adapters.LeagueListAdapter;
import org.scribe.model.Verb;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 *
 * @author: dhoang
 */
public class LeagueListActivity extends BaseActivity
{
    private static final String TAG = LeagueListActivity.class.getSimpleName();

    private OAuthManager mOAuthManager;
    private ResultReceiver mResultReceiver;

    private ListView mLeaguesListView;
    private ArrayList<League> mLeagues = new ArrayList<League>();
    private LeagueListAdapter mAdapter;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.league_list);

        mLeaguesListView = (ListView)findViewById(R.id.leaguesListView);
        mLeaguesListView.setOnItemClickListener(mOnItemClickListener);

        Bundle extras = getIntent().getExtras();

        if (extras != null)
        {
            String verifier_url = extras.getString(BundleConstants.YAHOO_OAUTH_VERIFIER_URL);

            Uri uri = Uri.parse(verifier_url);

            if (uri != null)
            {
                OAuthGetAccessTokenTask task = new OAuthGetAccessTokenTask(this);
                task.execute(uri);
            }
        }
        else
        {
            new GetLeaguesListTask(this).execute();
        }

        mResultReceiver = new ResultReceiver(new Handler())
        {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData)
            {
                Log.v(TAG, "ResultReceiver.onReceiveResult(): " + resultCode);

                if (resultData != null && resultData.containsKey(BundleConstants.YAHOO_SPORTS_API_RESULT))
                {
                    if (resultCode == YahooSportsApiService.API_RESULT_OK)
                    {
                        buildLeaguesList(resultData.getString(BundleConstants.YAHOO_SPORTS_API_RESULT));
                    }
                }
            }
        };
    }

    private void getLeaguesList()
    {
        Log.v(TAG, "getLeaguesList()");

        Intent intent = new Intent(this, YahooSportsApiService.class);

        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST, BundleConstants.GET_LEAGUES_LIST);
        intent.putExtra(BundleConstants.YAHOO_SPORTS_API_REQUEST_TYPE, Verb.GET.name());
        intent.putExtra(BundleConstants.EXTRA_RESULT_RECEIVER, mResultReceiver);
        startService(intent);
    }

    private void buildLeaguesList(String aData)
    {
        Log.v(TAG, "buildLeaguesList()" + aData);

        try
        {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();

            parser.setInput(new ByteArrayInputStream(aData.getBytes()), "UTF_8");
            int eventType=parser.getEventType();
            LeagueXmlParser leagueXmlParser = new LeagueXmlParser();

            League league = null;

            while(eventType != XmlPullParser.END_DOCUMENT)
            {
                if(eventType == XmlPullParser.START_TAG)
                {
                    if(parser.getName().equalsIgnoreCase("game"))
                    {
                        mLeagues = leagueXmlParser.readGames(parser);
                    }
                }
                eventType = parser.next();
            }
            mAdapter = new LeagueListAdapter(this, mLeagues);
            mLeaguesListView.setAdapter(mAdapter);
        }
        catch (XmlPullParserException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        catch (IOException e)
        {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private ListView.OnItemClickListener mOnItemClickListener = new ListView.OnItemClickListener()
    {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id)
        {
            Log.v(TAG, "Click!: " + position + " - " + id);
            League league = (League)mAdapter.getItem(position);

            Preferences preferences = Preferences.getInstance(getApplicationContext());
            preferences.putString(Preferences.KEY_LEAGUE_KEY, league.getLeagueKey());

            if (league.getTeams().size() > 0)
            {
                preferences.putString(Preferences.KEY_CURRENT_USER_TEAM_KEY, league.getTeams().get(0).getTeamKey());
            }

            getGlobalInfo();
        }
    };

    private class OAuthGetAccessTokenTask extends AsyncTask<Uri, Void, Void>
    {
        private Context mContext;

        public OAuthGetAccessTokenTask(Context aContext)
        {
            mContext = aContext;
            mOAuthManager = OAuthManager.getInstance(mContext);
        }

        @Override
        protected void onPreExecute()
        {
            mLoadingAnimation = ProgressDialog.show(mContext, "", mContext.getResources().getString(R.string.oauth_step_2), true, false, null);
        }

        @Override
        protected Void doInBackground(Uri... uris)
        {
            Log.v(TAG, "OAuthGetAccessTokenTask.doInBackground()");

            if (mOAuthManager != null && uris != null)
            {
                mOAuthManager.getAccessToken(uris[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            if (mLoadingAnimation != null)
            {
                mLoadingAnimation.dismiss();
            }

            new GetLeaguesListTask(mContext).execute();
        }
    }

    private class GetLeaguesListTask extends AsyncTask<Void, Void, Void>
    {
        private Context mContext;

        public GetLeaguesListTask(Context aContext)
        {
            mContext = aContext;
            mOAuthManager = OAuthManager.getInstance(mContext);
        }

        @Override
        protected void onPreExecute()
        {
            mLoadingAnimation = ProgressDialog.show(mContext, "", mContext.getResources().getString(R.string.getting_leagues_msg), true, false, null);
        }

        @Override
        protected Void doInBackground(Void... voids)
        {
            getLeaguesList();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid)
        {
            if (mLoadingAnimation != null)
            {
                mLoadingAnimation.dismiss();
            }
        }
    }
}